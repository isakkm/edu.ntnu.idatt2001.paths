package edu.ntnu.idatt2001.paths.model.actions;

import edu.ntnu.idatt2001.paths.model.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GoldActionTest {

    private Player player;
    private Action action;

    @BeforeEach
    public void setup() {
        player = new Player.Builder("Player 1").build();
    }

    @Nested
    public class ExecuteTest {

        @Test
        public void executePositiveGoldToPlayer() {
            action = new GoldAction(1);
            action.execute(player);
            assertTrue(player.getGold() == 1);
        }

        @Test
        public void executeNegativeGoldToPlayer() {
            player.addGold(2);
            action = new GoldAction(-1);
            action.execute(player);
            assertTrue(player.getGold() == 1);
        }

        @Test
        public void executeGoldToPlayerRepeatedly() {
            action = new GoldAction(1);
            action.execute(player);
            action.execute(player);
            action.execute(player);
            assertTrue(player.getGold() == 3);
        }
    }

    @Test
    public void testToPathsFormatString() {
        GoldAction action = new GoldAction(5);
        assertEquals("GoldAction:5", action.toPathsFormatString());
    }
}
