package edu.ntnu.idatt2001.paths.model.goals;

import edu.ntnu.idatt2001.paths.model.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ScoreGoalTest {

  @Nested
  @DisplayName("Constructor tests")
  public class ConstructorTest {

    @Test
    @DisplayName("Should throw IllegalArgumentException when minimumScore is less than 1")
    public void scoreGoalCannotBeLessThan1() {
      assertThrows(IllegalArgumentException.class, () -> new ScoreGoal(0));
    }
  }

  @Nested
  @DisplayName("Test for getter method")
  public class GettersTest {
    private ScoreGoal scoreGoal;

    @BeforeEach
    public void setUp() {
      scoreGoal = new ScoreGoal(10);
    }

    @Test
    @DisplayName("Should return minimumScore")
    public void getMinimumScore() {
      assertEquals(10, scoreGoal.getGoalValue());
    }
  }

  @Nested
  @DisplayName("Test for isFulfilled method")
  public class IsFulfilledTest {
    private ScoreGoal scoreGoal;
    private Player player;

    @BeforeEach
    public void setUp() {
      player = new Player.Builder("Player 1").health(100).gold(0).build();
      scoreGoal = new ScoreGoal(10);
    }

    @Test
    @DisplayName("Should return true when player has more score than minimumScore")
    public void playerHasMoreScoreThanMinimumScore() {
      player.addScore(15);
      assertTrue(scoreGoal.isFulfilled(player));
    }

    @Test
    @DisplayName("Should return false when player has less score than minimumScore")
    public void playerHasLessScoreThanMinimumScore() {
      player.addScore(-15);
      assertFalse(scoreGoal.isFulfilled(player));
    }
  }
}
