package edu.ntnu.idatt2001.paths.model;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.paths.model.actions.HealthAction;
import edu.ntnu.idatt2001.paths.model.player.Player;
import edu.ntnu.idatt2001.paths.model.player.PlayerChangeListener;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class PlayerTest {
  
  @Nested
  @DisplayName("Builder constructor test")
  public class BuilderConstructorTest {
    @Test
    @DisplayName("Test player cannot have null as name")
    public void playerCannotHaveNullName() {
      assertThrows(IllegalArgumentException.class, () -> new Player.Builder(null).build());
    }
  
    @Test
    @DisplayName("Test player cannot have empty name")
    public void playerCannotHaveBlankOrEmptyName() {
      assertThrows(IllegalArgumentException.class, () -> new Player.Builder(" ") );
    }
  
    @Test
    @DisplayName("Test player initial default values")
    public void playerInitializedWithOnlyNameHasDefaultValues() {
      Player player = new Player.Builder("Player 1").build();
      assertEquals(100, player.getHealth());
      assertEquals(0, player.getScore());
      assertEquals(0, player.getGold());
    }
  
    @Test
    @DisplayName("Test player initialized with zero health throws exception")
    public void playerInitializedWithZeroHealthThrowsException() {
      assertThrows(IllegalArgumentException.class, () -> new Player.Builder("Player 1").health(0).build());
    }
  
    @Test
    @DisplayName("Test player initialized with negative health throws exception")
    public void playerInitializedWithNegativeHealthThrowsException() {
      assertThrows(IllegalArgumentException.class, () -> new Player.Builder("Player 1").health(-1).build());
      assertThrows(IllegalArgumentException.class, () -> new Player.Builder("Player 1").health(-50).build());
    }
  
    @Test
    @DisplayName("Test player initialized without inventory becomes empty")
    public void playerInitializedWithoutInventoryBecomesEmptyList() {
      Player player = new Player.Builder("Player 1").build();
      assertEquals(0, player.getInventory().size());
    }
    
    @Test
    @DisplayName("Test gold can not be negative")
    public void goldCannotBeNegative() {
      assertThrows(IllegalArgumentException.class, ()-> new Player.Builder("Player 1").gold(-1));
      assertThrows(IllegalArgumentException.class, ()-> new Player.Builder("Player 1").gold(-100));
    }

    @Test
    @DisplayName("Test score can not be negative")
    public void scoreCannotBeNegative() {
      assertThrows(IllegalArgumentException.class, ()-> new Player.Builder("Player 1").score(-1));
      assertThrows(IllegalArgumentException.class, ()-> new Player.Builder("Player 1").score(-100));
    }

    @Test
    @DisplayName("Test health can not be negative")
    public void healthCannotBeNegative() {
      assertThrows(IllegalArgumentException.class, ()-> new Player.Builder("Player 1").health(-1));
      assertThrows(IllegalArgumentException.class, ()-> new Player.Builder("Player 1").health(-100));
    }
  
    @Test
    @DisplayName("Test null inventory items throws exception")
    public void inventoryItemsWhichIsNullThrowsException() {
      Map<String, Integer> inventory = new HashMap<>(Map.of("Diamond Pickaxe", 1, "Cobblestone", 64, "Iron Chest plate", 1));
      inventory.put(null, 0);
      assertThrows(IllegalArgumentException.class, ()-> new Player.Builder("Player 1").inventory(inventory));
    }

    @Test
    @DisplayName("Test initialize player change listener with builder")
    public void playerChangeListenerTest() {
      List<PlayerChangeListener> playerChangeListeners = new ArrayList<>();
      playerChangeListeners.add(new PlayerChangeListener() {
        @Override
        public void playerChanged(Player player) {}});
      System.out.println(playerChangeListeners.size());
      Player player = new Player.Builder("Player 1").playerChangeListeners(playerChangeListeners).build();
      assertEquals(1, player.getPlayerChangeListeners().size());
    }

    @Test
    @DisplayName("Test initialize player with null listener throws exception")
    public void playerChangeListenerWhichIsNullThrowsException() {
      List<PlayerChangeListener> playerChangeListeners = new ArrayList<>();
      playerChangeListeners.add(null);
      assertThrows(IllegalArgumentException.class, ()-> new Player.Builder("Player 1").playerChangeListeners(playerChangeListeners));
    }
  }
  
  @Nested
  @DisplayName("Test equal method")
  public class EqualsTest {

    @Test
    @DisplayName("Test player with same name are not equal")
    public void playersWithSameNameAreNotEqual() {
      Player player1 = new Player.Builder("Player").build();
      Player player2 = new Player.Builder("Player").build();
      assertNotEquals(player1, player2);
    }
  }

  @Nested
  public class ModifyingPlayerAbilitiesTest {
    @Test
    @DisplayName("Test subtraction from player health")
    public void canSubtractHealthFromPlayer() {
      Player player = new Player.Builder("Player1").build();
      player.addHealth(-10);
      assertEquals(90, player.getHealth());
    }
  
    @Test
    @DisplayName("Test add score to player")
    public void canAddScorePointsToPlayer() {
      Player player = new Player.Builder("Player1").build();
      player.addScore(10);
      assertEquals(10, player.getScore());
    }
    
    @Test
    @DisplayName("Test add gold to player")
    public void canAddGoldToPlayer() {
      Player player = new Player.Builder("Player1").build();
      player.addGold(100);
      assertEquals(100, player.getGold());
    }
    
    @Test
    @DisplayName("Test health cannot be negative")
    public void healthCannotBeNegative() {
      Player player = new Player.Builder("Player1").build();
      player.addHealth(-110);
      assertEquals(0, player.getHealth());
    }
  
    @Test
    @DisplayName("Test score cannot be negative")
    public void scoreCannotBeNegative() {
      Player player = new Player.Builder("Player1").build();
      player.addScore(-100);
      assertEquals(0, player.getScore());
    }
    @Test
    @DisplayName("Test gold  cannot be negative")
    public void goldCannotBeNegative() {
      Player player = new Player.Builder("Player1").build();
      player.addGold(-100);
      assertEquals(0, player.getGold());
    }
  }
  
  @Nested
  @DisplayName("Test inventory test")
  public class ModifyingInventoryTest {
    @Test
    @DisplayName("Test add item to inventory")
    public void canAddItemToInventory() {
      Player player = new Player.Builder("Player 1").build();
      player.addToInventory("Item example");
      assertEquals(1, player.getInventory().size());
    }
    
    @Test
    @DisplayName("Test cannot add blank item to inventory")
    public void cannotAddBlankStringToInventory() {
      Player player = new Player.Builder("Player 1").build();
      assertThrows(IllegalArgumentException.class, () -> player.addToInventory(" "));
    }
  
    @Test
    @DisplayName("Test cannot add null item to inventory")
    public void cannotAddNullToInventory() {
      Player player = new Player.Builder("Player 1").build();
      assertThrows(IllegalArgumentException.class, () -> player.addToInventory(null));
    }
    
    @Test
    @DisplayName("Test can add duplicate items in inventory")
    public void canAddDuplicatesToItem() {
      Player player = new Player.Builder("Player 1").build();
      player.addToInventory("Same item");
      player.addToInventory("Same item");
      assertEquals("Same item", player.getInventory().keySet().stream().toList().get(0));
      assertEquals(2, player.getInventory().get("Same item"));
    }
  }
  
  @Nested
  @DisplayName("Test methods test")
  public class MethodTests {

    @Test
    @DisplayName("Test the is alive method for false")
    public void testFalseForIsAliveMethod() {
      Player player = new Player.Builder("Player 1").health(100).build();
      HealthAction healthAction = new HealthAction(-100);
      healthAction.execute(player);
      assertFalse(player.isAlive());
    }

    @Test
    @DisplayName("Test the is alive method for true")
    public void testTrueForIsAliveMethod() {
      Player player = new Player.Builder("Player 1").health(100).build();
      HealthAction healthAction = new HealthAction(-90);
      healthAction.execute(player);
      assertTrue(player.isAlive());
    }

    @Test
    @DisplayName("Test get name method")
    public void testGetNameMethod() {
      Player player = new Player.Builder("Test").health(10).gold(10).score(10).build();
      assertEquals("Test", player.getName());
    }
  
    @Test
    @DisplayName("Test get add health method")
    public void testAddHealthMethodPositiveOutcome() {
      Player player = new Player.Builder("Test").health(10).gold(10).score(10).build();
      player.addHealth(10);
      assertEquals(20, player.getHealth());
    }
  
    @Test
    @DisplayName("Test get heath method")
    public void testGetHealthMethod() {
      Player player = new Player.Builder("Test").health(10).gold(10).score(10).build();
      assertEquals(10, player.getHealth());
    }
  
    @Test
    @DisplayName("Test add score method")
    public void testAddScoreMethodPositiveOutcome() {
      Player player = new Player.Builder("Test").health(10).gold(10).score(10).build();
      player.addScore(10);
      assertEquals(20, player.getScore());
    }
  
    @Test
    @DisplayName("Test get score method")
    public void testGetScoreMethod() {
      Player player = new Player.Builder("Test").health(10).gold(10).score(10).build();
      assertEquals(10, player.getScore());
    }
  
    @Test
    @DisplayName("Test add gold method")
    public void testAddGoldMethodPositiveOutcome() {
      Player player = new Player.Builder("Test").health(10).gold(10).score(10).build();
      player.addGold(10);
      assertEquals(20, player.getGold());
    }
  
    @Test
    @DisplayName("Test get gold method")
    public void testGetGoldMethod() {
      Player player = new Player.Builder("Test").health(10).gold(10).score(10).build();
      assertEquals(10, player.getGold());
    }

    @Test
    @DisplayName("Test the set inventory method")
    public void testSetInventoryMethod() {
      Player player = new Player.Builder("Test").health(10).gold(10).score(10).build();
      Map<String, Integer> inventory = Map.of("Sword", 1, "Shield", 1);
      player.setInventory(inventory);
      assertEquals(2, player.getInventory().size());
    }

    @Test
    @DisplayName("Test add to inventory method")
    public void testAddToInventoryMethodPositiveOutcome() {
      Player player = new Player.Builder("Test").health(10).gold(10).score(10).build();
      player.addToInventory("Test");
      assertEquals("Test", player.getInventory().keySet().stream().toList().get(0));
    }

    @Test
    @DisplayName("Test null to inventory method")
    public void testAddNullToInventoryMethod() {
      Player player = new Player.Builder("Test").health(10).gold(10).score(10).build();
      assertThrows(IllegalArgumentException.class, () -> player.addToInventory(null), "Item cannot be null or empty");
    }

    @Test
    @DisplayName("Test add blank ti inventory method")
    public void testAddBlankToInventoryMethod() {
      Player player = new Player.Builder("Test").health(10).gold(10).score(10).build();
      assertThrows(IllegalArgumentException.class, () -> player.addToInventory(" "), "Item cannot be null or empty");
    }

    @Test
    @DisplayName("Test add listener to player change listener method")
    public void testAddListenerToPlayerChangeListenerListMethod() {
      Player player = new Player.Builder("Test").health(10).gold(10).score(10).build();
      player.addPlayerChangeListener(new PlayerChangeListener() {
        @Override
        public void playerChanged(Player player) {}});
      assertEquals(1, player.getPlayerChangeListeners().size());
    }

    @Test
    @DisplayName("Test set list of listener method")
    void testSetPlayerChangeListenerMethod() {
      Player player = new Player.Builder("Test").health(10).gold(10).score(10).build();
      List<PlayerChangeListener> playerChangeListeners = List.of(new PlayerChangeListener() {
        @Override
        public void playerChanged(Player player) {}});
      player.setPlayerChangeListeners(playerChangeListeners);
      assertEquals(1, player.getPlayerChangeListeners().size());
    }
  }
}