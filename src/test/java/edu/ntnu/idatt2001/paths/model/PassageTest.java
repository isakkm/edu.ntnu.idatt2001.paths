package edu.ntnu.idatt2001.paths.model;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.paths.model.Link;
import edu.ntnu.idatt2001.paths.model.Passage;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class PassageTest {

  @Nested
  @DisplayName("Test constructor")
  public class ConstructorTest {
    @Test
    @DisplayName("Test passage cannot have null title")
    public void passageCannotHaveNullTitle() {
      assertThrows(IllegalArgumentException.class, () -> new Passage(null, "Content example"));
    }
  
    @Test
    @DisplayName("Test passage cannot have empty title")
    public void passageCannotHaveEmptyOrBlankTitle() {
      assertThrows(IllegalArgumentException.class, () -> new Passage(" ", "Content example"));
    }
  
    @Test
    @DisplayName("Test passage cannot have null content")
    public void passageCannotHaveNullContent() {
      assertThrows(IllegalArgumentException.class, () -> new Passage("Title example", null));
    }
  
    @Test
    @DisplayName("Test passage cannot have empty or blank content")
    public void passageCannotHaveEmptyOrBlankContent() {
      assertThrows(IllegalArgumentException.class, () -> new Passage("Title example", " "));
    }
    
    @Test
    @DisplayName("Test passage cannot have null links")
    public void passageCannotHaveNullLinks() {
      assertThrows(IllegalArgumentException.class, () -> new Passage("Title example", "Content example", null));
    }

    @Test
    @DisplayName("Test passage cannot have null link")
    public void passageCannotHaveNullLink() {
      assertThrows(IllegalArgumentException.class, () -> new Passage("Title example", "Content example", List.of(new Link("Text", "Content", null ))));
    }
    
    @Test
    @DisplayName("Test passage cannot have list which contain null values")
    public void passageCannotHaveListWhichContainNullValues() {
      List<Link> links = new ArrayList<>();
      links.add(null);
      assertThrows(IllegalArgumentException.class, () -> new Passage("Title example", "Content example", links));
    }
  }
  
  @Nested
  @DisplayName("Test equals method")
  public class EqualsTest {
    @Test
    @DisplayName("Test passages are equal if title match")
    public void passagesAreEqualIfTitleMatch() {
      Passage passage1 = new Passage("Same title", "Content1");
      Passage passage2 = new Passage("Same title", "Content2");
      assertEquals(passage1, passage2);
    }
    
    @Test
    @DisplayName("Test passages are not equal if only content match")
    public void passagesAreNotEqualIfOnlyContentMatch() {
      Passage passage1 = new Passage("Title1", "Same content");
      Passage passage2 = new Passage("Title2", "Same content");
      assertNotEquals(passage1, passage2);
    }
  }
  
  @Nested
  @DisplayName("Test methods")
  public class MethodTest {

    @Test
    @DisplayName("Test passage cannot add null link")
    public void passageCannotAddNullLink() {
      Passage passage = new Passage("Title example", "Content example");
      assertThrows(IllegalArgumentException.class, () -> passage.addLink(null));
    }

    @Test
    @DisplayName("Test passage cannot contain duplicate links")
    public void passageCannotContainDuplicateLinks() {
      Link link = new Link("Text example", "Reference example");
      Passage passage = new Passage("Title example", "Content example");
      assertTrue(passage.addLink(link));
      assertFalse(passage.addLink(link));
    }
    @Test
    @DisplayName("Test passages has links after adding links")
    public void passageHasLinksAfterAddingLinks() {
      Passage passage = new Passage("Title example", "Content example");
      Link link = new Link("Text example", "Reference example");
      assertFalse(passage.hasLinks());
      passage.addLink(link);
      assertTrue(passage.hasLinks());
    }

    @Test
    @DisplayName("Test passage toString method")
    public void passageToStringTest() {
      Passage passage = new Passage("Title example", "Content example");
      String expectedString = "Passage: title: Title example, connected links: 0";
      assertEquals(expectedString, passage.toString());
    }

    @Test
    @DisplayName("Test passage hashCode test")
    public void passageHashCodeTest() {
      Passage passage1 = new Passage("Title example", "Content example");
      Passage passage2 = new Passage("Title example", "Different example");
      assertEquals(passage1.hashCode(), passage2.hashCode());
    }
   }
}