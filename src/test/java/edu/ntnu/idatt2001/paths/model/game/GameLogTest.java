package edu.ntnu.idatt2001.paths.model.game;

import edu.ntnu.idatt2001.paths.model.Link;
import edu.ntnu.idatt2001.paths.model.Passage;
import edu.ntnu.idatt2001.paths.model.Story;
import edu.ntnu.idatt2001.paths.model.goals.GoldGoal;
import edu.ntnu.idatt2001.paths.model.player.Player;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GameLogTest {

  private Game game;
  private GameLog gameLog;
  private Player player;

  @BeforeEach
  public void setUp() {
    gameLog = new GameLog();
    player = new Player.Builder("Player 1").build();
    game = new Game(player, new Story("Story title", new Passage("Opening passage", "content")), List.of(new GoldGoal(100)));

    Player player1 = new Player.Builder("player 1").gold(1000).build();
    Player player2 = new Player.Builder("player 2").health(1000).build();
    Player player3 = new Player.Builder("player 3").score(1000).build();

    Passage passage1 = new Passage("Passage 1", "Content 1");
    Passage passage2 = new Passage("Passage 2", "Content 2");
    Passage passage3 = new Passage("Passage 3", "Content 3");

    gameLog.addLog(passage1, player1);
    gameLog.addLog(passage2, player2);
    gameLog.addLog(passage3, player3);

    Link link1 = new Link("Text", "Passage 1");
    Link link2 = new Link("Text", "Passage 2");

    gameLog.addLink(link1);
    gameLog.addLink(link2);
  }

  @Nested
  @DisplayName("Test methods")
  public class TestMethods {

    @Test
    @DisplayName("Test valid undo for true")
    public void trueValidUndoTest() {
      assertTrue(gameLog.isValidUndo());
    }

    @Test
    @DisplayName("Test valid undo for false")
    public void falseValidUndoTest() {
      gameLog.undo(game); // Removes one passage from a passage list - 2 left
      gameLog.undo(game);
      assertFalse(gameLog.isValidUndo());
    }

    @Test
    @DisplayName("Test valid undo for false 2")
    public void falseValidUndoTest2() {
      gameLog.undo(game);
      gameLog.undo(game);
      gameLog.undo(game);
      assertFalse(gameLog.isValidUndo());
    }

    @Test
    @DisplayName("Test undo player method")
    public void undoPlayerMethodTest() {
      gameLog.undoPlayer(player);
      assertEquals(1000, player.getHealth());
    }
  }
}