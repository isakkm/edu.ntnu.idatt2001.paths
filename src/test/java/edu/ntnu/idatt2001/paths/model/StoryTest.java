package edu.ntnu.idatt2001.paths.model;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.ntnu.idatt2001.paths.io.FileReader;
import edu.ntnu.idatt2001.paths.model.actions.ResetAllAction;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class StoryTest {
  private final Passage openingPassage = new Passage("Title example", "Content example");
  private final FileReader fileReader = new FileReader();
  
  @Nested
  @DisplayName("Test constructor")
  public class ConstructorTest {
    @Test
    @DisplayName("Test story cannot have null title")
    public void storyCannotHaveNullTitle() {
      assertThrows(IllegalArgumentException.class, () -> new Story(null, openingPassage));
    }
  
    @Test
    @DisplayName("Test story cannot have empty title")
    public void storyCannotHaveEmptyOrBlankTitle() {
      assertThrows(IllegalArgumentException.class, () -> new Story(" ", openingPassage));
    }
  
    @Test
    @DisplayName("Test story cannot have null passages")
    public void storyCannotHaveNullPassages() {
      assertThrows(IllegalArgumentException.class, () -> new Story("Title example", null, openingPassage));
    }

    @Test
    @DisplayName("Test story cannot have null opening passage")
    public void storyCannotHaveNullOpeningPassage() {
      Map<Link, Passage> passages = new HashMap<>();
      passages.put(null, new Passage("Title example", "Content example"));
      assertThrows(IllegalArgumentException.class, () -> new Story("Title example", passages, null));
    }
  
    @Test
    @DisplayName("Test story cannot have passages with null keys")
    public void storyCannotHavePassagesWithNullKeys() {
      Map<Link, Passage> passages = new HashMap<>();
      passages.put(null, new Passage("Title example", "Content example"));
      assertThrows(IllegalArgumentException.class, () -> new Story("Title example", passages, openingPassage));
    }
    
    @Test
    @DisplayName("Test story without passages has empty map")
    public void storyWithoutPassagesHasEmptyMap() {
      Story story = new Story("Title example", openingPassage);
      assertNotNull(story.getPassages());
      assertEquals(0, story.getPassages().size());
    }
  }
  
  @Nested
  @DisplayName("Test equals method")
  public class EqualsTest {
    @Test
    @DisplayName("Test stories are not equal with same title and opening passage")
    public void storiesAreNotEqualWithSameTitleAndOpeningPassage() {
      Story story1 = new Story("Same title", openingPassage);
      Story story2 = new Story("Same title", openingPassage);
      assertNotEquals(story1, story2);
    }
  }
  
  @Nested
  @DisplayName("Test add passage methods")
  public class AddPassageTest {
    @Test
    @DisplayName("Test add passage and create a link which refers back to passage")
    public void addPassageAndCreateALinkWhichRefersBackToPassage() {
      Passage passage = new Passage("Title example", "Content example");
      Story story = new Story("Title example", openingPassage);
      story.addPassage(passage);
      assertEquals(1, story.getPassages().size());
      assertTrue(story.getPassages().contains(passage));
      assertEquals(passage, story.getPassage(new Link(passage.getTitle(), passage.getTitle())));
    }
    
    @Test
    @DisplayName("Test add null passage throws exception")
    public void addNullPassageThrowsException() {
      Story story = new Story("Title example", openingPassage);
      assertThrows(IllegalArgumentException.class, () -> story.addPassage(null));
    }
    
    @Test
    @DisplayName("Test add duplicate passage throws exception")
    public void addDuplicatePassageThrowsException() {
      Story story = new Story("Title example", openingPassage);
      Passage passage = new Passage("Title1", "Content1");
      story.addPassage(passage);
      assertThrows(IllegalArgumentException.class, () -> story.addPassage(passage));
    }
    
    @Test
    @DisplayName("Test add passage with duplicate title throws exception")
    public void addPassageWithDuplicateTitleThrowsException() {
      Story story = new Story("Title example", openingPassage);
      Passage passage1 = new Passage("Same title", "Content example1");
      Passage passage2 = new Passage("Same title", "Content example2");
      story.addPassage(passage1);
      assertThrows(IllegalArgumentException.class, () -> story.addPassage(passage2));
    }
  }
  
  @Nested
  @DisplayName("Test for remove passage method")
  public class RemovePassageTest{

    @Test
    @DisplayName("Test for removing passage with null")
    public void removePassageWithNull() {
      try {
        Story story = fileReader.readStory(new File("src/test/resources/HauntedHouseWithNoReferredPassage.paths"));
        assertThrows(IllegalArgumentException.class, () -> story.removePassage(null));
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Test for removing passage with no passages referring to it")
    public void removePassageWithLink() {
      try {
        Story story = fileReader.readStory(new File("src/test/resources/HauntedHouseWithNoReferredPassage.paths"));
        int passagesInStoryBeforeRemoving = story.getPassages().size();
        story.removePassage(new Link("Go down to the basement","Basement"));
        assertEquals(passagesInStoryBeforeRemoving - 1, story.getPassages().size());
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      }
    }
  
    @Test
    @DisplayName("Test for no changes when removing passage with passages referring to it")
    public void failRemovingPassageWithLink() {
      try {
        Story story = fileReader.readStory(new File("src/test/resources/HauntedHouseExample.paths"));
        int passagesInStoryBeforeRemoving = story.getPassages().size();
        story.removePassage(new Link("Try to open the door","Another room"));
        assertEquals(passagesInStoryBeforeRemoving, story.getPassages().size());
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Test for removing passage when it is referenced by it self, and no other passages")
    public void removeSelfReferencingPassage() {
      Story story = new Story("Title example", openingPassage);
      Passage passage = new Passage("Title example", "Content example");
      Link link = new Link(passage.getTitle(), passage.getTitle());
      passage.addLink(link);
      story.addPassage(passage);
      story.removePassage(link);
      assertEquals(0, story.getPassages().size());
    }
  }
  
  @Nested
  @DisplayName("Test for get broken links methods")
  public class getBrokenLinksTest {
    
    @Test
    @DisplayName("Test for returning broken links from a story")
    public void getBrokenLinksFromStoryTest() {
      try {
        Story story = fileReader.readStory(new File("src/test/resources/HauntedHouseWithNoReferredPassage.paths"));
        fileReader.getLineErrors().forEach(System.out::println);
        List<Link> expectedLinks = List.of(new Link("Open the book", "The book of spells"),
                new Link("Get out of the basement", "Out of the basement"),
                new Link("Unlock the chain and open the refrigerator", "Refrigerator", List.of(new ResetAllAction())));
        assertEquals(expectedLinks, story.getBrokenLinks());
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      }
    }
  }
  
  
}