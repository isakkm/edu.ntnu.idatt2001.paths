package edu.ntnu.idatt2001.paths.model.game;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.paths.model.Link;
import edu.ntnu.idatt2001.paths.model.Passage;
import edu.ntnu.idatt2001.paths.model.Story;
import java.util.ArrayList;
import java.util.List;

import edu.ntnu.idatt2001.paths.model.game.Game;
import edu.ntnu.idatt2001.paths.model.goals.Goal;
import edu.ntnu.idatt2001.paths.model.goals.ScoreGoal;
import edu.ntnu.idatt2001.paths.model.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class GameTest {
  private Story story;
  private Player player;
  private List<Goal> goals;
  private Game game;
  
  
  @BeforeEach
  public void setUp() { //Trying @BeforeEach to avoid repeating code
    Passage openingPassage = new Passage("Passage title", "Passage content");
    goals = new ArrayList<>();
    goals.add(new ScoreGoal(100));
    player = new Player.Builder("Player 1").build();
    story = new Story("Story title", openingPassage);
    game = new Game(player, story, goals);
  }
  
  @Nested
  public class ConstructorTest {
    @Test
    public void playerCannotBeNull() {
      assertThrows(IllegalArgumentException.class, () -> new Game(null, story, goals));
    }
  
    @Test
    public void storyCannotBeNull() {
      assertThrows(IllegalArgumentException.class, () -> new Game(player, null, goals));
    }
  
    @Test
    public void goalsCannotBeNull() {
      assertThrows(IllegalArgumentException.class, () -> new Game(player, story, null));
    }
  
    @Test
    public void goalsCannotBeEmpty() {
      assertThrows(IllegalArgumentException.class, () -> new Game(player, story, new ArrayList<>()));
    }
  }
  
  @Nested
  public class MethodTests {
    @Test
    public void beginReturnsOpeningPassage() {
      Passage openingPassage = new Passage("Opening title", "Opening content");
      Story story1 = new Story("Story title", openingPassage);
      Game game1 = new Game(player, story1, goals);
      assertEquals(openingPassage, game1.begin());
    }
    
    @Test
    public void passageIsNullIfLinkIsNull() {
      assertNull(game.go(null));
    }
  
    @Test
    public void passageIsNullIfLinkReferenceToNothing() {
      assertNull(game.go(new Link("Link title", "Reference to noting")));
    }
    
    @Test
    public void testGameGetMethods() {
      assertEquals(player, game.getPlayer());
      assertEquals(story, game.getStory());
      assertEquals(goals, game.getGoals());
    }
  }
}