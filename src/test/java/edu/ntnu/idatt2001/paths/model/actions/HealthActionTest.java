package edu.ntnu.idatt2001.paths.model.actions;

import edu.ntnu.idatt2001.paths.model.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HealthActionTest {

    private Player player;
    private Action action;

    @BeforeEach
    public void setup() {
        player = new Player.Builder("Player 1").build();
    }

    @Nested
    public class ExecuteTest {

        @Test
        public void executePositiveHealthToPlayer() {
            action = new HealthAction(1);
            action.execute(player);
            assertTrue(player.getHealth() == 101);
        }

        @Test
        public void executeNegativeHealthToPlayer() {
            player.addHealth(2);
            action = new HealthAction(-1);
            action.execute(player);
            assertTrue(player.getHealth() == 101);
        }

        @Test
        public void executeHealthToPlayerRepeatedly() {
            action = new HealthAction(1);
            action.execute(player);
            action.execute(player);
            action.execute(player);
            assertTrue(player.getHealth() == 103);
        }
    }

    @Test
    public void testToPathsFormatString() {
        HealthAction action = new HealthAction(5);
        assertEquals("HealthAction:5", action.toPathsFormatString());
    }
}
