package edu.ntnu.idatt2001.paths.model.actions;

import com.sun.source.tree.PackageTree;
import edu.ntnu.idatt2001.paths.model.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ResetAllActionTest {

    private Player player;
    private Action action;
    private final String itemExample = "Diamond Pickaxe";

    @BeforeEach
    public void setup() {
        player = new Player.Builder("Player 1").build();
        player.addToInventory(itemExample);
        player.addGold(1000);
        player.addHealth(100);
        player.addScore(100);
    }

    @Nested
    public class ExecuteTest {

        @Test
        public void executeItemFromPlayer() {
            action = new ResetAllAction();
            player.addToInventory(itemExample);
            action.execute(player);
            assertTrue(player.getInventory().isEmpty());
        }

        @Test
        public void executeItemFromPlayerRepeatedly() {
            action = new ResetAllAction();
            player.addToInventory(itemExample);
            action.execute(player);
            player.addToInventory(itemExample);
            player.addToInventory(itemExample);
            action.execute(player);
            assertTrue(player.getInventory().isEmpty());
        }
    }

    @Test
    public void toPathsFormatStringTest() {
        action = new ResetAllAction();
        assertTrue(action.toPathsFormatString().equals("ResetAllAction:"));
    }
}
