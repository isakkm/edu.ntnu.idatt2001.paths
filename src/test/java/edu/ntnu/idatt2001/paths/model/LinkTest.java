package edu.ntnu.idatt2001.paths.model;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import edu.ntnu.idatt2001.paths.model.actions.Action;
import edu.ntnu.idatt2001.paths.model.actions.GoldAction;
import edu.ntnu.idatt2001.paths.model.actions.HealthAction;
import edu.ntnu.idatt2001.paths.model.actions.InventoryAction;
import edu.ntnu.idatt2001.paths.model.actions.ScoreAction;
import edu.ntnu.idatt2001.paths.model.Link;
import edu.ntnu.idatt2001.paths.model.player.Player;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LinkTest {
  
  @Nested
  @DisplayName("Test constructor")
  public class ConstructorTest {
    
    @Test
    @DisplayName("Test link cannot have null text")
    public void linkCannotHaveNullText() {
      assertThrows(IllegalArgumentException.class, () -> new Link(null, "Reference example"));
    }
  
    @Test
    @DisplayName("Test link cannot have empty text")
    public void linkCannotHaveEmptyText() {
      assertThrows(IllegalArgumentException.class, () -> new Link(" ", "Reference example"));
    }
  
    @Test
    @DisplayName("Test link cannot have empty text")
    public void linkCannotHaveBlankText() {
      assertThrows(IllegalArgumentException.class, () -> new Link("", "Reference example"));
    }
  
    @Test
    @DisplayName("Test link cannot have null reference")
    public void linkCannotHaveNullReference() {
      assertThrows(IllegalArgumentException.class, () -> new Link("Text example", null));
    }
    
    @Test
    @DisplayName("Test link cannot have empty reference")
    public void linkCannotHaveEmptyReference() {
      assertThrows(IllegalArgumentException.class, () -> new Link("Text example", " "));
    }
  
    @Test
    @DisplayName("Test link cannot have blank reference")
    public void linkCannotHaveBlankReference() {
      assertThrows(IllegalArgumentException.class, () -> new Link("Text example", ""));
    }
    
    @Test
    @DisplayName("Test link cannot have null actions")
    public void linkCannotHaveNullAction() {
      assertThrows(IllegalArgumentException.class, () -> new Link("Text example", "Reference example", null));
    }
    
    @Test
    @DisplayName("Test add actions in Link declarations")
    public void isPossibleToAddNewActionsInList() {
      List<Action> actions = new ArrayList<>();
      actions.add(new ScoreAction(10));
      actions.add(new GoldAction(10));
      Link link = new Link("Text example", "Reference example", actions);
      assertEquals(2, link.getActions().size());
    }
  
    @Test
    @DisplayName("Test action list cannot add null values")
    public void linkActionListCannotAddNullValues() {
      List<Action> actions = new ArrayList<>();
      actions.add(null);
      assertThrows(IllegalArgumentException.class, () -> new Link("Text example", "Reference example", actions));
    }
  }

  @Nested
  @DisplayName("Test equal method")
  public class EqualsTest {
    @Test
    @DisplayName("Test links are equal if text and reference match")
    public void linksAreEqualIfTextAndReferenceMatch() {
      Link link1 = new Link("Same text", "Same reference");
      Link link2 = new Link("Same text", "Same reference");
      assertEquals(link1, link2);
    }
    
    @Test
    @DisplayName("Test links are equal if reference is equal")
    public void linksAreEqualIfTextDoNotMatch() {
      Link link1 = new Link("Text1", "Same reference");
      Link link2 = new Link("Text2", "Same reference");
      assertEquals(link1, link2);
    }
  
    @Test
    @DisplayName("Test links are not equal if reference is different")
    public void linksAreNotEqualIfReferenceDoNotMatch() {
      Link link1 = new Link("Same Text", "Reference1");
      Link link2 = new Link("Same text", "Reference2");
      assertNotEquals(link1, link2);
    }
  }

  @Nested
  @DisplayName("Test methods")
  public class MethodTest {
    @Test
    @DisplayName("Test add action after initialization")
    public void isPossibleToAddActionAfterInitialization() {
      Link link = new Link("Text example", "Reference example");
      link.addAction(new ScoreAction(10));
      link.addAction(new GoldAction(10));
      assertEquals (2, link.getActions().size());
    }
    @Test
    @DisplayName("Test to add action cannot have null values")
    public void addActionMethodCannotAddNullValues() {
      Link link = new Link("Text example", "Reference example");
      assertThrows(IllegalArgumentException.class, () -> link.addAction(null));
    }

    @Test
    @DisplayName("Test executeActions on player")
    public void executeActionsOnPlayer() {
      Link link = new Link("Text example", "Reference example", List.of(
          new GoldAction(10), new HealthAction(-10), new InventoryAction("Sword")
      ));
      Player player = new Player.Builder("Player 1").gold(0).health(100).build();
      link.executeActions(player);
      assertEquals(10, player.getGold());
      assertEquals(90, player.getHealth());
      assertEquals(1, player.getInventory().size());
    }

    @Test
    @DisplayName("Test toString method")
    public void toStringTest() {
      Link link = new Link("Text example", "Reference example");
      String expectedString = "Link: text = Text example, reference = Reference example";
      assertEquals(expectedString, link.toString());
    }
  }
}