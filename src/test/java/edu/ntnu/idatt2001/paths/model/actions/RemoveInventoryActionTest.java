package edu.ntnu.idatt2001.paths.model.actions;

import edu.ntnu.idatt2001.paths.model.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RemoveInventoryActionTest {
    private Player player;
    private Action action;
    private final String itemExample = "Diamond Pickaxe";

    @BeforeEach
    public void setup() {
        player = new Player.Builder("Player 1").build();
    }

    @Nested
    public class ConstructorTest {

        @Test
        public void actionCannotRemoveBlankItem() {
            assertThrows(IllegalArgumentException.class, () -> new RemoveInventoryAction(" "));
        }

        @Test
        public void actionCannotRemoveNullItem() {
            assertThrows(IllegalArgumentException.class, () -> new RemoveInventoryAction(null));
        }
    }

    @Nested
    public class ExecuteTest {

        @Test
        public void executeItemFromPlayer() {
            action = new RemoveInventoryAction(itemExample);
            player.addToInventory(itemExample);
            action.execute(player);
            assertTrue(player.getInventory().isEmpty());
        }

        @Test
        public void executeItemFromPlayerRepeatedly() {
            action = new RemoveInventoryAction(itemExample);
            player.addToInventory(itemExample);
            player.addToInventory(itemExample);
            player.addToInventory(itemExample);
            action.execute(player);
            action.execute(player);
            action.execute(player);
            assertTrue(player.getInventory().isEmpty());
        }
    }

    @Test
    public void toPathsFormatStringTest() {
        action = new RemoveInventoryAction(itemExample);
        assertTrue(action.toPathsFormatString().contains("RemoveInventoryAction"));
        assertTrue(action.toPathsFormatString().contains(itemExample));
    }
}
