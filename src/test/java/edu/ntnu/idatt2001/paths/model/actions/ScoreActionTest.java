package edu.ntnu.idatt2001.paths.model.actions;

import edu.ntnu.idatt2001.paths.model.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ScoreActionTest {

    private Player player;
    private Action action;

    @BeforeEach
    public void setup() {
        player = new Player.Builder("Player 1").build();
    }

    @Nested
    public class ExecuteTest {

        @Test
        public void executePositiveScoreToPlayer() {
            action = new ScoreAction(1);
            action.execute(player);
            assertTrue(player.getScore() == 1);
        }

        @Test
        public void executeNegativeScoreToPlayer() {
            player.addScore(2);
            action = new ScoreAction(-1);
            action.execute(player);
            assertTrue(player.getScore() == 1);
        }

        @Test
        public void executeScoreToPlayerRepeatedly() {
            action = new ScoreAction(1);
            action.execute(player);
            action.execute(player);
            action.execute(player);
            assertTrue(player.getScore() == 3);
        }
    }

    @Test
    public void testToPathsFormatString() {
        ScoreAction action = new ScoreAction(5);
        assertEquals("ScoreAction:5", action.toPathsFormatString());
    }
}
