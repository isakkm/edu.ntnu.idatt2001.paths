package edu.ntnu.idatt2001.paths.model.goals;

import edu.ntnu.idatt2001.paths.model.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class GoldGoalTest {

  @Nested
  @DisplayName("Constructor tests")
  public class ConstructorTest {

    @Test
    @DisplayName("Should throw IllegalArgumentException when minimumGold is less than 1")
    public void goldGoalCannotBeLessThan1() {
      assertThrows(IllegalArgumentException.class, () -> new GoldGoal(0));
    }
  }

  @Nested
  @DisplayName("Test for getter method")
  public class GettersTest {
    private GoldGoal goldGoal;

    @BeforeEach
    public void setUp() {
      goldGoal = new GoldGoal(10);
    }

    @Test
    @DisplayName("Should return minimumGold")
    public void getMinimumGold() {
      assertEquals(10, goldGoal.getGoalValue());
    }
  }

  @Nested
  @DisplayName("Test for isFulfilled method")
  public class IsFulfilledTest {
    private GoldGoal goldGoal;
    private Player player;

    @BeforeEach
    public void setUp() {
      player = new Player.Builder("Player 1").health(100).gold(0).build();
      goldGoal = new GoldGoal(10);
    }

    @Test
    @DisplayName("Should return true when player has more gold than minimumGold")
    public void playerHasMoreGoldThanMinimumGold() {
      player.addGold(15);
      assertTrue(goldGoal.isFulfilled(player));
    }

    @Test
    @DisplayName("Should return false when player has less gold than minimumGold")
    public void playerHasLessGoldThanMinimumGold() {
      player.addGold(5);
      assertFalse(goldGoal.isFulfilled(player));
    }
  }
}
