package edu.ntnu.idatt2001.paths.model.goals;

import edu.ntnu.idatt2001.paths.model.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HealthGoalTest {

  @Nested
  @DisplayName("Constructor tests")
  public class ConstructorTest {

    @Test
    @DisplayName("Should throw IllegalArgumentException when minimumHealth is less than 1")
    public void healthGoalCannotBeLessThan1() {
      assertThrows(IllegalArgumentException.class, () -> new HealthGoal(0));
    }
  }

  @Nested
  @DisplayName("Test for getter method")
  public class GettersTest {
    private HealthGoal healthGoal;

    @Test
    @DisplayName("Should return minimumHealth")
    public void getMinimumHealth() {
      healthGoal = new HealthGoal(10);
      assertEquals(10, healthGoal.getGoalValue());
    }
  }

  @Nested
  @DisplayName("Test for isFulfilled method")
  public class IsFulfilledTest {
    private HealthGoal healthGoal;
    private Player player;

    @BeforeEach
    public void setUp() {
      player = new Player.Builder("Player 1").health(100).gold(0).build();
      healthGoal = new HealthGoal(125);
    }

    @Test
    @DisplayName("Should return true when player has more health than minimumHealth")
    public void playerHasMoreHealthThanMinimumHealth() {
      player.addHealth(50);
      assertTrue(healthGoal.isFulfilled(player));
    }

    @Test
    @DisplayName("Should return false when player has less health than minimumHealth")
    public void playerHasLessHealthThanMinimumHealth() {
      player.addHealth(-50);
      assertFalse(healthGoal.isFulfilled(player));
    }
  }
}
