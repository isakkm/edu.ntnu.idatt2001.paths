package edu.ntnu.idatt2001.paths.model.actions;

import edu.ntnu.idatt2001.paths.model.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class InventoryActionTest {
    private Player player;
    private Action action;
    private final String itemExample = "Diamond Pickaxe";

    @BeforeEach
    public void setup() {
        player = new Player.Builder("Player 1").build();
    }

    @Nested
    public class ConstructorTest {

        @Test
        public void actionCannotHaveBlankItem() {
            assertThrows(IllegalArgumentException.class, () -> new InventoryAction(" "));
        }

        @Test
        public void actionCannotHaveNullItem() {
            assertThrows(IllegalArgumentException.class, () -> new InventoryAction(null));
        }
    }

    @Nested
    public class ExecuteTest {

        @Test
        public void executeItemToPlayer() {
            action = new InventoryAction(itemExample);
            action.execute(player);
            assertTrue(player.getInventory().containsKey(itemExample));
        }

        @Test
        public void executeItemToPlayerRepeatedly() {
            action = new InventoryAction(itemExample);
            action.execute(player);
            action.execute(player);
            action.execute(player);
            assertEquals(3, player.getInventory().get(itemExample));
        }
    }

    @Test
    public void toPathsFormatStringTest() {
        action = new InventoryAction(itemExample);
        assertEquals("InventoryAction:Diamond Pickaxe", action.toPathsFormatString());
    }
}
