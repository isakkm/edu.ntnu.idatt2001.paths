package edu.ntnu.idatt2001.paths.model.goals;

import edu.ntnu.idatt2001.paths.model.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class InventoryGoalTest {
  private List<String> goals;
  @BeforeEach
  public void setUp() {
    goals = new ArrayList<>();
  }
  
  @Nested
  public class ConstructorTest {
    
    @Test
    public void listCannotHaveNullItems() {
      goals.add(null);
      assertThrows(IllegalArgumentException.class, () -> new InventoryGoal(goals));
    }
  
    @Test
    public void listCannotHaveBlankItems() {
      goals.add(" ");
      assertThrows(IllegalArgumentException.class, () -> new InventoryGoal(goals));
    }
  
    @Test
    public void listCannotBeNull() {
      assertThrows(IllegalArgumentException.class, () -> new InventoryGoal(null));
    }
  
    @Test
    public void listCannotBeEmpty() {
      assertThrows(IllegalArgumentException.class, () -> new InventoryGoal(goals));
    }
  }

  @Nested
  @DisplayName("Test for getter method")
  public class GettersTest {
    private InventoryGoal goal;

    @BeforeEach
    public void setUp() {
      goals.add("Diamond Sword");
      goals.add("Diamond Pickaxe");
      goal = new InventoryGoal(goals);
    }

    @Test
    @DisplayName("Should return list of goals")
    public void getGoals() {
      assertEquals(String.valueOf(goals).replaceAll("[\\[\\]]", ""), goal.getGoalValue());
    }
  }


  @Nested
  public class isFulfilledTest {
    @Test
    public void playerHasAllMandatoryItems() {
      Map<String, Integer> mandatoryItems = Map.of("Diamond Sword", 1, "Diamond Pickaxe", 1);
      Player player = new Player.Builder("Player 1").health(100).inventory(mandatoryItems).build();
      InventoryGoal goal = new InventoryGoal(mandatoryItems.keySet().stream().toList());
      assertTrue(goal.isFulfilled(player));
    }
  
    @Test
    public void playerHasAllMandatoryItemsAndMore() {
      List<String> mandatoryItems = List.of("Diamond Sword", "Diamond Pickaxe");
      Map<String, Integer> playerItems = Map.of("Diamond Sword", 1, "Diamond Pickaxe", 1, "Totem of Undying", 1);
      Player player = new Player.Builder("Player 1").health(100).inventory(playerItems).build();
      InventoryGoal goal = new InventoryGoal(mandatoryItems);
      assertTrue(goal.isFulfilled(player));
    }
  
    @Test
    public void playerHasOnlyOneOfTheMandatoryItems() {
      List<String> mandatoryItems = List.of("Diamond Sword", "Diamond Pickaxe");
      Map<String, Integer> playerItems = Map.of("Diamond Sword", 1);
      Player player = new Player.Builder("Player 1").health(100).inventory(playerItems).build();
      InventoryGoal goal = new InventoryGoal(mandatoryItems);
      assertFalse(goal.isFulfilled(player));
    }
  }
}