package edu.ntnu.idatt2001.paths.io;

import edu.ntnu.idatt2001.paths.exceptions.UnplayableStoryException;
import edu.ntnu.idatt2001.paths.model.Link;
import edu.ntnu.idatt2001.paths.model.Passage;
import edu.ntnu.idatt2001.paths.model.Story;
import edu.ntnu.idatt2001.paths.model.actions.GoldAction;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class FileReaderTest {
  
  private final FileReader reader = new FileReader();
  private final File hauntedHouseExample = new File("src/test/resources/HauntedHouseExample.paths");
  private final File wrongFormatExample = new File("src/test/resources/WrongFormatExample.paths");
  private final File wrongStoryTitleFormatExample = new File("src/test/resources/WrongStoryTitleFormatExample.paths");
  
  @Nested
  @DisplayName("Test for the correct format")
  public class CorrectFormatTest {
    
    @Test
    @DisplayName("Test for correct title")
    public void checkStoryTitleTest() {
      try {
        Story story = reader.readStory(hauntedHouseExample);
        assertEquals("Haunted House", story.getTitle());
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test for correct opening passage")
    public void checkOpeningPassageTest() {
      try {
        Story story = reader.readStory(hauntedHouseExample);
        Passage openingPassage = new Passage("Beginnings", "You are in a small, dimly lit room. " +
                "There is a door in front of you.", List.of(new Link("Try to open the door", "Another room")));
        assertEquals(openingPassage, story.getOpeningPassage());
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      }
    }
  
    @Test
    @DisplayName("Test for correct amount of passages")
    public void checkAmountOfPassagesTest() {
      try {
        Story story = reader.readStory(hauntedHouseExample);
        assertEquals(2, story.getPassages().size());
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      }
    }
  
    @Test
    @DisplayName("Test for correct passage from link")
    public void checkPassageFromLinkTest() {
      try {
        Story story = reader.readStory(hauntedHouseExample);
        Passage passage = new Passage("Another room", "The door opens to another room. " +
                "You see a desk with a large, dusty book.",
                List.of(new Link("Open the book", "The book of spells"),
                        new Link("Go back", "Beginnings")));
        Link link = new Link("Try to open the door", "Another room");
        assertEquals(passage, story.getPassage(link));
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      }
    }
  }

  @Nested
  @DisplayName("Check extension for action support")
  public class ReadActionTest {

    @Test
    @DisplayName("Check for correct Action type in opening passage")
    public void CheckForCorrectActionTypeInOpeningPassageTest() {
      GoldAction goldAction = new GoldAction(20);
      try {
        assertEquals(goldAction.getClass(), reader.readStory(hauntedHouseExample).
            getOpeningPassage().getLinks().get(0).getActions().get(0).getClass());
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Check for correct Action value in opening passage")
    public void CheckForCorrectActionValueInOpeningPassageTest() {
      try {
        int goldActionValue = 20;
        String[] actionValues = reader.readStory(hauntedHouseExample).
            getOpeningPassage().getLinks().get(0).getActions().get(0).toPathsFormatString().split(":");
        int readGoldActionValue = Integer.parseInt(actionValues[1]);
            assertEquals(goldActionValue, readGoldActionValue);
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      }
    }
  }
  
  @Nested
  @DisplayName("Test for the incorrect format")
  public class IncorrectFormatTest {
    
    @Test
    @DisplayName("Test for exception thrown if file name is wrong")
    public void checkIncorrectFileNameTest() {
      assertThrows(RuntimeException.class, () -> reader.readStory(new File("src/test/resources/HauntedHouseExample.pathss")));
    }
  
    @Test
    @DisplayName("Test exception for wrong story title format")
    public void checkEmptyStoryTitleExceptionTest() {
      assertThrows(UnplayableStoryException.class, () -> reader.readStory(wrongStoryTitleFormatExample));
    }
  
    @Test
    @DisplayName("Test exception for wrong format with no empty lines")
    public void checkNoEmptyLinesBetweenPassagesTest() {
      try {
        reader.readStory(wrongFormatExample);
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      } catch (UnplayableStoryException e) {
        assertEquals(2, reader.getLineErrors().size());
      }
    }
  }
}
