package edu.ntnu.idatt2001.paths.io;

import edu.ntnu.idatt2001.paths.model.Link;
import edu.ntnu.idatt2001.paths.model.Passage;
import edu.ntnu.idatt2001.paths.model.Story;
import edu.ntnu.idatt2001.paths.model.actions.GoldAction;
import edu.ntnu.idatt2001.paths.model.actions.HealthAction;
import edu.ntnu.idatt2001.paths.model.actions.ScoreAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class FileWriterTest {
  
  private final FileReader fileReader = new FileReader();
  private final FileWriter fileWriter = new FileWriter();
  
  private final File writeFile = new File("src/test/resources/WriteFile.paths");
  File newFile = new File("src/test/resources/WriteFile2.paths");
  private Story story;
  private Passage openingPassage;
  private Passage youAreDead;
  private Passage belowEarth;
  
  @BeforeEach
  public void setUp() {
    // Opening passage
    openingPassage = new Passage("Alone in forrest", "You appear to be alone in a "
            + "forrest at night. You hear a roar from behind",
            List.of(new Link("Don't move", "You are dead"),
                    new Link("Run for it!", "Secret tunnel")));
    
    // Story
    story = new Story("The forrest of doom", openingPassage);
    
    // youAreDead passage
    youAreDead = new Passage("You are dead", "Welcome to heaven",
            List.of(new Link("Do it like Jesus", "Resurrection", List.of(new HealthAction(100),new GoldAction(10), new ScoreAction(100)))));
    story.addPassage(youAreDead);
    
    // belowEarth passage
    belowEarth = new Passage("Secret tunnel", "You arrive at a secret tunnel",
            List.of(new Link("Follow it down", "Below Earth"),
                    new Link("Find another escape", "You are dead")));
    story.addPassage(belowEarth);
  }
  
  @Nested
  @DisplayName("Test for the write story method")
  public class WriteStoryMethodTest {
    
    @Test
    @DisplayName("Test to write story and retrieve title")
    public void writeStoryAndRetrieveTitleTest() {
      try {
        fileWriter.writeStory(writeFile, story);
        assertEquals("The forrest of doom", fileReader.readStory(writeFile).getTitle());
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      }
    }
  
    @Test
    @DisplayName("Test to write story and retrieve opening passage")
    public void writeStoryAndRetrieveOpeningPassageTest() {
      try {
        fileWriter.writeStory(writeFile, story);
        assertEquals(openingPassage, fileReader.readStory(writeFile).getOpeningPassage());
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      }
    }
  
    @Test
    @DisplayName("Test to write story and retrieve passage from link")
    public void writeStoryAndRetrievePassageFromLinkTest() {
      try {
        fileWriter.writeStory(writeFile, story);
        assertEquals(youAreDead, fileReader.readStory(writeFile).getPassage(new Link("Don't move", "You are dead")));
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      }
    }
  
    @Test
    @DisplayName("Test to write story and retrieve different passage from link")
    public void writeStoryAndRetrievePassageFromLinkTest2() {
      try {
        fileWriter.writeStory(writeFile, story);
        assertEquals(belowEarth, fileReader.readStory(writeFile).getPassage(new Link("Run for it!", "Secret tunnel")));
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Test to write story without valid file extension throws exception")
    public void writeStoryWithoutValidFileThrowsException() {
      assertThrows(IOException.class, ()-> fileWriter.writeStory(new File("src/test/resources/WriteFile.wrongExtension"), story));
    }
  }
  
  @Nested
  @DisplayName("Test for the write story to new file method")
  public class WriteStoryToNewFileTest {
    
    
    @Test
    @DisplayName("Test to check if new file has been created")
    public void CheckIfNewFileHasBeenCreatedTest() {
      try {
        newFile.delete();
        newFile = new File("src/test/resources/WriteFile2.paths");
        boolean hasBeenCreated = fileWriter.writeStoryToNewFile(newFile, story);
        assertTrue(hasBeenCreated);
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      }
    }
  
    @Test
    @DisplayName("Test to check if new file already exist")
    public void CheckIfNewFileExistTest() {
      try {
        File newFile = new File("src/test/resources/WriteFile2.paths");
        assertFalse(fileWriter.writeStoryToNewFile(newFile, story));
      } catch (IOException e) {
        System.out.println("App crashed: " + e.getMessage());
      }
    }
    @Test
    @DisplayName("Test to write story without valid file extension throws exception")
    public void writeStoryWithoutValidFileThrowsException() {
      assertThrows(IOException.class, ()-> fileWriter.writeStoryToNewFile(new File("src/test/resources/WriteFile.wrongExtension"), story));
    }
  }
}
