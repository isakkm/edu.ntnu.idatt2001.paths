package edu.ntnu.idatt2001.paths.io;

import edu.ntnu.idatt2001.paths.model.Link;
import edu.ntnu.idatt2001.paths.model.Passage;
import edu.ntnu.idatt2001.paths.model.Story;
import edu.ntnu.idatt2001.paths.model.actions.GoldAction;
import edu.ntnu.idatt2001.paths.model.actions.HealthAction;
import edu.ntnu.idatt2001.paths.model.actions.ScoreAction;
import edu.ntnu.idatt2001.paths.model.game.Game;
import edu.ntnu.idatt2001.paths.model.game.GameLog;
import edu.ntnu.idatt2001.paths.model.goals.Goal;
import edu.ntnu.idatt2001.paths.model.goals.GoldGoal;
import edu.ntnu.idatt2001.paths.model.goals.HealthGoal;
import edu.ntnu.idatt2001.paths.model.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class ProgressSerializerTest {
  Game currentGame;
  GameLog gameLog;
  Passage activePassage;
  File file = new File("src/test/resources/SerializingTestFile.txt");
  
  @BeforeEach
  public void setUp() {
    // Opening passage
    Passage openingPassage = new Passage("Alone in forrest", "You appear to be alone in a "
            + "forrest at night. You hear a roar from behind",
            List.of(new Link("Don't move", "You are dead"),
                    new Link("Run for it!", "Secret tunnel")));
  
    // Story
    Story story = new Story("The forrest of doom", openingPassage);
  
    // youAreDead passage
    activePassage = new Passage("You are dead", "Welcome to heaven",
            List.of(new Link("Do it like Jesus", "Resurrection", List.of(new HealthAction(100),new GoldAction(10), new ScoreAction(100)))));
    story.addPassage(activePassage);
  
    // belowEarth passage
    Passage belowEarth = new Passage("Secret tunnel", "You arrive at a secret tunnel",
            List.of(new Link("Follow it down", "Below Earth"),
                    new Link("Find another escape", "You are dead", List.of(new HealthAction(100)))));
    story.addPassage(belowEarth);
    
    // Player
    Player player = new Player.Builder("Player 1").health(200).gold(1000).score(100).inventory(Map.of("Sword", 1, "Shield", 1)).build();
    
    // Goals
    List<Goal> goals = List.of(new HealthGoal(300), new GoldGoal(1100));
    
    currentGame = new Game(player, story, goals);
    gameLog = new GameLog();
  }
  
  
  
  @Nested
  @DisplayName("Serialize game progress test")
  public class SerializeGameProgressTest {
    
    @Test
    @DisplayName("Serialize active game passage test")
    public void serializeGameActivePassageTest() {
      try {
        ProgressSerializer.getInstance().serializeGameProgress(file, currentGame, activePassage, gameLog);
        List<Object> progress = (List<Object>) ProgressDeserializer.getInstance().deserializeGameProgress(file);
        Passage deserializedPassage = (Passage) progress.get(1);
        assertEquals(activePassage.getTitle(), deserializedPassage.getTitle());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  
    @Test
    @DisplayName("Serialize game opening passage test")
    public void serializeGameOpeningPassageTest() {
      try {
        ProgressSerializer.getInstance().serializeGameProgress(file, currentGame, activePassage, gameLog);
        List<Object> progress = (List<Object>) ProgressDeserializer.getInstance().deserializeGameProgress(file);
        Game deserializedGame = (Game) progress.get(0);
        assertEquals(currentGame.begin().getTitle(), deserializedGame.begin().getTitle());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
