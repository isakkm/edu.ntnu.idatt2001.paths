package edu.ntnu.idatt2001.paths.validation;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class FormatValidationTest {

  @Nested
  @DisplayName("Test for inventory text")
  public class InventoryTextTest {
    @Test
    @DisplayName("Test for valid inventory text")
    void testIsValidInventoryText() {
      assertTrue(FormatValidation.isValidInventoryText("Sword, Shield"));
      assertTrue(FormatValidation.isValidInventoryText("Sword, Shield, Key"));
    }

    @Test
    @DisplayName("Test for invalid inventory text")
    void testIsInvalidInventoryText() {
      assertFalse(FormatValidation.isValidInventoryText(""));
    }
  }


  @Nested
  @DisplayName("Test for positive number")
  public class PositiveNumberTest {

    @Test
    @DisplayName("Test for valid positive number")
    void testIsValidPositiveNumber() {
      assertTrue(FormatValidation.isValidPositiveNumber("1"));
      assertTrue(FormatValidation.isValidPositiveNumber("123"));
    }

    @Test
    @DisplayName("Test for invalid positive number")
    void testIsInvalidPositiveNumber() {
      assertFalse(FormatValidation.isValidPositiveNumber("-1"));
      assertFalse(FormatValidation.isValidPositiveNumber("1.2"));
      assertFalse(FormatValidation.isValidPositiveNumber("abc"));
    }
  }

  @Nested
  @DisplayName("Test for number input")
  public class NumberInputTest {
    @Test
    @DisplayName("Test for valid number input")
    void testIsValidNumberInput() {
      assertTrue(FormatValidation.isValidNumberInput("1"));
      assertTrue(FormatValidation.isValidNumberInput("123"));
      assertTrue(FormatValidation.isValidNumberInput("-1"));
    }

    @Test
    @DisplayName("Test for invalid number input")
    void testIsInvalidNumberInput() {
      assertFalse(FormatValidation.isValidNumberInput("1.2"));
      assertFalse(FormatValidation.isValidNumberInput("abc"));
    }
  }

  @Nested
  @DisplayName("Test for optional number input")
  public class OptionalNumberInputTest {
    @Test
    @DisplayName("Test for valid optional number input")
    void testIsValidOptionalNumberInput() {
      assertTrue(FormatValidation.isValidOptionalNumberInput("1"));
      assertTrue(FormatValidation.isValidOptionalNumberInput("123"));
      assertTrue(FormatValidation.isValidOptionalNumberInput("-12"));
      assertTrue(FormatValidation.isValidOptionalNumberInput(""));
    }

    @Test
    @DisplayName("Test for invalid optional number input")
    void testIsInvalidOptionalNumberInput() {
      assertFalse(FormatValidation.isValidOptionalNumberInput("1.2"));
      assertFalse(FormatValidation.isValidOptionalNumberInput("abc"));
    }
  }

  @Nested
  @DisplayName("Test for passage start")
  public class PassageStartTest {
    @Test
    @DisplayName("Test for valid passage start")
    void testIsValidPassageStart() {
      assertTrue(FormatValidation.isValidPassageStart("::Passage title"));
      assertTrue(FormatValidation.isValidPassageStart("::3rd Street"));
    }

    @Test
    @DisplayName("Test for invalid passage start")
    void testIsInvalidPassageStart() {
      assertFalse(FormatValidation.isValidPassageStart("::"));
      assertFalse(FormatValidation.isValidPassageStart(":Passage title"));
      assertFalse(FormatValidation.isValidPassageStart("Passage title"));
    }
  }

  @Nested
  @DisplayName("Test for actions")
  public class ActionTest {

    @Test
    @DisplayName("Test for valid action text")
    void testIsValidAction() {
      assertTrue(FormatValidation.isValidAction("{HealthAction:-10;ScoreAction:-5}"));
      assertTrue(FormatValidation.isValidAction("{HealthAction:10}"));
      assertTrue(FormatValidation.isValidAction("{GoldAction:-100}"));
      assertTrue(FormatValidation.isValidAction("{GoldAction:100}"));
      assertTrue(FormatValidation.isValidAction("{GoldAction:100;HealthAction:-10}"));
      assertTrue(FormatValidation.isValidAction("{GoldAction:100;HealthAction:-10;InventoryAction:Sword}"));
      assertTrue(FormatValidation.isValidAction("{InventoryAction:Sword}"));
      assertTrue(FormatValidation.isValidAction("{InventoryAction:Shield}"));
      assertTrue(FormatValidation.isValidAction("{HealthAction:-10;ScoreAction:-5}"));
      assertTrue(FormatValidation.isValidAction("{InventoryAction:Key;InventoryAction:Note}"));
    }

    @Test
    @DisplayName("Test for invalid action text")
    void testIsInvalidAction() {
      assertFalse(FormatValidation.isValidAction("{HealthAction:10.5}"));
      assertFalse(FormatValidation.isValidAction("{GoldAction:10.5}"));
      assertFalse(FormatValidation.isValidAction("{InventoryAction:10.5}"));
    }
  }

  @Nested
  @DisplayName("Test for valid link and action line")
  public class TestIsValidLinkAndActionLine {
    @Test
    @DisplayName("Test for valid link and action line")
    public void testIsValidLinkAndActionLine() {
      assertTrue(FormatValidation.isValidLinkAndActionLine("[\"Who are you?\"](Left alone to die){HealthAction:-10;ScoreAction:-5}"));
      assertTrue(FormatValidation.isValidLinkAndActionLine("[\"Who are you?\"](Left alone to die){HealthAction:-10}"));
      assertTrue(FormatValidation.isValidLinkAndActionLine("[Kill the old master](Death){HealthAction:-10}"));
      assertTrue(FormatValidation.isValidLinkAndActionLine("[Take right](The tunnel){ScoreAction:5 ; InventoryAction:Sword ; InventoryAction:Shield}"));
    }

    @Test
    @DisplayName("Test for invalid link and action line")
    public void testIsInvalidLinkAndActionLine() {
      assertFalse(FormatValidation.isValidLinkAndActionLine("[\"Who are you?\"](Left alone to die){HealthAction:-10;ScoreAction:-5"));
      assertFalse(FormatValidation.isValidLinkAndActionLine("[\"Who are you?\"](Left alone to die){HealthAction:-10ScoreAction:-5}"));
      assertFalse(FormatValidation.isValidLinkAndActionLine("[\"Who are you?\"](Left alone to die){HealthAction-10;ScoreAction:-5}"));
      assertFalse(FormatValidation.isValidLinkAndActionLine("[\"Who are you?\"(Left alone to die){HealthAction:-10;ScoreAction:-5}"));
      assertFalse(FormatValidation.isValidLinkAndActionLine("[\"Who are you?\"](Left alone to die{HealthAction:-10;ScoreAction:-5}"));
    }
  }
}