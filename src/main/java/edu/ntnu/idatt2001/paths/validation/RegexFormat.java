package edu.ntnu.idatt2001.paths.validation;

import java.util.Objects;

/**
 * Enum class for the different regular expressions used to check if format is valid.
 *
 * @author Jens
 * @version 1.0
 */
public enum RegexFormat {
  
  /**
   * Numbers:
   * Pattern:
   * <pre>123 or -123</pre>
   */
  NUMBER("-?\\d+"),
  
  /**
   * Positive Numbers:
   */
  POSITIVE_NUMBER("\\d+"),
  
  /**
   * Optional Numbers:
   * Pattern:
   * <pre>123 or -123 or nothing </pre>
   */
  OPTIONAL_NUMBER("(|-?\\d+)"), //(\d+)?
  
  /**
   * Goals text:
   * Pattern:
   * <pre>Sword</pre>
   * <pre>Sword, Shield</pre>
   * <pre>Sword, Shield, Armor,...</pre>
   */
  INVENTORY_TEXT("(\\s+)?[\\w\\s]+(((\\s+)?,(\\s+)?[\\w\\s]+)+)?"), // "(\s+)?[\w\s]+(((\s+)?,(\s+)?[\w\s]+)+)?"
  
  /**
   * Passage title.
   * Pattern:
   * <pre>::Passage title</pre>
   */
  PASSAGE_TITLE("^:{2}.{1,}$"),
  
  /**
   * Action.
   * Pattern:
   * <pre>{ActionType:value}</pre>
   * <pre>or {ActionType:value;ActionType:value}...</pre>
   * <pre>or {ActionType:value;ActionType:value;... }</pre>
   */
  ACTION("((\\s+)?\\{(\\s+)?\\}|((\\{(\\s+)?\\w+:-?([\\w\\s]+))((\\s+)?;(\\s+)?(\\w+:-?(?:\\d+|[\\w\\s]+))?)+(\\s+)?}|(\\{(\\s+)?\\w+:-?(\\d+|[\\w\\s]+))(\\s+)?})?)"),
  
  /**
   * Link and Action in one.
   * Pattern:
   * <pre>[link text](passage title){ActionType:value}</pre>
   * <pre>or [link text](passage title){ActionType:value;ActionType:value;...}</pre>
   * <pre>or [link text](passage title) {ActionType:value ; ActionType:value ; ...}</pre>
   * <pre>or [link text] (passage title) {ActionType:value}...</pre>
   * <pre>or [link text](passage title){}</pre>
   * <pre>or [link text](passage title)</pre>
   */
  LINK_ACTION("((\\[[^\\]]+\\](\\s?)+\\(([^)]+)?\\))((\\s+)?\\{(\\s+)?\\}|((\\{(\\s+)?\\w+:-?([\\w\\s]+))((\\s+)?;(\\s+)?(\\w+:-?(?:\\d+|[\\w\\s]+))?)+(\\s+)?}|(\\{(\\s+)?\\w+:-?(\\d+|[\\w\\s]+))(\\s+)?})?))");

  private final String pattern;

  /**
   * Constructor for RegexEnum.
   *
   * @param pattern The pattern to be used.
   */
  RegexFormat(String pattern) {
    this.pattern = pattern;
  }

  /**
   * Get the pattern.
   *
   * @return The pattern.
   */
  public String getPattern() {
    return pattern;
  }
  
  /**
   * Check if a string matches chosen pattern.
   *
   * @param string The string to check.
   * @param pattern The pattern to check with.
   * @return Predicate of whether the string matches the pattern.
   * @throws NullPointerException If the string is null.
   */
  public static boolean matches(final String string, final RegexFormat pattern) {
    return Objects.requireNonNull(string, "The string cannot be null.")
            .matches(pattern.getPattern());
  }
}