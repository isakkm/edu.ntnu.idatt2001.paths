package edu.ntnu.idatt2001.paths.validation;

import edu.ntnu.idatt2001.paths.exceptions.WrongStoryFormatException;

import java.util.Scanner;

/**
 * Represents the format validator for this application.
 * This class contains several methods to check if the lines
 * the FileReader must parse is on correct format.
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 1.0
 * @see RegexFormat
 */
public class FormatValidation {
  
  /**
   * Method to check if a line is on a valid inventory-text format.
   *
   * @param line String - the line to check.
   * @return boolean - true if the line is valid, false if not.
   */
  public static boolean isValidInventoryText(final String line) {
    return RegexFormat.matches(line, RegexFormat.INVENTORY_TEXT);
  }
  
  /**
   * Method to check if a line is on a valid positive number format.
   *
   * @param line String - the line to check.
   * @return boolean - true if the line is valid, false if not.
   */
  public static boolean isValidPositiveNumber(final String line) {
    return RegexFormat.matches(line, RegexFormat.POSITIVE_NUMBER);
  }
  
  /**
   * Method to check if a line is valid number input.
   *
   * @param line String - the line to check.
   * @return boolean - true if the line is valid, false if not
   */
  public static boolean isValidNumberInput(final String line) {
    return RegexFormat.matches(line, RegexFormat.NUMBER);
  }
  
  /**
   * Method to check if a line is a valid optional number input.
   *
   * @param line String - the line to check.
   * @return boolean - true if the line is valid, false if not
   */
  public static boolean isValidOptionalNumberInput(final String line) {
    return RegexFormat.matches(line, RegexFormat.OPTIONAL_NUMBER);
  }
  
  /**
   * Method to check if a line is a valid passage start.
   * @param line String - the line to check.
   * @return boolean - true if the line is valid, false if not
   * @throws NullPointerException if the line is null
   */
  public static boolean isValidPassageStart(final String line) throws NullPointerException {
    return RegexFormat.matches(line, RegexFormat.PASSAGE_TITLE);
  }
  
  /**
   * Method to check if a line is on valid action format.
   * @param line String - the line to check.
   * @return boolean - true if the line is valid, false if not
   * @throws NullPointerException if the line is null
   */
  public static boolean isValidAction(final String line) throws NullPointerException {
    return RegexFormat.matches(line, RegexFormat.ACTION);
  }
  
  /**
   * Method to check if a link with actions is a valid format.
   * @param line String - the action to check.
   * @return boolean - true if the action is valid, false if not
   * @throws NullPointerException if the action is null
   */
  public static boolean isValidLinkAndActionLine(final String line) throws NullPointerException {
    return RegexFormat.matches(line, RegexFormat.LINK_ACTION);
  }
  
  /**
   * Method to check if there is at least one line separation between passages.
   * @param line String - the line to check.
   * @throws WrongStoryFormatException if the line after ending passage is not empty.
   */
  public static boolean isValidLineSeparation(Scanner scanner, final String line, int currentLine) throws WrongStoryFormatException {
    if (scanner.hasNextLine() && !line.isEmpty()) {
      throw new WrongStoryFormatException("Error in line " + currentLine + ": "
      + line + " - passages must be separated");
    }
    return scanner.hasNextLine() && !line.isEmpty();
  }
  
  /**
   * Method to check if an action value is valid according to action type.
   * @param value String - the value to check.
   * @throws NumberFormatException if the value cannot be parsed into a number.
   */
  public static boolean isValidActionValues(String value, int currentLine, String action) throws NumberFormatException {
    if (!RegexFormat.matches(value, RegexFormat.NUMBER)) {
      throw new NumberFormatException("Error in line " + currentLine + ": " + action + ":" + value
              + " - invalid action value");
    }
    return RegexFormat.matches(value, RegexFormat.NUMBER);
  }
}
