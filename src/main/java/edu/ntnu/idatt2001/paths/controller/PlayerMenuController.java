package edu.ntnu.idatt2001.paths.controller;

import edu.ntnu.idatt2001.paths.validation.FormatValidation;
import edu.ntnu.idatt2001.paths.model.player.Player;
import edu.ntnu.idatt2001.paths.view.AlertDialog;
import edu.ntnu.idatt2001.paths.view.App;
import edu.ntnu.idatt2001.paths.view.element.TextFieldElement;
import edu.ntnu.idatt2001.paths.view.panes.PlayerMenuPane;
import javafx.event.Event;
import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the player menu controller.
 * This controller is responsible for the creation of a player.
 * Inherits properties from the AbstractController.
 * Corresponds to the PlayerMenuPane class.
 *
 * @author Jens Chrsitian Aanestad
 * @author Isak Kallestad Mandal
 * @version 1.0
 * @see PlayerMenuPane
 * @see AbstractController
 */
public class PlayerMenuController extends AbstractController {
  
  /**
   * Represents the action to be executed when the choose-goals-button is pressed.
   * Checks if a player already is confirmed, and if not recommends the
   * user to confirm player before choosing goals.
   *
   * @param event Event.
   */
  public void onChooseGoalsButtonPressed(Event event) {
    if (App.getPlayer() == null) {
      if (!AlertDialog.showYesNoConfirmationDialog("No player confirmed yet",
              "It is recommended that you create a player before you create goals. "
                      + "Do you want to do this?")) {
        switchToGoalsMenuPane(event);
      }
    } else if (App.getGoals() != null) {
      if (AlertDialog.showYesNoConfirmationDialog("Goals already exist"
              , "There already exists a set of goals. Do you still want to edit goals?")) {
        switchToGoalsMenuPane(event);
      }
    } else {
      switchToGoalsMenuPane(event);
    }
  }
  
  /**
   * Represents the action to be executed when the confirm-player-button is pressed.
   * Validates the input from text fields.
   *
   * @param event Event.
   * @param playerNameTextField TextFiled - the player name text field.
   * @param playerHealthTextField TextFiled - the player health text field.
   * @param playerGoldTextField TextFiled - the player gold text field.
   */
  public void onConfirmPlayerButtonPressed(Event event, TextField playerNameTextField
          , TextField playerHealthTextField, TextField playerGoldTextField) {
    checkForInvalidInput(playerNameTextField, playerHealthTextField, playerGoldTextField);
  }
  
  /**
   * Private method to validate text field values from the PlayerMenuPane. If one
   * or more text field value is invalid, an adapted warning message will be displayed.
   * The text fields that potentially has an invalid value will be turned red. If there are
   * no invalid values then the game's player will be set.
   *
   * @param playerNameTextField TextFiled - the player name text field.
   * @param playerHealthTextField TextFiled - the player health text field.
   * @param playerGoldTextField TextFiled - the player gold text field.
   */
  private void checkForInvalidInput(TextField playerNameTextField
          , TextField playerHealthTextField, TextField playerGoldTextField) {
    if (playerNameTextField.getText().isBlank()
            || !FormatValidation.isValidOptionalNumberInput(playerHealthTextField.getText())
            || !FormatValidation.isValidOptionalNumberInput(playerGoldTextField.getText())) {
      List<String> invalidInputs = new ArrayList<>();
      if (playerNameTextField.getText().isBlank()) {
        TextFieldElement.markTextFieldRed(playerNameTextField);
        invalidInputs.add("Name");
      } else {
        TextFieldElement.resetTextField(playerNameTextField);
      }
      if (!FormatValidation.isValidOptionalNumberInput(playerHealthTextField.getText())) {
        TextFieldElement.markTextFieldRed(playerHealthTextField);
        invalidInputs.add("Health");
      } else {
        TextFieldElement.resetTextField(playerHealthTextField);
      }
      if (!FormatValidation.isValidOptionalNumberInput(playerGoldTextField.getText())) {
        TextFieldElement.markTextFieldRed(playerGoldTextField);
        invalidInputs.add("Gold");
      } else {
        TextFieldElement.resetTextField(playerGoldTextField);
      }
      AlertDialog.showWarningDialog("Invalid input", getWarningMessage(invalidInputs));
    }
    else {
      try {
        if (playerHealthTextField.getText().isBlank()) {
          playerHealthTextField.setText(String.valueOf(100));
        }
        if (playerGoldTextField.getText().isBlank()) {
          playerGoldTextField.setText(String.valueOf(0));
        }
        App.setPlayer(new Player.Builder(playerNameTextField.getText())
                .health(Integer.parseInt(playerHealthTextField.getText()))
                .gold(Integer.parseInt(playerGoldTextField.getText())).build());
        AlertDialog.showInformationDialog("Player Confirmed", "The player is officially confirmed");
        
        TextFieldElement.markTextFieldGreen(playerNameTextField);
        TextFieldElement.markTextFieldGreen(playerHealthTextField);
        TextFieldElement.markTextFieldGreen(playerGoldTextField);
        
      } catch (IllegalArgumentException e) {
        AlertDialog.showWarningDialog("Illegal Argument", e.getMessage());
      }
    }
  }
  
  /**
   * Returns a message string for the warning dialog to be displayed if
   * text field values are invalid.
   *
   * @return String - the warning message for criteria not met.
   */
  private String getWarningMessage(List<String> invalidInputs) {
    String warningMessage = "";
    if (invalidInputs.size() == 1) {
      warningMessage = invalidInputs.get(0) + " is invalid.";
    } else if (invalidInputs.size() == 2) {
      warningMessage = invalidInputs.get(0) + " and " + invalidInputs.get(1) + " are invalid.";
    } else {
      warningMessage = invalidInputs.get(0) + ", " + invalidInputs.get(1) +  " and " + invalidInputs.get(2) + " are invalid.";
    }
    return warningMessage;
  }
}