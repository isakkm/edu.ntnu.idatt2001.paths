package edu.ntnu.idatt2001.paths.controller;

import edu.ntnu.idatt2001.paths.exceptions.UnplayableStoryException;
import edu.ntnu.idatt2001.paths.exceptions.WrongFileExtensionException;
import edu.ntnu.idatt2001.paths.io.FileReader;
import edu.ntnu.idatt2001.paths.io.ProgressDeserializer;
import edu.ntnu.idatt2001.paths.model.game.Game;
import edu.ntnu.idatt2001.paths.model.Story;
import edu.ntnu.idatt2001.paths.view.AlertDialog;
import edu.ntnu.idatt2001.paths.view.App;
import edu.ntnu.idatt2001.paths.view.panes.ImportStoryPane;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.Event;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Represents the import story controller.
 * This controller is responsible for importing stories and catch wrong formats.
 * Inherits properties form the AbstractController.
 * Corresponds to the ImportGame class.
 *
 * @author Jens Christian Aanestad
 * @version 0.1
 * @see edu.ntnu.idatt2001.paths.io.FileReader
 * @see ImportStoryPane
 * @see AbstractController
 */
public class ImportStoryController extends AbstractController {
  
  private FileReader fileReader = new FileReader();
  
  private final BooleanProperty storyImportedCorrectly = new SimpleBooleanProperty(false);
  
  /**
   * Represents the action to be executed when the import-story-button is pressed.
   * Opens a file chooser and filters after .paths extension. Handles potential
   * format exceptions when trying to read the story by showing warning dialogs.
   *
   * @param event Event - The event that triggered the action.
   * @param fileName Label - the file name label to update.
   * @param location Label - the file location label to update.
   * @param gridPane GridPane - the grid pane to show errors in file.
   * @param scrollPane ScrollPane - the scroll pane to turn visible.
   */
  public void onImportStoryButtonPressed(Event event, Label fileName, Label location, GridPane gridPane, ScrollPane scrollPane) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Open Story File");
    File file = fileChooser.showOpenDialog(new Stage());
    
    if (file != null) {
      try {
        fileName.setText("File name: " + file.getName());
        location.setText("Location: " + file.getPath());
        fileReader = new FileReader();
        Story story = fileReader.readStory(file);
        storyImportedCorrectly.set(true);
        fileReader.getLineErrors().forEach(System.out::println);
        App.setGame(new Game(App.getPlayer(), story, App.getGoals()));
        App.getGameLog().addLog(App.getGame().begin(), App.getGame().getPlayer());
        resetGridPane(gridPane);
        scrollPane.setVisible(false);
        AlertDialog.showInformationDialog("Story is playable",
                "The imported story is playable.");
      }
      catch (IOException e) {
        storyImportedCorrectly.set(false);
        AlertDialog.showWarningDialog("Unexpected error", e.getMessage());
      }
      catch (WrongFileExtensionException e) {
        storyImportedCorrectly.set(false);
        AlertDialog.showWarningDialog("Wrong file extension", e.getMessage());
      }
      catch (UnplayableStoryException e) {
        addErrorLineInGridPane(gridPane, fileReader.getLineErrors());
        storyImportedCorrectly.set(false);
        scrollPane.setVisible(true);
        AlertDialog.showWarningDialog("Error in paths file", e.getMessage());
      }
    }
  }
  
  /**
   * Adds error lines labels in the grid pane.
   *
   * @param gridPane GridPane - the grid pane to add error lines in.
   * @param lineOfError List{@literal <}String> - the list of error lines.
   */
  private void addErrorLineInGridPane(GridPane gridPane, List<String> lineOfError) {
    // Resets the grid pane.
    resetGridPane(gridPane);
    
    int rowIndex = 0;
    for (String line : lineOfError) {
      Label errorLine = new Label(line);
      errorLine.setStyle("-fx-text-fill: white; -fx-font-size: 20");
      gridPane.getRowConstraints().add(new RowConstraints(30));
      gridPane.add(errorLine, 0, rowIndex);
      rowIndex++;
    }
  }
  
  /**
   * Resets the grid pane by removing all belonging children.
   *
   * @param gridPane GridPane - the grid pane to remove all.
   */
  private void resetGridPane(GridPane gridPane) {
    gridPane.getChildren().removeAll(gridPane.getChildren());
    gridPane.getRowConstraints().removeAll(gridPane.getRowConstraints());
  }
  
  /**
   * Represents the action to be executed when the load-progress-button is
   * pressed. Deserializes the import-game-progress file abd loads the game
   * from that file.
   *
   * @param event Event.
   */
  public void onLoadProgressButtonPressed(Event event) {
    try {
      List<Object> progressList = (List<Object>) ProgressDeserializer.getInstance()
              .deserializeGameProgress(App.getImportedGameProgress());
      switchToGamePane(event, App.loadGame(progressList));
    } catch (IOException e) {
      AlertDialog.showWarningDialog("Unexpected error",
              "Could not play main story due to an unexpected error in the file loading");
    }
  }
  
  /**
   * Method to get the story imported correctly.
   *
   * @return BooleanProperty - The story imported correctly.
   */
  public BooleanProperty getStoryImportedCorrectly() {
    return storyImportedCorrectly;
  }
}
