package edu.ntnu.idatt2001.paths.controller;

import edu.ntnu.idatt2001.paths.io.ProgressSerializer;
import edu.ntnu.idatt2001.paths.model.game.GameLog;
import edu.ntnu.idatt2001.paths.model.actions.ResetAllAction;
import edu.ntnu.idatt2001.paths.view.AlertDialog;
import edu.ntnu.idatt2001.paths.view.App;
import edu.ntnu.idatt2001.paths.view.panes.GamePane;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.Event;
import javafx.scene.control.Button;

import java.io.IOException;

/**
 * Represents the hamburger menu pane controller.
 * This controller is responsible the hamburger panes button actions.
 * Inherits properties from the AbstractController.
 * Corresponds to the HamburgerMenuPane class.
 *
 * @author Jens Chrsitian Aanestad
 * @version 1.0
 * @see edu.ntnu.idatt2001.paths.view.panes.HamburgerMenuPane
 * @see AbstractController
 */
public class HamburgerMenuController extends AbstractController {
  BooleanProperty validUndo = new SimpleBooleanProperty(App.getGameLog().isValidUndo());
  
  /**
   * Represents the action to be executed when the continue button is pressed.
   * Simulates the action when pressing the hamburger menu icon  in the game pane.
   *
   * @param event Event.
   */
  public void onContinueButtonPressed(Event event) {
    GameController.onHamburgerMenuIconPressed(event, GamePane.getActivePassagePane().getHamburgerMenuPane());
  }
  
  /**
   * Represents the action to be executed when the restart button is pressed.
   * Resets all players attributes and switches the pane to the game pane with opening passage.
   *
   * @param event Event.
   */
  public void onRestartButtonPressed(Event event) {
    GameController.onHamburgerMenuIconPressed(event, GamePane.getActivePassagePane().getHamburgerMenuPane());
    ResetAllAction reset = new ResetAllAction();
    reset.execute(App.getGame().getPlayer());
    App.setGameLog(new GameLog());
    App.getGameLog().addLog(App.getGame().begin(), App.getGame().getPlayer());
    switchToGamePane(event, App.getGame().getStory().getOpeningPassage());
  }
  
  /**
   * Represents the action to be executed when the save button is pressed.
   * Checks which game the user is on and saves by serializing the game and
   * the active passage.
   *
   * @param event Event.
   */
  public void onSaveProgressButtonPressed(Event event) {
    if (App.getGame().getStory().getTitle().equals("The Forrest of Doom")) {
      try {
        ProgressSerializer.getInstance().serializeGameProgress(App.getMainGameProgress(),
                App.getGame(), GamePane.getActivePassage(), App.getGameLog());
        AlertDialog.showInformationDialog("Progress saved",
                "The progress was successfully saved");
      } catch (IOException e) {
        AlertDialog.showWarningDialog("Save attempt failed",
                "Could unfortunately not save progress.");
      }
    } else {
      try {
        ProgressSerializer.getInstance().serializeGameProgress(App.getImportedGameProgress(),
                App.getGame(), GamePane.getActivePassage(), App.getGameLog());
        AlertDialog.showInformationDialog("Progress saved",
                "The progress was successfully saved");
      } catch (IOException e) {
        AlertDialog.showWarningDialog("Save attempt failed",
                "Could unfortunately not save progress.");
      }
    }
  }
  
  /**
   * Represent the action to be executed when the undo-button is pressed.
   * Sets the game's player's attributes to previous state, and switches
   * passage pane to previous passage.
   *
   * @param event Event.
   */
  public void onUndoButtonPressed(Event event) {
    GameController.onHamburgerMenuIconPressed(event, GamePane.getActivePassagePane().getHamburgerMenuPane());
    switchToGamePane(event, App.getGameLog().undo(App.getGame()));
  }
  
  /**
   * Represents the action to be executed when the main menu button is pressed.
   * Stops the game, resets player, and switches pane to the main menu pane.
   *
   * @param event Event.
   */
  public void onMainMenuButtonPressed(Event event) {
    GameController.onHamburgerMenuIconPressed(event, GamePane.getActivePassagePane().getHamburgerMenuPane());
    ResetAllAction reset = new ResetAllAction();
    App.setGameLog(new GameLog());
    reset.execute(App.getGame().getPlayer());
    switchToMainMenuPane(event);
  }
  
  /**
   * Returns a boolean-property for a valid undo. Returns true if undo is valid.
   *
   * @return validUndo BooleanProperty - the booleanProperty for valid undo.
   */
  public BooleanProperty getValidUndo() {
    return validUndo;
  }
}
