package edu.ntnu.idatt2001.paths.controller;

import edu.ntnu.idatt2001.paths.model.goals.GoldGoal;
import edu.ntnu.idatt2001.paths.model.goals.HealthGoal;
import edu.ntnu.idatt2001.paths.model.goals.InventoryGoal;
import edu.ntnu.idatt2001.paths.model.goals.ScoreGoal;
import edu.ntnu.idatt2001.paths.view.App;
import edu.ntnu.idatt2001.paths.view.Tools;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.event.Event;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * Represents the game controller.
 * This controller is responsible for updating player stats and passages.
 * Corresponds to the GamePane class.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 1.0
 * @see edu.ntnu.idatt2001.paths.view.panes.GamePane
 */
public class GameController {
  private static int menuPressedNumber = 0;
  
  /**
   * Represents the action to be executed when the hamburger menu icon is clicked.
   * Plays a button sound and displays a menu pane with buttons.
   *
   * @param event Event - the event
   * @param hamburgerMenuPane StackPane - the hamburger menu pane to be displayed.
   */
  public static void onHamburgerMenuIconPressed(Event event, StackPane hamburgerMenuPane) {
    Tools.playButtonSound();
    hamburgerMenuPane.setVisible(menuPressedNumber % 2 == 0);
    menuPressedNumber++;
  }
  
  /**
   * Initializes the display of a goals-list to a VBox pane.
   *
   * @param goalsVBox VBox - the goalsVBox to add the goals-list to.
   */
  public void initializeGoals(VBox goalsVBox) {
    App.getGame().getGoals().forEach(goal -> {
      String goalString = goal.isFulfilled(App.getGame().getPlayer()) ? "☑" : "☐";
      if (!goal.getClass().equals(InventoryGoal.class)) {
        Label goalLabel = new Label();
        goalLabel.setStyle("-fx-text-fill: white; -fx-font-size: 20");
        if (GoldGoal.class.equals(goal.getClass())) {
          goalString += " Collect " + goal.getGoalValue() + " gold";
        } else if (HealthGoal.class.equals(goal.getClass())) {
          goalString += " Collect " + goal.getGoalValue() + " hearts";
        } else if (ScoreGoal.class.equals(goal.getClass())) {
          goalString += " Collect " + goal.getGoalValue() + " score";
        }
        goalLabel.setText(goalString);
        goalsVBox.getChildren().add(goalLabel);
      } else {
        Label inventoryGoal = new Label(goalString + " Collect items: ");
        inventoryGoal.setStyle("-fx-text-fill: white; -fx-font-size: 20");
        goalsVBox.getChildren().add(inventoryGoal);
        List<Label> items = new ArrayList<>();
        String itemString = (String) goal.getGoalValue();
        Arrays.stream(itemString.split("(, )")).toList().forEach(item -> {
          Label itemLabel = new Label("     \u2022 "+ item);
          itemLabel.setStyle("-fx-text-fill: white; -fx-font-size: 20");
          goalsVBox.getChildren().add(itemLabel);
        });
      }
    });
  }
  
  /**
   * Initializes the display of the player's inventory items to a VBox pane.
   *
   * @param inventoryVBox VBox - the vbox to add inventory items to.
   */
  public void initializeInventory(VBox inventoryVBox) {
    inventoryVBox.getChildren().clear();
    App.getGame().getPlayer().getInventory().forEach((item, value) -> {
      String itemString = value == 1 ? "\u2022 " + item : "\u2022 " + item + " #" + value;
      Label itemLabel = new Label(itemString);
      itemLabel.setWrapText(true);
      itemLabel.setPadding(new Insets(0, 0, 0, 30));
      itemLabel.setStyle("-fx-text-fill: white; -fx-font-size: 24");
      inventoryVBox.getChildren().add(itemLabel);
    });
  }
}
