package edu.ntnu.idatt2001.paths.controller;

import edu.ntnu.idatt2001.paths.view.AlertDialog;
import edu.ntnu.idatt2001.paths.view.App;
import edu.ntnu.idatt2001.paths.view.element.ButtonElement;
import edu.ntnu.idatt2001.paths.view.panes.*;
import javafx.event.Event;
import javafx.scene.control.Button;

import java.awt.*;
import java.net.URL;

/**
 * Represents the start menu controller.
 * This controller class is responsible for handling the actions for the start menu scene.
 * Inherits properties form the AbstractController.
 * Corresponds to the MainMenuPane class.
 *
 * @author jensc
 * @version 1.0
 * @see MainMenuPane
 * @see AbstractController
 */
public class MainMenuController extends AbstractController {
  
  /**
   * Checks if criteria for importing story is met before switching
   * to the import game scene.
   *
   * @param event Event.
   * @param playerButton Button - changes border color for button if player is null.
   * @param goalsButton Button - changes border color for button if goals is null or has size 0.
   */
  public void onImportStoryButtonPressed(Event event, Button playerButton, Button goalsButton) {
    if (checkCriteriaToPlayGame(event, playerButton, goalsButton)) {
      switchToImportGamePane(event);
    } else {
      AlertDialog.showWarningDialog("Criteria not met", getCriteriaMessage() + "import story.");
    }
  }
  
  /**
   * Represents the actions to be executed when the play-main-story button is pressed.
   * Checks if criteria for playing is met before switching pane to the play main story menu pane.
   * Starts game if criteria is met.
   *
   * @param event Event.
   * @param playerButton Button - changes border color for button if player is null.
   * @param goalsButton Button - changes border color for button if goals is null or has size 0.
   */
  public void onPlayMainStoryButtonPressed(Event event, Button playerButton, Button goalsButton) {
    if (checkCriteriaToPlayGame(event, playerButton, goalsButton)) {
      switchToPlayMainStoryMenuPane(event);
    } else {
      AlertDialog.showWarningDialog("Criteria not met", getCriteriaMessage() + "play main story.");
    }
  }
  
  /**
   * Represents the action  to be executed when the help button is pressed.
   * Opens the user manual pdf on the users web browser.
   *
   * @param event Event.
   */
  public void onHelpButtonPressed(Event event) {
    try {
      Desktop.getDesktop().browse(new URL(
              "https://gitlab.stud.idi.ntnu.no/isakkm/edu.ntnu.idatt2001.paths/-/wikis/uploads/"
                      + "89a098129ebe2f7508352426dd16274b/TheForrestOfDoomBrukerManual.pdf").toURI());
    } catch (Exception e) {
      AlertDialog.showWarningDialog("Could not open the help manual in your browser",
              "\nConsider to exit the application and try again");
    }
  }
  
  /**
   * Checks if the criteria for importing story or playing main story is met. If
   * not, then the border color for the button matching criteria will be colored red.
   * Returns true if criteria is met.
   *
   * @param event Event
   * @param playerButton Button - changes border color for button if player is null.
   * @param goalsButton Button - changes border color for button if goals is null or has size 0.
   * @return boolean - true if criteria is met.
   */
  public boolean checkCriteriaToPlayGame(Event event, Button playerButton, Button goalsButton) {
    if (App.getPlayer() == null) {
      ButtonElement.markButtonRed(playerButton);
    } else {
      ButtonElement.resetButton(playerButton);
    }
    if (App.getGoals() == null || App.getGoals().size() == 0) {
      ButtonElement.markButtonRed(goalsButton);
    } else {
      ButtonElement.resetButton(goalsButton);
    }
    
    return (App.getPlayer() != null && (App.getGoals() != null && App.getGoals().size() != 0));
  }
  
  /**
   * Returns a message string for the warning dialog to be displayed if criteria is not met
   *
   * @return String - the warning message for criteria not met.
   */
  private String getCriteriaMessage() {
    String criteriaMessage = "";
    if (App.getPlayer() == null && (App.getGoals() == null || App.getGoals().size() == 0)) {
      criteriaMessage = "You must choose a player and goals before you are allowed to ";
    } else if (App.getPlayer() == null) {
      criteriaMessage = "You must choose a player before you are allowed to ";
    } else if (App.getGoals() == null || App.getGoals().size() == 0) {
      criteriaMessage = "You must choose goals before you are allowed to ";
    }
    
    return criteriaMessage;
  }
}
