package edu.ntnu.idatt2001.paths.controller;

import edu.ntnu.idatt2001.paths.io.FileReader;
import edu.ntnu.idatt2001.paths.io.ProgressDeserializer;
import edu.ntnu.idatt2001.paths.model.game.Game;
import edu.ntnu.idatt2001.paths.model.game.GameLog;
import edu.ntnu.idatt2001.paths.view.AlertDialog;
import edu.ntnu.idatt2001.paths.view.App;
import javafx.event.Event;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Represents the play main story menu controller.
 * This is responsible for controlling input in the play main story menu.
 * Inherits properties from the AbstractController.
 *
 * @author Jens Christian Aanestad
 * @version 1.0
 * @see AbstractController
 * @see edu.ntnu.idatt2001.paths.view.panes.PlayMainStoryMenuPane
 */
public class PlayMainStoryMenuController extends AbstractController {
  FileReader fileReader = new FileReader();
  
  /**
   * Represents the actions to be executed when the play-main-story button is pressed.
   * Checks if criteria for playing is met before starting new main story.
   * Starts game if criteria is met.
   *
   * @param event Event.
   */
  public void onNewGameButtonPressed(Event event) {
      try {
        App.setGame(new Game(App.getPlayer(), fileReader.readStory(
                new File("src/main/resources/pathsFiles/ForrestOfDoom.paths")), App.getGoals()));
        switchToGamePane(event);
        App.setGameLog(new GameLog());
        App.getGameLog().addLog(App.getGame().begin(), App.getGame().getPlayer());
      } catch (IOException e) {
        AlertDialog.showWarningDialog("Unexpected error",
                "Could not play main story due to an unexpected error in the file loading");
      }
  }
  
  /**
   * Represents the actions to be executed when the load game story button is pressed.
   * Deserializes the importedGameProgress file and loads the game if saved game exists.
   *
   * @param event Event.
   */
  public void onLoadGameButtonPressed(Event event) {
    try {
      if (App.getMainGameProgress().length() != 0) {
        List<Object> progressList = (List<Object>) ProgressDeserializer.getInstance()
                .deserializeGameProgress(App.getMainGameProgress());
        switchToGamePane(event, App.loadGame(progressList));
      } else {
        AlertDialog.showWarningDialog("Cannot load game",
                "Cannot load game because you have not saved any game.");
      }
    } catch (IOException e) {
      AlertDialog.showWarningDialog("Unexpected error",
              "Could not play main story due to an unexpected error in the file loading");
    }
  }
  
  /**
   * Represents the action to be executed when the main menu button is pressed.
   * Switches pane to the main menu pane.
   *
   * @param event Event.
   */
  public void onMainMenuButtonPressed(Event event) {
    switchToMainMenuPane(event);
  }
}
