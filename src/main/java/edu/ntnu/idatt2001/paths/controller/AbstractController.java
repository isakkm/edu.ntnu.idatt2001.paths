package edu.ntnu.idatt2001.paths.controller;

import edu.ntnu.idatt2001.paths.model.Passage;
import edu.ntnu.idatt2001.paths.view.App;
import edu.ntnu.idatt2001.paths.view.panes.*;
import javafx.event.Event;

/**
 * Represents an abstract controller with default switch pane methods.
 * This class is meant to be inherited in order to avoid code duplication.
 *
 * @author Jens Christian Aanestad
 * @version 1.0
 */
public abstract class AbstractController {
  
  /**
   * Switches pane to the main menu pane.
   *
   * @param event Event.
   */
  public void switchToMainMenuPane(Event event) {
    App.changeRootPane(new MainMenuPane());
  }
  
  /**
   * Switches pane to the player menu pane.
   *
   * @param event Event.
   */
  public void switchToPlayerMenuPane(Event event) {
    App.changeRootPane(new PlayerMenuPane());
  }
  
  /**
   * Switches pane to the play-main-story-menu pane.
   *
   * @param event Event.
   */
  public void switchToPlayMainStoryMenuPane(Event event) {
    App.changeRootPane(new PlayMainStoryMenuPane());
  }
  
  /**
   * Switches pane to the game pane.
   *
   * @param event Event.
   */
  public void switchToGamePane(Event event) {
    App.changeRootPane(new GamePane(App.getGame().begin()));
  }
  
  /**
   * Switches pane to the game pane on a specific passage.
   *
   * @param event Event.
   */
  public void switchToGamePane(Event event, Passage passage) {
    App.changeRootPane(new GamePane(passage));
  }
  
  /**
   * Switches pane to the goals menu pane.
   *
   * @param event Event.
   */
  public void switchToGoalsMenuPane(Event event) {
    App.changeRootPane(new GoalsMenuPane());
  }
  
  /**
   * Switches pane to the import game pane.
   *
   * @param event Event.
   */
  public void switchToImportGamePane(Event event) {
    App.changeRootPane(new ImportStoryPane());
  }
}
