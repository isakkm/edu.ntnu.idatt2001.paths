package edu.ntnu.idatt2001.paths.controller;

import edu.ntnu.idatt2001.paths.validation.FormatValidation;
import edu.ntnu.idatt2001.paths.model.goals.*;
import edu.ntnu.idatt2001.paths.view.AlertDialog;
import edu.ntnu.idatt2001.paths.view.App;
import edu.ntnu.idatt2001.paths.view.Tools;
import edu.ntnu.idatt2001.paths.view.panes.GoalsMenuPane;
import javafx.event.Event;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the goals menu controller
 * This controller is responsible for creating goals.
 * Inherits properties from the AbstractController.
 * Corresponds to the GoalsMenuPane class.
 *
 * @author Isak Kallestad Mandal
 * @author Jens Christian Aanestad
 * @version 1.0
 * @see GoalsMenuPane
 * @see AbstractController
 */
public class GoalsMenuController extends AbstractController {
  // List of game goals.
  public static final List<String> GAME_GOALS = List.of("Gold goal", "Health goal", "Inventory goal", "Score goal");
  private int rowIndex = 0;
  
  /**
   * Represents the action to be executed when the delete goal button has been pressed.
   * Deletes the row of a grid pane and adjust grid pane row indexes.
   *
   * @param gridPane GridPane - the grid pane to remove row from.
   * @param deleteButton - the delete button which was pressed.
   */
  public void onDeleteGoalButtonPressed(GridPane gridPane, Button deleteButton) {
    removeRow(gridPane, getRowIndexAsInteger(deleteButton));
  }
  
  /**
   * Adds a new and empty goal in the grid pane.
   *
   * @param gridPane GridPane - the grid pane add new goal in.
   */
  public void onNewGoalButtonPressed(GridPane gridPane) {
    newGoal(gridPane, "", "");
  }
  
  /**
   * Adds a ComboBox and TextField and Button representing a new goal in the grid pane.
   *
   * @param gridPane GridPane - the grid pane add new goal in.
   * @param goalType String - the goal type.
   * @param goalValue String - the goal value.
   */
  public void newGoal(GridPane gridPane, String goalType, String goalValue) {
    gridPane.getRowConstraints().add(new RowConstraints(30));
  
    // Goal type - ComboText.
    ComboBox<String> comboBox = new ComboBox<>();
    comboBox.setValue(goalType);
    comboBox.getItems().addAll(GAME_GOALS);
    comboBox.setMinHeight(30);
    comboBox.setMinWidth(170);
    comboBox.setFocusTraversable(false);
    comboBox.setCursor(Cursor.HAND);
  
    // Goal value - TextField.
    TextField textField = new TextField();
    textField.setText(goalValue);
    textField.setPromptText("value");
    textField.setMinHeight(30);
    textField.setMinWidth(170);
    textField.setFocusTraversable(false);
  
    // Delete button - Button.
    Button button = new Button("X");
    button.setStyle("-fx-font-weight: bold;");
    button.setFocusTraversable(false);
    button.setCursor(Cursor.HAND);
    button.setOnAction(action -> onDeleteGoalButtonPressed(gridPane, button));
  
    // Add nodes to grid pane.
    gridPane.add(comboBox, 0, rowIndex);
    gridPane.add(textField, 1, rowIndex);
    gridPane.add(button, 2, rowIndex);
    rowIndex++;
  }
  
  /**
   * Represents the action to be executed when the choose-player-button has been pressed.
   * Checks if player is initialized and asks if you still would like to edit player. If
   * yes then switches scene.
   *
   * @param event Event
   */
  public void onChoosePlayerButtonPressed(Event event) {
    if (App.getPlayer() != null) {
      if (AlertDialog.showYesNoConfirmationDialog("Player already exist"
              , "There already exists a player. Do you still want to edit player?")) {
        switchToPlayerMenuPane(event);
      }
    } else {
      switchToPlayerMenuPane(event);
    }
  }
  
  /**
   * Represents the action to be executed when the confirm-goals-button has been pressed.
   * Marks all invalid input, and if there are any then an alert message will be displayed.
   * Sets the goals if all inputs are valid.
   *
   * @param gridPane GridPane.
   */
  public void onConfirmGoalsButtonPressed(GridPane gridPane) {
    markInvalidGoalRed(gridPane);
    if (gridPane.getChildren().stream().anyMatch(child -> child.getStyle().equals("-fx-border-color: red"))) {
      AlertDialog.showWarningDialog("Invalid goals",
              "It appears to be one or more invalid goal input.");
    } else if (gridPane.getChildren().size() == 0) {
      AlertDialog.showWarningDialog("No goals created",
              "You must at least create one goal in order to confirm.");
    } else {
      try {
        App.setGoals(getGoalsFromGridPane(gridPane));
        AlertDialog.showInformationDialog("Goals confirmed",
                "The goals were successfully confirmed.");
        markAllGoalsGreen(gridPane);
      } catch (IllegalArgumentException e) {
        AlertDialog.showWarningDialog("Invalid goals", e.getMessage());
      }
    }
  }
  
  /**
   * Iterates through the grid pane and returns a list of goals.
   * Note that this method does not check for invalid values.
   *
   * @param gridPane GridPane - the grid pane to iterate through.
   * @return List {@literal <}Goal> - list of goals.
   */
  public List<Goal> getGoalsFromGridPane(GridPane gridPane) {
    List<Goal> listOfGoals = new ArrayList<>();
    for (int i = 0; i < gridPane.getRowCount(); i++) {
      ComboBox<String> comboBox = (ComboBox<String>) Tools.getNodeByRowColumnIndex(i, 0, gridPane);
      TextField textField = (TextField) Tools.getNodeByRowColumnIndex(i, 1, gridPane);
      switch (comboBox.getValue()) {
        case "Gold goal" -> listOfGoals.add(new GoldGoal(Integer.parseInt(textField.getText())));
        case "Health goal" -> listOfGoals.add(new HealthGoal(Integer.parseInt(textField.getText())));
        case "Score goal" -> listOfGoals.add(new ScoreGoal(Integer.parseInt(textField.getText())));
        case "Inventory goal" -> listOfGoals.add(new InventoryGoal(List.of(textField.getText().split("(\\s+)?,(\\s+)?"))));
      }
    }
    return listOfGoals;
  }
  
  /**
   * Iterates through all nodes in a grid pane, and marks border color red
   * if it contains invalid input.
   *
   * @param gridPane GridPane - the grid pane to iterate through.
   */
  private void markInvalidGoalRed(GridPane gridPane) {
    gridPane.getChildren().forEach(child -> {
      child.setStyle("");
      if (child.getClass() == TextField.class) {
        TextField textField = (TextField) child;
        ComboBox comboBox = (ComboBox) Tools.
                getNodeByRowColumnIndex(getRowIndexAsInteger(child), 0, gridPane);
        ComboBox<String> comboBoxConverted = (ComboBox<String>) comboBox;
        if (comboBox.getValue() == null || comboBoxConverted.getValue().isBlank()) {
          comboBox.setStyle("-fx-border-color: red");
          child.setStyle("-fx-border-color: red");
        } else if (comboBoxConverted.getValue().equals("Inventory goal")
                && !FormatValidation.isValidInventoryText(textField.getText())) {
          child.setStyle("-fx-border-color: red");
        } else if (GAME_GOALS.stream().anyMatch(goal -> comboBoxConverted.getValue().equals(goal)
                && !comboBoxConverted.getValue().equals("Inventory goal"))
                && !FormatValidation.isValidPositiveNumber(textField.getText())) {
          child.setStyle("-fx-border-color: red");
        }
      }
    });
  }
  
  /**
   * Iterates through all nodes in a grid pane and marks their border color green.
   *
   * @param gridPane GridPane - the grid pane to iterate through.
   */
  private void markAllGoalsGreen(GridPane gridPane) {
    for (int i = 0; i < gridPane.getRowCount(); i++) {
      Tools.getNodeByRowColumnIndex(i, 0, gridPane).setStyle("-fx-border-color: green");
      Tools.getNodeByRowColumnIndex(i, 1, gridPane).setStyle("-fx-border-color: green");
    }
  }
  
  /**
   * Initializes goals to be displayed on the grid pane if there already exists goals.
   *
   * @param gridPane GridPane - the grid pane to initialize goals on.
   */
  public void initializeGoals(GridPane gridPane) {
    if (App.getGoals() != null) {
      App.getGoals().forEach(goal -> {
        String goalType = "";
        if (GoldGoal.class.equals(goal.getClass())) {
          goalType = "Gold goal";
        } else if (HealthGoal.class.equals(goal.getClass())) {
          goalType = "Health goal";
        } else if (ScoreGoal.class.equals(goal.getClass())) {
          goalType = "Score goal";
        } else if (InventoryGoal.class.equals(goal.getClass())) {
          goalType = "Inventory goal";
        }
        newGoal(gridPane, goalType, String.valueOf(goal.getGoalValue()));
      });
      markAllGoalsGreen(gridPane);
    }
  }

  /**
   * Returns the row index to a node in a grid pane.
   *
   * @param node Node - the node to retrieve the row index from.
   * @return row int - the row index of the node
   */
  public int getRowIndexAsInteger(Node node) {
    final var row = GridPane.getRowIndex(node);
    // By some reason when trying to retrieve rowIndex from node on row 0
    // the method getRowIndex returns null. Must therefore check for null.
    if (row == null) {
      return 0;
    }
    return row;
  }
  
  /**
   * Removes chosen row on chosen grid pane.
   *
   * @param grid GridPane - the grid pane where row is to be removed
   * @param targetRowIndexIntegerObject Integer - the index for the row to be removed
   */
  public void removeRow(GridPane grid, Integer targetRowIndexIntegerObject) {
    final int targetRowIndex =
            targetRowIndexIntegerObject == null ? 0 : targetRowIndexIntegerObject;
    
    // Remove children from row
    grid.getChildren().removeIf(node -> getRowIndexAsInteger(node) == targetRowIndex);
    // Update indexes for elements in further rows
    grid.getChildren().forEach(node -> {
      final int rowIndex = getRowIndexAsInteger(node);
      if (targetRowIndex < rowIndex) {
        GridPane.setRowIndex(node, rowIndex - 1);
      }
    });
    rowIndex -= 1;
    // Remove row constraints
    grid.getRowConstraints().remove(targetRowIndex);
  }
}