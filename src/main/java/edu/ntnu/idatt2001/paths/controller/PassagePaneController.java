package edu.ntnu.idatt2001.paths.controller;


import edu.ntnu.idatt2001.paths.model.Link;
import edu.ntnu.idatt2001.paths.model.Passage;
import edu.ntnu.idatt2001.paths.view.AlertDialog;
import edu.ntnu.idatt2001.paths.view.App;
import edu.ntnu.idatt2001.paths.view.Tools;
import edu.ntnu.idatt2001.paths.view.element.ButtonElement;
import edu.ntnu.idatt2001.paths.view.panes.CharacterPane;
import edu.ntnu.idatt2001.paths.view.panes.ContentPane;
import javafx.event.Event;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * Represents the passage pane controller.
 * This controller is responsible for navigating the passages.
 * Inherits properties from the AbstractController class.
 * Corresponds to the PassagePane class.
 *
 * @author Jens Chrsitian Aanestad
 * @author Isak Kallestad Mandal
 * @version 1.0
 * @see edu.ntnu.idatt2001.paths.view.panes.PassagePane
 * @see AbstractController
 */
public class PassagePaneController extends AbstractController {
  private int linkNumber = 0;
  
  /**
   * Generates link buttons from a passage. Each button will be
   * placed on either the left side VBox or right side VBox depending
   * on even or odd linkNumber.
   *
   * @param leftVBox VBox - the vbox to add odd buttons.
   * @param rightVBox VBox - the vbox to add even buttons.
   * @param passage Passage - the passage with links.
   */
  public void generateLinkButtons(VBox leftVBox, VBox rightVBox, Passage passage, CharacterPane characterPane) {
    passage.getLinks().forEach(link -> {
      Button linkButton = ButtonElement.createButton(link.getText(), 200, 50);
      linkButton.setMaxWidth(50);
      linkButton.setOnAction(event -> onLinkButtonPressed(event, link, characterPane));
      if (linkNumber % 2 == 0) {
        leftVBox.getChildren().add(linkButton);
      } else {
        rightVBox.getChildren().add(linkButton);
      }
      linkNumber++;
    });
  }
  
  /**
   * Represents the action to be executed when a link button is pressed.
   * Switches pane to corresponding passage pane in the game pane. If link
   * do not match/is broken, then an error dialog message will be displayed.
   *
   * @param event Event.
   * @param link Link - the link to the passage to change pane to.
   */
  public void onLinkButtonPressed(Event event, Link link, CharacterPane characterPane) {
    if (App.getGame().go(link) == null) {
      AlertDialog.showErrorDialog("Broken Link", "This is a broken link");
    } else {
      App.getGameLog().addLog(App.getGame().go(link), App.getGame().getPlayer());
      App.getGameLog().addLink(link);
      link.executeActions(App.getGame().getPlayer());
      Tools.playRunningSound();
      Tools.transitionAnimation(characterPane, 0,1000).setOnFinished(event1 -> {
        Tools.transitionAnimation(characterPane, -1000, 0).setOnFinished(event2 -> {
          switchToGamePane(event, App.getGame().go(link));
        });
      });
    }
  }
  
  /**
   * Controller method for updating passage pane and checks if all goals
   * is fulfilled and player is alive. If all goals is fulfilled or player
   * is not alive, the title and content on the passage pane will be set, and
   * link buttons will disappear.
   *
   * @param passageTitleLabel Label - the passage pane's title label.
   * @param contentPane ContentPane - the passage pane's content pane.
   * @param leftButtonHolder VBox - the passage pane's left button holder.
   * @param rightButtonHolder VBox - the passage pane's right button holder.
   * @return Content pane - the new content pane.
   */
  public ContentPane updatePassagePane(Label passageTitleLabel, ContentPane contentPane,
                                VBox leftButtonHolder, VBox rightButtonHolder) {
    if (App.getGame().getGoals().stream().allMatch(goal -> goal.isFulfilled(App.getGame().getPlayer()))) {
      if (App.getPlayer().isAlive()) {
        passageTitleLabel.setText("You Won");
        contentPane = new ContentPane("Congratulations you have won with pride!");
      }
      else {
        Tools.playDeathSound();
        passageTitleLabel.setText("You Won, but unfortunately are dead");
        contentPane = new ContentPane("Congratulations you have won, but you paid the ultimate price.");
      }
      leftButtonHolder.getChildren().clear();
      rightButtonHolder.getChildren().clear();
    }
    else {
      if (!App.getGame().getPlayer().isAlive()) {
        Tools.playDeathSound();
        passageTitleLabel.setText("You Died");
        contentPane = new ContentPane("You have lost. Now your soul is forfeit.");
        leftButtonHolder.getChildren().clear();
        rightButtonHolder.getChildren().clear();
      }
    }
    return contentPane;
  }
  
  /**
   * Checks if player is alive, and if not sets the passage pane's character pane
   * to a "dead" character.
   *
   * @param characterPane CharacterPane - the passage pane's character pane.
   * @return CharacterPane - the updated character pane.
   */
  public CharacterPane updateCharacter(CharacterPane characterPane) {
    if (!App.getGame().getPlayer().isAlive()) {
      characterPane = new CharacterPane(Tools.getImage("deadIndianaJonesPixel"), 200, 150);
    }
    return characterPane;
  }
}
