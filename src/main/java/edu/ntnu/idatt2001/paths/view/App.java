package edu.ntnu.idatt2001.paths.view;

import edu.ntnu.idatt2001.paths.model.game.Game;
import edu.ntnu.idatt2001.paths.model.game.GameLog;
import edu.ntnu.idatt2001.paths.model.*;
import edu.ntnu.idatt2001.paths.model.goals.Goal;
import edu.ntnu.idatt2001.paths.model.player.Player;
import edu.ntnu.idatt2001.paths.view.panes.MainMenuPane;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Represents the class that starts the application.
 * @author jensc & isak
 * @version 1.0
 */
public class App extends Application {
  private static File mainGameProgress;
  private static File importedGameProgress;
  private static Stage stage;
  private static Game game;
  private static GameLog gameLog = new GameLog();
  private static Player player;
  private static List<Goal> goals;
  
  /**
   * The entrypoint of the application.
   *
   * @param args arguments.
   */
  public static void main(String[] args) {
    launch(args);
  }
  
  /**
   * Initializes the application.
   *
   * @param primaryStage Stage - the stage to set scene.
   */
  @Override
  public void start(Stage primaryStage) {
    
    // Creates external files in order to save progress.
    try {
      // The path to user's profile directory.
      String userDir = System.getProperty("user.home");
      // Adds another folder in the user's profile directory named TheForrestOfDoom.
      Path pathsFolder = Paths.get(userDir, "TheForrestOfDoom");
  
      // Checks if TheForrestOfDoom folder not exists.
      // If so creates files to this folder.
      if (!Files.exists(pathsFolder)) {
        Files.createDirectories(pathsFolder);
        Path mainStoryProgressPath = pathsFolder.resolve("mainGameProgress.txt");
        Files.createFile(mainStoryProgressPath);
        Path importedStoryProgressPath = pathsFolder.resolve("importedGameProgress.txt");
        Files.createFile(importedStoryProgressPath);
      }
      
      // Setting the files.
      setMainGameProgress(new File(userDir + "/TheForrestOfDoom/mainGameProgress.txt"));
      setImportedGameProgress(new File(userDir + "/TheForrestOfDoom/importedGameProgress.txt"));
      
    } catch (IOException e) {
      AlertDialog.showWarningDialog("Unexpected error",
              "An unexpected error has occurred. Consider to restart the application.");
    }
    
    MainMenuPane mainMenuPane = new MainMenuPane();
    Scene scene = new Scene(mainMenuPane, mainMenuPane.getPrefWidth(), mainMenuPane.getPrefHeight());
    stage = primaryStage;
    stage.setScene(scene);
    stage.setResizable(true);
    stage.setMaximized(true);
    stage.setTitle("The Forrest of Doom");
    stage.getIcons().add(new Image("images/hornsOfDoom.png"));
    stage.show();
  }
  
  /**
   * Returns the game (object) of the application.
   *
   * @return Game - the game of the application.
   */
  public static Game getGame() {
    return game;
  }
  
  /**
   * Returns the player of the game.
   *
   * @return Player - the player of the game.
   */
  public static Player getPlayer() {
    return player;
  }
  
  /**
   * Returns the goals of the game.
   *
   * @return List {@literal <}Goal> - the list of goals in the game.
   */
  public static List<Goal> getGoals() {
    return goals;
  }
  
  /**
   * Returns the game log of the application.
   *
   * @return gameLog - the game log of the application.
   */
  public static GameLog getGameLog() {
    return gameLog;
  }
  
  /**
   * Sets the game of the application to a new game.
   *
   * @param game Game - the new game to be set.
   */
  public static void setGame(Game game) {
    App.game = game;
  }
  
  /**
   * Sets the player of the game to a new player.
   *
   * @param player Player - the player to be set.
   */
  public static void setPlayer(Player player) {
    App.player = player;
  }
  
  /**
   * Sets the list of goals of the game to a new list of goals.
   *
   * @param goals List {@literal <}Goal> - the list of goals to be set.
   */
  public static void setGoals(List<Goal> goals) {
    App.goals = goals;
  }
  
  /**
   * Sets the game log to a new game log.
   *
   * @param gameLog GameLog - the new game log to be set.
   */
  public static void setGameLog(GameLog gameLog) {
    App.gameLog = gameLog;
  }
  
  /**
   * Sets the main game progress file to a new file.
   *
   * @param mainGameProgress File - the new file.
   */
  public void setMainGameProgress(File mainGameProgress) {
    App.mainGameProgress = mainGameProgress;
  }
  
  /**
   * Sets the imported game progress file to a new file.
   *
   * @param importedGameProgress File - the new file.
   */
  public void setImportedGameProgress(File importedGameProgress) {
    App.importedGameProgress = importedGameProgress;
  }
  
  /**
   * Returns the main game progress file.
   *
   * @return File - the main game progress file.
   */
  public static File getMainGameProgress() {
    return mainGameProgress;
  }
  
  /**
   * Returns the imported game progress file.
   *
   * @return File - the imported game progress file.
   */
  public static File getImportedGameProgress() {
    return importedGameProgress;
  }
  
  /**
   * Get the stage of the application.
   *
   * @return Stage - the application stage.
   */
  public static Stage getStage() {
    return stage;
  }
  
  /**
   * Set the scene of the application.
   *
   * @param pane Pane - the pane to set as the scene.
   */
  public static void changeRootPane(Pane pane) {
    stage.getScene().setRoot(pane);
  }
  
  /**
   * Casts the game variable and passage variables from a progressList
   * from the progress file and sets the player. Returns the active passage
   * from the progress file.
   *
   * @param progressList List{@literal <}Object> progressList - the progressList to cast from.
   * @return Passage - the passage from the progressList file.
   */
  public static Passage loadGame(List<Object> progressList) {
    Game deserializedGame = (Game) progressList.get(0);
    Passage deserializedPassage = (Passage) progressList.get(1);
    GameLog deserializedGameLog = (GameLog) progressList.get(2);
    App.setGame(new Game(deserializedGame.getPlayer(), deserializedGame.getStory(), deserializedGame.getGoals()));
    App.setGameLog(deserializedGameLog);
    
    return deserializedPassage;
  }
}
