package edu.ntnu.idatt2001.paths.view.element;

import edu.ntnu.idatt2001.paths.view.Tools;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;

/**
 * Represents the class used for effectively creating backgrounds.
 *
 * @author jensc
 * @version 1.0
 */
public class BackGroundElement {
  
  /**
   * Creates a background with the given background name. If the name is incorrect,
   * then no background will be set.
   *
   * @param backgroundName String - the background name in the resource/images folder.
   * @return BackgroundImage - The created BackgroundImage.
   */
  public static BackgroundImage createBackGround(String backgroundName) {
  
    return new BackgroundImage(
            Tools.getImage(backgroundName),
            BackgroundRepeat.NO_REPEAT,
            BackgroundRepeat.NO_REPEAT,
            BackgroundPosition.CENTER,
            new BackgroundSize(1.0, 1.0, true, true, false, true));
  }
}
