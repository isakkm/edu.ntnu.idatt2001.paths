package edu.ntnu.idatt2001.paths.view.panes;

import edu.ntnu.idatt2001.paths.controller.PlayMainStoryMenuController;
import edu.ntnu.idatt2001.paths.view.element.BackGroundElement;
import edu.ntnu.idatt2001.paths.view.element.ButtonElement;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class PlayMainStoryMenuPane extends StackPane {
  PlayMainStoryMenuController controller = new PlayMainStoryMenuController();
  
  public PlayMainStoryMenuPane() {
    super();
  
    // Create a background image
    BackgroundImage backgroundImage = BackGroundElement.createBackGround("forrestBackGround");
    setBackground(new Background(backgroundImage));
  
    // VBox - button holder
    VBox vBox = new VBox(20);
    vBox.setAlignment(Pos.CENTER);
    vBox.setSpacing(20);
    vBox.setPadding(new javafx.geometry.Insets(50));
    getChildren().add(vBox);
  
    // Title
    Label title = new Label("Play Main Story");
    title.setStyle(""
            + "-fx-font-size: 70;"
            + "-fx-text-fill: white;"
            + "-fx-font-style: bold");
    title.setTranslateY(-100);
    vBox.getChildren().add(title);
  
    // New Game button
    Button newGame = ButtonElement.createButton("New Game", 300, 55);
    newGame.setOnAction(controller::onNewGameButtonPressed);
    vBox.getChildren().add(newGame);
  
    // Load Game button
    Button loadGame = ButtonElement.createButton("Load Game", 300, 55);
    loadGame.setOnAction(controller::onLoadGameButtonPressed);
    vBox.getChildren().add(loadGame);
  
    // Main Menu button
    Button mainMenu = ButtonElement.createButton("Main Menu", 300, 55);
    mainMenu.setOnAction(controller::onMainMenuButtonPressed);
    vBox.getChildren().add(mainMenu);
  }
}
