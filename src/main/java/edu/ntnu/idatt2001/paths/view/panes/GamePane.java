package edu.ntnu.idatt2001.paths.view.panes;

import edu.ntnu.idatt2001.paths.controller.GameController;
import edu.ntnu.idatt2001.paths.model.Passage;

import edu.ntnu.idatt2001.paths.model.player.PlayerChangeListener;
import edu.ntnu.idatt2001.paths.view.App;
import edu.ntnu.idatt2001.paths.view.Tools;
import edu.ntnu.idatt2001.paths.view.element.BackGroundElement;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

import java.util.List;

/**
 * Represents the game pane.
 * This class is responsible for viewing the game pane from imported story.
 * Inherits properties from BorderPane.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 0.1
 * @see BorderPane
 * @see GameController
 */
public class GamePane extends BorderPane {
  private final GameController controller = new GameController();
  private static PassagePane activePassagePane;
  private static Passage activePassage;
  
  /**
   * Constructor to initialize the game pane.
   */
  public GamePane(Passage passage) {
    super();
    
    // Create a background image - default background.
    BackgroundImage backgroundImage = BackGroundElement
            .createBackGround("forrestBackGround");
    setBackground(new Background(backgroundImage));
  
    // Passage pane.
    setCenter(new PassagePane(passage));
    activePassage = passage;
    activePassagePane = (PassagePane) getCenter();
    
    // VBox - holds the top tier StackPanes.
    VBox tierHolder = new VBox();
    tierHolder.setSpacing(15);
    setTop(tierHolder);
    
    // StackPane - first tier.
    StackPane firstTierStackPane = new StackPane();
    tierHolder.getChildren().add(firstTierStackPane);
    
    // StackPane - second tier.
    StackPane secondTierStackPane = new StackPane();
    tierHolder.getChildren().add(secondTierStackPane);
  
    // Title label.
    Label title = new Label(App.getGame().getStory().getTitle());
    title.setStyle(""
            + "-fx-font-size: 50;"
            + "-fx-text-fill: white;"
            + "-fx-font-weight: bold");
    
    // Player name label
    Label playerName = new Label(App.getGame().getPlayer().getName());
    playerName.setStyle(""
            + "-fx-font-size: 30;"
            + "-fx-text-fill: white;"
            + "-fx-font-weight: bold");
    playerName.setAlignment(Pos.CENTER_RIGHT);
    
    // Player name holder - in order to align label.
    VBox playerNameHolder = new VBox();
    playerNameHolder.getChildren().add(playerName);
    playerNameHolder.setAlignment(Pos.CENTER_RIGHT);
    playerNameHolder.setPadding(new Insets(0, 20, 0, 0));
    
    // HBox - Player stats.
    HBox statsHBox = new HBox();
    statsHBox.setSpacing(30);
    
    // Player stats values.
    IntegerProperty playerGold = new SimpleIntegerProperty(App.getGame().getPlayer().getGold());
    IntegerProperty playerHealth = new SimpleIntegerProperty(App.getGame().getPlayer().getHealth());
    IntegerProperty playerScore = new SimpleIntegerProperty(App.getGame().getPlayer().getScore());
    
    // Player stats labels.
    Label goldLabel = new Label(String.valueOf(playerGold.getValue()));
    goldLabel.setStyle("-fx-text-fill: white; -fx-font-size: 25; -fx-font-weight: bold");
    Label healthLabel = new Label(String.valueOf(playerHealth.getValue()));
    healthLabel.setStyle("-fx-text-fill: white; -fx-font-size: 25; -fx-font-weight: bold");
    Label scoreLabel = new Label(String.valueOf(playerScore.getValue()));
    scoreLabel.setStyle("-fx-text-fill: white; -fx-font-size: 25; -fx-font-weight: bold");
    
    // Inventory label.
    Label inventoryLabel = new Label("Inventory: ");
    inventoryLabel.setStyle("-fx-text-fill: white; -fx-font-size: 25; -fx-font-weight: bold");
    inventoryLabel.setAlignment(Pos.CENTER);
    
    // Inventory HBox - in order to set alignment.
    HBox inventoryHBox = new HBox();
    inventoryHBox.getChildren().add(inventoryLabel);
    inventoryHBox.setPadding(new Insets(0, 0, 0, 15));
    inventoryHBox.setAlignment(Pos.CENTER_LEFT);
    
    // Goals label.
    Label goalsLabel = new Label("Goals: ");
    goalsLabel.setStyle("-fx-text-fill: white; -fx-font-size: 25; -fx-font-weight: bold");
    goalsLabel.setAlignment(Pos.CENTER);
    
    // Goals HBox - in order to set alignment.
    HBox goalsHBox = new HBox();
    goalsHBox.getChildren().add(goalsLabel);
    goalsHBox.setPadding(new Insets(0, 120, 0, 0));
    goalsHBox.setAlignment(Pos.CENTER_RIGHT);
    
    // Hamburger menu image
    ImageView hamburgerMenu = new ImageView(Tools.getImage("hamburgerMenuIcon"));
    hamburgerMenu.setCursor(Cursor.HAND);
    hamburgerMenu.setFitWidth(50);
    hamburgerMenu.setFitHeight(50);
    
    // Player stats images.
    ImageView goldImage = new ImageView(Tools.getImage("goldIngot"));
    ImageView healthImage = new ImageView(Tools.getImage("healthPixel"));
    ImageView scoreImage = new ImageView(Tools.getImage("bitcoin"));
    List.of(goldImage, healthImage, scoreImage).forEach(image -> {
      if (image == goldImage) {
        image.setFitWidth(70);
      } else {
        image.setFitWidth(50);
      }
      image.setFitHeight(50);
    });
    
    // HBox stats pairing in order to map stat label and stat image.
    HBox goldHBox = new HBox();
    goldHBox.getChildren().addAll(goldLabel, goldImage);
    
    HBox healthHBox = new HBox();
    healthHBox.getChildren().addAll(healthLabel, healthImage);
    
    HBox scoreHBox = new HBox();
    scoreHBox.getChildren().addAll(scoreLabel, scoreImage);
    
    List.of(goldHBox, healthHBox, scoreHBox).forEach(hBox -> {
      hBox.setAlignment(Pos.CENTER);
      hBox.setSpacing(10);
    });
    
    // HBox - Hamburger menu holder (in order to align the imageview)
    HBox hamburgerMenuHBox = new HBox();
    hamburgerMenuHBox.getChildren().add(hamburgerMenu);
    hamburgerMenuHBox.setPadding(new Insets(0, 0, 0, 30));
    hamburgerMenuHBox.setAlignment(Pos.CENTER_LEFT);
    
    // firstTierStackPane - add elements
    firstTierStackPane.getChildren().add(playerNameHolder);
    firstTierStackPane.getChildren().add(hamburgerMenuHBox);
    firstTierStackPane.getChildren().add(title);
    firstTierStackPane.setAlignment(Pos.CENTER);
    
    // secondTierStackPane - add elements
    statsHBox.getChildren().addAll(goldHBox, healthHBox, scoreHBox);
    statsHBox.setAlignment(Pos.CENTER);
    statsHBox.setSpacing(40);
    secondTierStackPane.getChildren().addAll(inventoryHBox, statsHBox, goalsHBox);
    
    // HBox ground
    HBox groundHBox = new HBox();
    // Binds the groundHBox height with 9% of the scene height.
    groundHBox.prefHeightProperty().bind(App.getStage().getScene().heightProperty().multiply(0.09));
    setBottom(groundHBox);
    
    // StackPane inventory
    StackPane inventoryStackPane = new StackPane();
    inventoryStackPane.setMinWidth(200);
    inventoryStackPane.setMaxWidth(200);
    setLeft(inventoryStackPane);
    
    // VBox inventory
    VBox inventoryVBox = new VBox();
    inventoryStackPane.getChildren().add(inventoryVBox);
    
    // StackPane goals
    StackPane goalsStackPane = new StackPane();
    goalsStackPane.setMinWidth(200);
    setRight(goalsStackPane);
    
    // ScrollPane goals
    ScrollPane goalsScrollPane = new ScrollPane();
    goalsScrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
    goalsScrollPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
    goalsScrollPane.setStyle(""
            + "-fx-background-color: transparent;"
            + "-fx-background: transparent");
    goalsStackPane.getChildren().add(goalsScrollPane);
    
    // VBox goals
    VBox goalsVBox = new VBox();
    goalsStackPane.getChildren().add(goalsVBox);
    
    // Actions
    hamburgerMenu.setOnMouseClicked(event -> GameController.onHamburgerMenuIconPressed(event, activePassagePane.getHamburgerMenuPane()));
    controller.initializeGoals(goalsVBox);
    controller.initializeInventory(inventoryVBox);
  }
  
  /**
   * Returns the active passage pane to the game pane.
   *
   * @return activePassagePane PassagePane.
   */
  public static PassagePane getActivePassagePane() {
    return activePassagePane;
  }
  
  /**
   * Returns the active passage to the game.
   *
   * @return activePassage Passage.
   */
  public static Passage getActivePassage() {
    return activePassage;
  }
}
