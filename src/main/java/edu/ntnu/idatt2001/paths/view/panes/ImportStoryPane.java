package edu.ntnu.idatt2001.paths.view.panes;

import edu.ntnu.idatt2001.paths.controller.ImportStoryController;
import edu.ntnu.idatt2001.paths.view.App;
import edu.ntnu.idatt2001.paths.view.element.BackGroundElement;
import edu.ntnu.idatt2001.paths.view.element.ButtonElement;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.*;

/**
 * Represents the import story pane
 * This class is responsible for viewing and importing story.
 * Inherits properties from StackPane.
 *
 * @author Jens Christian Aanestad
 * @version 1.0
 * @see StackPane
 * @see ImportStoryController
 */
public class ImportStoryPane extends StackPane {
  private final ImportStoryController controller = new ImportStoryController();
  
  /**
   * Constructor to initialize the import menu pane.
   */
  public ImportStoryPane() {
    super();
  
    // Sets the background.
    BackgroundImage backgroundImage = BackGroundElement.createBackGround("forrestBackGround");
    setBackground(new Background(backgroundImage));
    
    // Vertical box
    VBox vBox = new VBox(20);
    vBox.setAlignment(Pos.CENTER);
    vBox.setSpacing(22);
    vBox.setPadding(new javafx.geometry.Insets(25));
    getChildren().add(vBox);
    
    // Title
    Label title = new Label("Import story");
    title.setStyle(""
            + "-fx-font-size: 70;"
            + "-fx-text-fill: white;"
            + "-fx-font-style: bold");
    vBox.getChildren().add(title);
  
    // Import story button
    Button importStoryButton = ButtonElement.createButton("Choose File", 250, 45);
    vBox.getChildren().add(importStoryButton);
    
    // File name label
    Label fileNameLabel = new Label("File name: ");
    fileNameLabel.setWrapText(true);
    fileNameLabel.setStyle("-fx-text-fill: white; -fx-font-size: 22");
    vBox.getChildren().add(fileNameLabel);
  
    // File location label
    Label fileLocationLabel = new Label("Location: ");
    fileLocationLabel.setWrapText(true);
    fileLocationLabel.setStyle("-fx-text-fill: white; -fx-font-size: 22");
    vBox.getChildren().add(fileLocationLabel);
    
    // Play game button
    Button playGameButton = ButtonElement.createButton("Play Game", 250, 45);
    playGameButton.setOnAction(controller::switchToGamePane);
    playGameButton.disableProperty().bind(controller.getStoryImportedCorrectly().not());
    vBox.getChildren().add(playGameButton);
    
    // Load game button.
    Button loadProgressButton = ButtonElement.createButton("Load Game", 250, 45);
    loadProgressButton.setOnAction(controller::onLoadProgressButtonPressed);
    loadProgressButton.disableProperty().bind(new SimpleBooleanProperty(App.getImportedGameProgress().length() != 0).not());
    vBox.getChildren().add(loadProgressButton);
  
    // Back to menu button
    Button backToMainMenu = ButtonElement.createButton("Main Menu", 250, 45);
    backToMainMenu.setOnAction(controller::switchToMainMenuPane);
    vBox.getChildren().add(backToMainMenu);
  
    // Scroll pane - grid pane holder
    ScrollPane scrollPane = new ScrollPane();
    scrollPane.setPrefSize(0, 200);
    scrollPane.setMaxWidth(900);
    scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
    scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
    scrollPane.setStyle(""
            + "-fx-background-color: rgba(0, 0, 0, 0.8);"
            + "-fx-background: transparent; "
            + "-fx-border-color: white; "
            + "-fx-background-radius: 5px; "
            + "-fx-border-radius: 5px");
    setFocusTraversable(false);
    scrollPane.setVisible(false);
    vBox.getChildren().add(scrollPane);
  
    // Grid pane - action holder
    GridPane gridPane = new GridPane();
    gridPane.setStyle(""
            + "-fx-background-color: transparent;"
            + "-fx-background: transparent");
    gridPane.setVgap(10);
    gridPane.setHgap(10);
    scrollPane.setContent(gridPane);
    
    // Actions
    importStoryButton.setOnAction(event -> controller.onImportStoryButtonPressed(event, fileNameLabel, fileLocationLabel, gridPane, scrollPane));
  }
}
