package edu.ntnu.idatt2001.paths.view.panes;

import edu.ntnu.idatt2001.paths.controller.GoalsMenuController;
import edu.ntnu.idatt2001.paths.view.element.BackGroundElement;
import edu.ntnu.idatt2001.paths.view.element.ButtonElement;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;

/**
 * Represents the goals menu pane.
 * This class is responsible for viewing the goals-menu.
 * Inherits properties from StackPane.
 *
 * @author Isak Kallestad Mandal
 * @version 0.9
 * @see StackPane
 * @see GoalsMenuController
 */
public class GoalsMenuPane extends StackPane {
  private GoalsMenuController controller = new GoalsMenuController();
  
  /**
   * Constructor to initialize the goals menu pane.
   */
  public GoalsMenuPane() {
    super();
    
    // Sets the background.
    BackgroundImage backgroundImage = BackGroundElement.createBackGround("forrestBackGround");
    setBackground(new Background(backgroundImage));
    
    // VBox - to hold elements
    VBox vBox = new VBox(20);
    vBox.setAlignment(Pos.CENTER);
    vBox.setSpacing(20);
    vBox.setPadding(new javafx.geometry.Insets(10));
    vBox.setMaxWidth(500);
    getChildren().add(vBox);
    
    // Title - the pane's title
    Label title = new Label("Choose Goals");
    title.setStyle(""
            + "-fx-font-size: 70;"
            + "-fx-text-fill: white;"
            + "-fx-font-style: bold");
    vBox.getChildren().add(title);
    
    // Scroll pane
    ScrollPane scrollPane = new ScrollPane();
    scrollPane.setPrefSize(0, 200);
    scrollPane.setMaxWidth(400);
    scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
    scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
    scrollPane.setStyle(""
            + "-fx-background-color: transparent;"
            + "-fx-background: transparent; "
            + "-fx-border-color: white; "
            + "-fx-background-radius: 5px; "
            + "-fx-border-radius: 5px");
    setFocusTraversable(false);
    vBox.getChildren().add(scrollPane);
    
    // Grid pane
    GridPane gridPane = new GridPane();
    gridPane.setPadding(new Insets(5, 0, 0, 5));
    gridPane.setStyle(""
            + "-fx-background-color: transparent;"
            + "-fx-background: transparent");
    gridPane.setVgap(10);
    gridPane.setHgap(10);
    scrollPane.setContent(gridPane);
    
    // Column Constraints
    ColumnConstraints leftColumn = new ColumnConstraints();
    leftColumn.setPercentWidth(45);
    ColumnConstraints midColumn = new ColumnConstraints();
    midColumn.setPercentWidth(45);
    ColumnConstraints rightColumn = new ColumnConstraints();
    rightColumn.setPercentWidth(10);
    gridPane.getColumnConstraints().addAll(leftColumn, midColumn, rightColumn);
    
    // New goal button
    Button newGoalButton = ButtonElement.createButton("New Goal", 200, 20);
    newGoalButton.setOnAction(event -> controller.onNewGoalButtonPressed(gridPane));
    vBox.getChildren().add(newGoalButton);
    
    // HBox - button holder
    HBox hBox = new HBox(20);
    hBox.setAlignment(Pos.CENTER);
    hBox.setSpacing(30);
    hBox.setPadding(new javafx.geometry.Insets(20));
    getChildren().add(hBox);
    vBox.getChildren().add(hBox);
    
    // Main menu button
    Button mainMenuButton = ButtonElement.createButton("Main Menu", 200, 20);
    mainMenuButton.setOnAction(controller::switchToMainMenuPane);
    hBox.getChildren().add(mainMenuButton);
    
    // Confirm goals button
    Button confirmGoalsButton = ButtonElement.createButton("Confirm Goals", 200, 20);
    confirmGoalsButton.setOnAction(action -> controller.onConfirmGoalsButtonPressed(gridPane));
    hBox.getChildren().add(confirmGoalsButton);
    
    // Choose player button
    Button choosePlayerButton = ButtonElement.createButton("Choose Player", 200, 20);
    choosePlayerButton.setOnAction(controller::switchToPlayerMenuPane);
    hBox.getChildren().add(choosePlayerButton);
    
    // Initialize goals on grid pane if goals already exists
    controller.initializeGoals(gridPane);
  }
}
