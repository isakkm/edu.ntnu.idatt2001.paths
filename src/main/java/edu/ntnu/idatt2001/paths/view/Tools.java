package edu.ntnu.idatt2001.paths.view;

import edu.ntnu.idatt2001.paths.view.panes.MainMenuPane;
import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Represents a class with diverse static methods for the application.
 * @author jensc
 * @version 1.0
 */
public class Tools {
  
  /**
   * Plays a button sound when called.
   */
  public static void playButtonSound() {
    try {
      String fileName = Objects.requireNonNull(MainMenuPane.class.getClassLoader()
              .getResource("sounds/ButtonHoverSoundEffect.mp3")).toURI().toString();
      Media media = new Media(fileName);
      MediaPlayer player = new MediaPlayer(media);
      player.play();
    } catch (URISyntaxException ignored) {}
  }
  
  /**
   * Plays a running sound when called.
   */
  public static void playRunningSound() {
    try {
      String fileName = Objects.requireNonNull(MainMenuPane.class.getClassLoader()
              .getResource("sounds/RunningSoundEffect2.mp3")).toURI().toString();
      Media media = new Media(fileName);
      MediaPlayer player = new MediaPlayer(media);
      player.play();
    } catch (URISyntaxException ignored) {}
  }
  
  /**
   * Plays a death sound when called.
   */
  public static void playDeathSound() {
    try {
      String fileName = Objects.requireNonNull(MainMenuPane.class.getClassLoader()
              .getResource("sounds/TheRockEyeBrowRaiseSoundEffect.mp3")).toURI().toString();
      Media media = new Media(fileName);
      MediaPlayer player = new MediaPlayer(media);
      player.play();
    } catch (URISyntaxException ignored) {}
  }
  
  /**
   * Returns an image from the resource images folder. If the image name finds no match then
   * an alert dialog will be displayed.
   *
   * @param imageName String - the name of the png file. (Without .png!)
   * @return image Image
   */
  public static Image getImage(String imageName) {
    Image image = null;
    try {
      image = new Image(new FileInputStream("src/main/resources/images/" + imageName + ".png"));
    } catch (FileNotFoundException ex) {
      AlertDialog.showErrorDialog("Unexpected error", "Could not load images.\n"
              + "Consider to shut down the application");
    }
    return image;
  }
  
  /**
   * Adds an animation and sound effect when hovering mouse over a node.
   *
   * @param node Node - the node to add the mouse animation.
   */
  public static void hoverMouseAnimationWithSound(Node node) {
    node.addEventHandler(MouseEvent.MOUSE_ENTERED_TARGET, event -> {
      playButtonSound();
      node.setOpacity(0.6);
    });
    node.addEventHandler(MouseEvent.MOUSE_EXITED_TARGET, event -> {
      node.setOpacity(1);
    });
  }
  
  /**
   * Adds an animation when hovering mouse over a node.
   *
   * @param node Node - the node to add the mouse animation.
   */
  public static void hoverMouseAnimation(Node node) {
    node.addEventHandler(MouseEvent.MOUSE_ENTERED_TARGET, event -> {
      node.setOpacity(0.6);
    });
    node.addEventHandler(MouseEvent.MOUSE_EXITED_TARGET, event -> {
      node.setOpacity(1);
    });
  }
  
  /**
   * Executes a transition animation on a node from a start x value to an end x value.
   *
   * @param node Node - the node to add animation to.
   * @param startX int - the start x value.
   * @param endX int - the ending x value.
   * @return TranslateTransition - the translate-transition.
   */
  public static TranslateTransition transitionAnimation(Node node, int startX, int endX) {
    TranslateTransition translateTransition = new TranslateTransition(Duration.seconds(0.5), node);
    node.setTranslateX(startX);
    translateTransition.setToX(endX);
    translateTransition.play();
    return translateTransition;
  }
  
  /**
   * Returns the node with coordinates in a grid pane.
   * If the coordinate is empty or does not exist, then
   * null will be returned. Since the GridPane class do not
   * have any intuitive way for returning element/node by coordinates,
   * this class is therefore implemented.
   *
   * @param row Integer - number representing the row
   * @param column Integer - number representing the column
   * @param gridPane GridPane - the gridPane to loop through
   * @return Node - the node at given coordinate
   */
  public static Node getNodeByRowColumnIndex(Integer row, Integer column, GridPane gridPane) {
    Node result = null;
    for (Node node : gridPane.getChildren()) {
      Integer gridRow = gridPane.getRowIndex(node);
      Integer gridColumn = gridPane.getColumnIndex(node);
      /*
       By some wierd reason if the gridRowIndex or gridColumnIndex to a node is 0, then it returns
       null. Do not know why, but it has been issued by several on stackoverflow.
       */
      if (gridRow == null) {
        gridRow = 0;
      }
      if (gridColumn == null) {
        gridColumn = 0;
      }
      if (gridRow == row && gridColumn == column) {
        result = node;
        break;
      }
    }
    return result;
  }
}
