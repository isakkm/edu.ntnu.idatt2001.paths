package edu.ntnu.idatt2001.paths.view.element;


import edu.ntnu.idatt2001.paths.view.Tools;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;

/**
 * Represents the class used for effectively creating buttons with same style.
 *
 * @author jensc
 * @version 1.0
 */
public class ButtonElement {
  
  /**
   * Creates a button with the given text, width and height.
   *
   * @param text String - The text to be displayed on the button.
   * @param width int - the button's width.
   * @param height int - the button's height.
   * @return Button - The created button.
   */
  public static Button createButton(String text, int width, int height) {
    Button button = new Button(text);
    button.setWrapText(true);
    button.setFocusTraversable(false);
    button.setCursor(Cursor.HAND);
    button.setAlignment(Pos.CENTER);
    button.setMinHeight(height);
    button.setMinWidth(width);
    button.addEventHandler(ActionEvent.ACTION ,event -> Tools.playButtonSound());
    Tools.hoverMouseAnimation(button);
    button.setStyle("-fx-text-fill: black;"
            + "-fx-font-size: 20;"
            + "-fx-background-radius: 5px;"
            + "-fx-border-radius: 5px;");
    return button;
  }
  
  /**
   * Marks a button's border red.
   *
   * @param button Button - the button to mark.
   */
  public static void markButtonRed(Button button) {
    button.setStyle("-fx-text-fill: black;"
            + "-fx-font-size: 20;"
            + "-fx-background-radius: 5px;"
            + "-fx-border-radius: 5px;"
            + "-fx-border-color: red");
  }
  
  /**
   * Marks a button's border green.
   *
   * @param button Button - the button to mark.
   */
  public static void markButtonGreen(Button button) {
    button.setStyle("-fx-text-fill: black;"
            + "-fx-font-size: 20;"
            + "-fx-background-radius: 5px;"
            + "-fx-border-radius: 5px;"
            + "-fx-border-color: Green");
  }
  
  /**
   * Resets a button's border.
   *
   * @param button Button - the button to reset.
   */
  public static void resetButton(Button button) {
    button.setStyle("-fx-text-fill: black;"
            + "-fx-font-size: 20;"
            + "-fx-background-radius: 5px;"
            + "-fx-border-radius: 5px;");
  }
}
