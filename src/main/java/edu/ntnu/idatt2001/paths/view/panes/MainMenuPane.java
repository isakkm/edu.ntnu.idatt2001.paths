package edu.ntnu.idatt2001.paths.view.panes;

import edu.ntnu.idatt2001.paths.controller.MainMenuController;
import edu.ntnu.idatt2001.paths.view.App;
import edu.ntnu.idatt2001.paths.view.element.BackGroundElement;
import edu.ntnu.idatt2001.paths.view.element.ButtonElement;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.stage.Screen;

/**
 * Represents the main menu pane.
 * This pane is the first pane to be initialized after starting the application.
 * Inherits properties from StackPane.
 *
 * @author jensc & isak
 * @version 1.0
 * @see StackPane
 * @see MainMenuController
 */
public class MainMenuPane extends StackPane {
  private final MainMenuController controller = new MainMenuController();
  
  /**
   * Constructor to initialize the main menu pane.
   */
  public MainMenuPane() {
    super();
    
    // Create a background image
    BackgroundImage backgroundImage = BackGroundElement.createBackGround("forrestBackGround");
    setBackground(new Background(backgroundImage));
  
    // Get the screen size and set the size of the pane to the screen size
    Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
    setPrefSize(screenBounds.getWidth(), screenBounds.getHeight());
  
    // VBox - button holder
    VBox vBox = new VBox(20);
    vBox.setAlignment(Pos.CENTER);
    vBox.setSpacing(20);
    vBox.setPadding(new javafx.geometry.Insets(50));
    getChildren().add(vBox);
    
    // Title
    Label title = new Label("The Forrest of Doom");
    title.setStyle(""
        + "-fx-font-size: 70;"
        + "-fx-text-fill: white;"
        + "-fx-font-style: bold");
    vBox.getChildren().add(title);
    
    // Choose player button
    Button choosePlayer = ButtonElement.createButton("Choose Player", 300, 55);
    choosePlayer.setOnAction(controller::switchToPlayerMenuPane);
    vBox.getChildren().add(choosePlayer);
  
    // Choose goals button
    Button chooseGoals = ButtonElement.createButton("Choose Goals", 300, 55);
    chooseGoals.setOnAction(controller::switchToGoalsMenuPane);
    vBox.getChildren().add(chooseGoals);

    // Play main story button
    Button playMainStory = ButtonElement.createButton("Play Main Story", 300, 55);
    playMainStory.setOnAction(event -> controller.onPlayMainStoryButtonPressed(event, choosePlayer, chooseGoals));
    vBox.getChildren().add(playMainStory);

    // Import story button
    Button importGame = ButtonElement.createButton("Import Story", 300, 55);
    importGame.setOnAction(event -> controller.onImportStoryButtonPressed(event, choosePlayer, chooseGoals));
    vBox.getChildren().add(importGame);
    
    // Help button
    Button help = ButtonElement.createButton("Help", 300, 55);
    help.setOnAction(controller::onHelpButtonPressed);
    vBox.getChildren().add(help);
  
    // Quit game button
    Button quitGame = ButtonElement.createButton("Quit Game", 300, 55);
    quitGame.setOnAction(event -> System.exit(0));
    vBox.getChildren().add(quitGame);
  }
}
