package edu.ntnu.idatt2001.paths.view.panes;

import edu.ntnu.idatt2001.paths.controller.PlayerMenuController;
import edu.ntnu.idatt2001.paths.view.App;
import edu.ntnu.idatt2001.paths.view.element.BackGroundElement;
import edu.ntnu.idatt2001.paths.view.element.ButtonElement;
import edu.ntnu.idatt2001.paths.view.element.TextFieldElement;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;

import java.util.List;

/**
 * Represents the player menu pane.
 * This class is responsible for viewing the player menu.
 * Inherits properties from StackPane.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 0.9
 * @see StackPane
 * @see PlayerMenuController
 */
public class PlayerMenuPane extends StackPane {
  private final PlayerMenuController controller = new PlayerMenuController();
  
  /**
   * Constructor to initialize the player menu pane.
   */
  public PlayerMenuPane() {
    super();
    
    // Sets the background.
    BackgroundImage backgroundImage = BackGroundElement.createBackGround("forrestBackGround");
    setBackground(new Background(backgroundImage));
    
    // Vbox - Element holder
    VBox elementVBox = new VBox(20);
    elementVBox.setAlignment(Pos.CENTER);
    elementVBox.setSpacing(10);
    elementVBox.setPadding(new Insets(10));
    getChildren().add(elementVBox);
    
    // HBox - player Vbox holder.
    HBox playerVBoxHolder = new HBox(20);
    playerVBoxHolder.setAlignment(Pos.CENTER);
  
    // VBox - player name holder
    VBox playerLabels = new VBox(20);
  
    // VBox - player health holder
    VBox playerTextFields = new VBox(20);
    
    // HBox - button holder
    HBox buttonHBox = new HBox(20);
    buttonHBox.setAlignment(Pos.CENTER);
    buttonHBox.setSpacing(30);
    buttonHBox.setPadding(new Insets(20));
  
    List.of(playerLabels, playerTextFields).forEach(pane -> {
      pane.setAlignment(Pos.CENTER);
      pane.setSpacing(30);
      pane.setPadding(new Insets(20));
    });
    
    // Player menu title
    Label playerMenuTitle = new Label("Choose Player");
    playerMenuTitle.setStyle("-fx-font-size: 70;"
            + "-fx-text-fill: White;"
            + "-fx-font-style: bold");
    playerMenuTitle.setTranslateY(-20);
    elementVBox.getChildren().add(playerMenuTitle);
    
    // Player name text
    Label playerNameText = new Label("Player Name");
    playerNameText.setStyle("-fx-font-size: 30;"
            + "-fx-text-fill: White;"
            + "-fx-font-style: bold");
    playerLabels.getChildren().add(playerNameText);
    
    // Player name text field
    TextField playerName = TextFieldElement.createTextField("Player name", 200, 40);
    playerName.setEditable(true);
    playerTextFields.getChildren().add(playerName);
    
    // Player health title
    Label playerHealthText = new Label("Player Health");
    playerHealthText.setStyle("-fx-font-size: 30;"
            + "-fx-text-fill: White;"
            + "-fx-font-style: bold");
    playerLabels.getChildren().add(playerHealthText);
    
    // Player health text field
    TextField playerHealth = TextFieldElement.createTextField("Optional (Default is 100)", 200, 40);
    playerHealth.setEditable(true);
    playerTextFields.getChildren().add(playerHealth);
    
    // Player gold title
    Label playerGoldText = new Label("Player Gold  ");
    playerGoldText.setStyle("-fx-font-size: 30;"
            + "-fx-text-fill: White;"
            + "-fx-font-style: bold");
    playerLabels.getChildren().add(playerGoldText);
    
    // Player Gold text field
    TextField playerGold = TextFieldElement.createTextField("Optional (Default is 0)", 200, 40);
    playerGold.setEditable(true);
    playerTextFields.getChildren().add(playerGold);
    
    playerVBoxHolder.getChildren().addAll(playerLabels, playerTextFields);
    elementVBox.getChildren().add(playerVBoxHolder);
    
    // Sets the text fields if player already exists
    if (App.getPlayer() != null) {
      playerName.setText(App.getPlayer().getName());
      playerHealth.setText(String.valueOf(App.getPlayer().getHealth()));
      playerGold.setText(String.valueOf(App.getPlayer().getGold()));
      TextFieldElement.markTextFieldGreen(playerName);
      TextFieldElement.markTextFieldGreen(playerHealth);
      TextFieldElement.markTextFieldGreen(playerGold);
    }
    
    // Main menu button
    Button mainMenuButton = ButtonElement.createButton("Main Menu", 200, 20);
    mainMenuButton.setOnAction(controller::switchToMainMenuPane);
    buttonHBox.getChildren().add(mainMenuButton);
    
    // Confirm player button
    Button confirmPlayerButton = ButtonElement.createButton("Confirm Player", 200, 20);
    confirmPlayerButton.setOnAction(event -> controller.onConfirmPlayerButtonPressed(
            event, playerName, playerHealth, playerGold));
    buttonHBox.getChildren().add(confirmPlayerButton);
    
    // Choose goals button
    Button chooseGoalsButton = ButtonElement.createButton("Choose Goals", 200, 20);
    chooseGoalsButton.setOnAction(controller::switchToGoalsMenuPane);
    buttonHBox.getChildren().add(chooseGoalsButton);
    elementVBox.getChildren().add(buttonHBox);
  }
}
