package edu.ntnu.idatt2001.paths.view.element;

import edu.ntnu.idatt2001.paths.view.Tools;
import javafx.scene.Cursor;
import javafx.scene.control.TextField;

/**
 * Represents the class used for effectively creating text fields with same style.
 *
 * @author Isak Kallestad Mandal
 * @version 0.9
 */
public class TextFieldElement {
    public static String borderColor = "#000000";
    public static String backgroundColor = "#FFFFFF";
    
    /**
     * Creates a text field with the given prompt text, width and height.
     *
     * @param promptText String - the text fields prompt text.
     * @param width int - the text field's width.
     * @param height int - the text field's height.
     * @return TextField - the created text field.
     */
    public static TextField createTextField(String promptText, int width, int height) {
        TextField textField = new TextField();
        textField.setFocusTraversable(false);
        textField.setCursor(Cursor.TEXT);
        textField.setPromptText(promptText);
        textField.setMaxWidth(width);
        textField.setMaxHeight(height);
        textField.setMinWidth(width);
        textField.setMinHeight(height);
        Tools.hoverMouseAnimation(textField);
        textField.setStyle("-fx-background-color: " + backgroundColor + ";"
                + "-fx-border-color: " + borderColor + ";"
                + "-fx-font-size: 16;"
                + "-fx-border-radius: 5px;"
                + "-fx-background-radius: 5px");
        return textField;
    }
    
    /**
     * Marks a text field's border red.
     *
     * @param textField TextField - the text field to mark.
     */
    public static void markTextFieldRed(TextField textField) {
        textField.setStyle("-fx-background-color: " + backgroundColor + ";"
                + "-fx-font-size: 16;"
                + "-fx-border-radius: 5px;"
                + "-fx-background-radius: 5px;"
                + "-fx-border-color: red");
    }
    
    /**
     * Marks a text field's border green.
     *
     * @param textField TextField - the text field to mark.
     */
    public static void markTextFieldGreen(TextField textField) {
        textField.setStyle("-fx-background-color: " + backgroundColor + ";"
                + "-fx-font-size: 16;"
                + "-fx-border-radius: 5px;"
                + "-fx-border-color: Green");
    }
    
    /**
     * Resets a text field's border.
     *
     * @param textField TextField - the text field to reset.
     */
    public static void resetTextField(TextField textField) {
        textField.setStyle("-fx-background-color: " + backgroundColor + ";"
                + "-fx-border-color: " + borderColor + ";"
                + "-fx-font-size: 16;"
                + "-fx-border-radius: 5px;");
    }
}
