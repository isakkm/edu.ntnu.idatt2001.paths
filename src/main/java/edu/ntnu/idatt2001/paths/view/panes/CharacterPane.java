package edu.ntnu.idatt2001.paths.view.panes;

import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

/**
 * Represents a character in order to display the character figure.
 * Inherits properties from HBox.
 *
 * @author Jens Christian Aanestad
 * @version 1.0
 */
public class CharacterPane extends HBox {
  
  /**
   * The constructor ro initialize a character pane.
   *
   * @param image Image - the image of the character.
   * @param width int - the width of the image.
   * @param height int - the height of the image.
   */
  public CharacterPane(Image image, int width, int height) {
    ImageView character = new ImageView(image);
    character.setFitWidth(width);
    character.setFitHeight(height);
    this.setMaxWidth(200);
    setAlignment(Pos.BOTTOM_CENTER);
    this.getChildren().add(character);
  }
}
