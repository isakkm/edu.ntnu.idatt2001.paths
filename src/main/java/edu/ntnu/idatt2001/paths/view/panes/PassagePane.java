package edu.ntnu.idatt2001.paths.view.panes;

import edu.ntnu.idatt2001.paths.controller.PassagePaneController;
import edu.ntnu.idatt2001.paths.model.Passage;
import edu.ntnu.idatt2001.paths.view.App;
import edu.ntnu.idatt2001.paths.view.Tools;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;

/**
 * Represents tha passage pane from chosen passage.
 * This pane is responsible for viewing passages and navigation between them.
 * Inherits properties from StackPane.
 *
 * @author Jens Christian Aanestad
 * @version 0.1
 * @see StackPane
 * @see edu.ntnu.idatt2001.paths.controller.PassagePaneController
 * @see HamburgerMenuPane
 * @see ContentPane
 */
public class PassagePane extends BorderPane {
  private final PassagePaneController controller = new PassagePaneController();
  private final HamburgerMenuPane hamburgerMenuPane = new HamburgerMenuPane();
  private final Label passageTitleLabel;
  private ContentPane contentPane;
  private CharacterPane characterPane;
  
  /**
   * Constructor to initialize the passage pane.
   *
   * @param passage Passage - the passage to generate the pane with.
   */
  public PassagePane(Passage passage) {
    super();
    
    // Passage label title.
    passageTitleLabel = new Label(passage.getTitle());
    passageTitleLabel.setStyle(""
            + "-fx-font-size: 40;"
            + "-fx-text-fill: white;"
            + "-fx-font-style: bold");
    passageTitleLabel.setAlignment(Pos.CENTER);
    passageTitleLabel.setWrapText(true
    );
    
    // content
    contentPane = new ContentPane(passage.getContent());
  
    // Character pane - the game character figure.
    characterPane = new CharacterPane(Tools.getImage("indianaJonesPixel"), 150, 200);
  
    // VBox - left links button holder
    VBox leftButtonHolder = new VBox();
    leftButtonHolder.setMinWidth(250);
    leftButtonHolder.setSpacing(15);
    leftButtonHolder.setPadding(new javafx.geometry.Insets(25));
    leftButtonHolder.setAlignment(Pos.CENTER_LEFT);
    setLeft(leftButtonHolder);
  
    // VBox - right button links
    VBox rightButtonHolder = new VBox();
    rightButtonHolder.setMinWidth(250);
    rightButtonHolder.setSpacing(15);
    rightButtonHolder.setPadding(new javafx.geometry.Insets(25));
    rightButtonHolder.setAlignment(Pos.CENTER_RIGHT);
    setRight(rightButtonHolder);
  
    // Generate buttons.
    controller.generateLinkButtons(leftButtonHolder, rightButtonHolder, passage, characterPane);
    // Update passage pane.
    contentPane = controller.updatePassagePane(passageTitleLabel, contentPane, leftButtonHolder, rightButtonHolder);
    // Update character pane.
    characterPane = controller.updateCharacter(characterPane);
  
    // VBox - title and content holder
    VBox titleContentVBox = new VBox();
    titleContentVBox.setSpacing(15);
    titleContentVBox.setPadding(new javafx.geometry.Insets(25));
    titleContentVBox.setAlignment(Pos.TOP_CENTER);
    titleContentVBox.getChildren().addAll(passageTitleLabel, contentPane);
    
    // Center StackPane
    StackPane centerStackPane = new StackPane();
    centerStackPane.getChildren().addAll(characterPane, titleContentVBox);
    setCenter(centerStackPane);
  
    // Hamburger menu pane.
    centerStackPane.getChildren().add(hamburgerMenuPane);
    hamburgerMenuPane.setVisible(false);
  }
  
  /**
   * Returns the hamburger pane in the passage pane.
   *
   * @return hamburgerMenuPane HamburgerMenuPane
   */
  public HamburgerMenuPane getHamburgerMenuPane() {
    return hamburgerMenuPane;
  }
}
