package edu.ntnu.idatt2001.paths.view.panes;

import edu.ntnu.idatt2001.paths.controller.HamburgerMenuController;
import edu.ntnu.idatt2001.paths.view.App;
import edu.ntnu.idatt2001.paths.view.element.ButtonElement;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.util.List;

/**
 * Represents a hamburger menu pane.
 * This class is responsible for viewing the hamburger pane and its buttons.
 * Inherits properties from StackPane.
 *
 * @author Jens Christian Aanestad
 * @version 1.0
 * @see StackPane
 * @see HamburgerMenuController
 */
public class HamburgerMenuPane extends StackPane {
  private final HamburgerMenuController controller = new HamburgerMenuController();
  
  /**
   * Constructor to initialize a hamburger pane.
   */
  public HamburgerMenuPane() {
    super();
    
    // Styling
    setStyle(""
            + "-fx-background-color: rgba(0, 0, 0, 0.8);"
            + "-fx-border-color: white; "
            + "-fx-background-radius: 5px; "
            + "-fx-border-radius: 5px");
    setMaxSize(400, 500);
    
  
    // VBox Button holder
    VBox buttonHolder = new VBox();
    buttonHolder.setAlignment(Pos.CENTER);
    buttonHolder.setSpacing(20);
    buttonHolder.getChildren().addAll(createButtons());
  
    getChildren().add(buttonHolder);
  }
  
  /**
   * Returns the hamburger pane buttons.
   *
   * @return List {@literal <}Button> - a list of buttons.
   */
  private List<Button> createButtons() {
    
    // Continue Button
    Button continueButton = ButtonElement.createButton("Continue", 200, 50);
    continueButton.setOnAction(controller::onContinueButtonPressed);

    // Restart Button
    Button restartButton = ButtonElement.createButton("Restart", 200, 50);
    restartButton.setOnAction(controller::onRestartButtonPressed);
    
    // Save progress Button
    Button saveProgressButton = ButtonElement.createButton("Save Progress", 200, 50);
    saveProgressButton.disableProperty().bind(new SimpleBooleanProperty(App.getGame().getPlayer().isAlive()).not());
    saveProgressButton.setOnAction(controller::onSaveProgressButtonPressed);
  
    // Undo Button
    Button undoButton = ButtonElement.createButton("Undo", 200, 50);
    undoButton.disableProperty().bind(controller.getValidUndo().not());
    undoButton.setOnAction(controller::onUndoButtonPressed);
  
    // Main Menu Button
    Button mainMenuButton = ButtonElement.createButton("Main Menu", 200, 50);
    mainMenuButton.setOnAction(controller::onMainMenuButtonPressed);
    
    return List.of(continueButton, restartButton, saveProgressButton, undoButton, mainMenuButton);
  }
}
