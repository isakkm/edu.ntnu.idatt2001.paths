package edu.ntnu.idatt2001.paths.view;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * Represents the alert dialogs the application can give.
 * @author jensc
 * @version 1.0
 */
public class AlertDialog {
  
  /**
   * Creates a new alert dialog with an alert type, title and content.
   * @param alertType - The type of alert.
   * @param title - The title of the alert box.
   * @param content - The content of the dialog.
   * @return Alert - The alert dialog.
   */
  private static Alert initializeAlertDialog(Alert.AlertType alertType, String title, String content) {
    Alert alert = new Alert(alertType);
    alert.setTitle(title);
    alert.setHeaderText(null);
    alert.setContentText(content);
    return alert;
  }
  
  /**
   * Shows an information dialog with a title and content.
   * @param title - The title of the dialog.
   * @param content - The content of the dialog.
   */
  public static void showInformationDialog(String title, String content) {
    initializeAlertDialog(Alert.AlertType.INFORMATION, title, content).showAndWait();
  }
  
  /**
   * Shows an error dialog with a title and content.
   * @param title - The title of the dialog.
   * @param content - The content of the dialog.
   */
  public static void showErrorDialog(String title, String content) {
    initializeAlertDialog(Alert.AlertType.ERROR, title, content).showAndWait();
  }
  
  /**
   * Shows a warning dialog with a title and content.
   * @param title - The title of the dialog.
   * @param content - The content of the dialog.
   */
  public static void showWarningDialog(String title, String content) {
    initializeAlertDialog(Alert.AlertType.WARNING, title, content).showAndWait();
  }
  
  /**
   * Shows a yes/no confirmation dialog with a title and content.
   * Checks if the user clicked the yes button.
   * @param title - The title of the dialog.
   * @param content - The content of the dialog.
   * @return boolean - True if the user clicked Yes, false otherwise.
   */
  public static boolean showYesNoConfirmationDialog(String title, String content) {
    ButtonType yes = new ButtonType("Yes", ButtonBar.ButtonData.OK_DONE);
    ButtonType no = new ButtonType("No", ButtonBar.ButtonData.CANCEL_CLOSE);
    Alert confirmationDialog = initializeAlertDialog(Alert.AlertType.CONFIRMATION, title, content);
    confirmationDialog.getButtonTypes().setAll(yes, no);
    Optional<ButtonType> result = confirmationDialog.showAndWait();
    return result.get() == yes;
  }
  
  /**
   * Shows a confirmation dialog with a title and content.
   * Checks if the user clicked the ok button.
   * @param title - The title of the dialog.
   * @param content - The content of the dialog.
   * @return boolean - True if the user clicked OK, false otherwise.
   */
  public static boolean showOkConfirmationDialog(String title, String content) {
    Alert confirmationDialog = initializeAlertDialog(Alert.AlertType.CONFIRMATION, title, content);
    Optional<ButtonType> result = confirmationDialog.showAndWait();
    return result.get() == ButtonType.OK;
  }
}
