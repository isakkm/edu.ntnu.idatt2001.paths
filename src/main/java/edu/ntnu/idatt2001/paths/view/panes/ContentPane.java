package edu.ntnu.idatt2001.paths.view.panes;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

/**
 * Represents a content pane for displaying a passage's content.
 * Inherits properties from StackPane.
 *
 * @author Jens Christian Aanestad
 * @version 1.0
 * @see StackPane
 */
public class ContentPane extends StackPane {
  
  /**
   * Constructor to initialize a content pane.
   *
   * @param content String - the content to be displayed.
   */
  public ContentPane(String content) {
    super();
    
    // Styling
    setStyle(""
            + "-fx-background-color: rgba(0, 0, 0, 0.4);"
            + "-fx-border-color: white; "
            + "-fx-background-radius: 10px; "
            + "-fx-border-radius: 10px");
    setMinHeight(50);
    setMaxWidth(600);
    
    // Content label
    Label contentLabel = new Label(content);
    contentLabel.setStyle(""
            + "-fx-font-size: 15;"
            + "-fx-text-fill: white;");
    contentLabel.setPadding(new javafx.geometry.Insets(10));
    contentLabel.setAlignment(Pos.CENTER);
    contentLabel.setMinHeight(60);
    contentLabel.setWrapText(true);
    getChildren().add(contentLabel);
  }
}
