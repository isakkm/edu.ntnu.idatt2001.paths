package edu.ntnu.idatt2001.paths.io;

import edu.ntnu.idatt2001.paths.model.Passage;
import edu.ntnu.idatt2001.paths.model.game.Game;
import edu.ntnu.idatt2001.paths.model.game.GameLog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 * Represent the serializer for game progress objects.
 * This class is using the singleton design pattern in order to give global
 * access and create one instance of this class. This is done in order
 * to avoid two or more threads serializing a file simultaneously.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 1.0
 */
public class ProgressSerializer {
  private static ProgressSerializer instance;
  
  /**
   * Singleton constructor with private access modifier.
   */
  private ProgressSerializer() {}
  
  /**
   * Returns an instance of the PathsSerializer with lazy singleton.
   * Instance is created only when it is requested for the first time.
   *
   * @return PathsSerializer - the instance of the PathsSerializer class.
   */
  public static synchronized ProgressSerializer getInstance() {
    if (instance == null) {
      instance = new ProgressSerializer();
    }
    return instance;
  }
  
  /**
   * Serializes given game and the active passage the user was on when
   * pressing the save progress button. Serializes game and passage as
   * an object list in chosen file.
   *
   * @param file File - the file to serialize the game and passage in.
   * @param game Game - the game to serialize.
   * @param passage Passage - the passage to serialize.
   * @throws IOException if fails to locate file, or game/passage is incomplete.
   */
  public void serializeGameProgress(File file, Game game, Passage passage, GameLog gameLog) throws IOException {
    ObjectOutputStream os = null;
    FileOutputStream fs = null;
    List<Object> progress = List.of(game, passage, gameLog);
    try {
      fs = new FileOutputStream(file);
         os = new ObjectOutputStream(fs);
         os.writeObject(progress);
    } finally {
      if (os != null) {
        os.close();
        fs.close();
      }
    }
  }
}
