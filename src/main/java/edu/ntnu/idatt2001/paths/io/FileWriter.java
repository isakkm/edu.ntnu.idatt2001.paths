package edu.ntnu.idatt2001.paths.io;

import edu.ntnu.idatt2001.paths.model.Link;
import edu.ntnu.idatt2001.paths.model.Story;

import java.io.*;

/**
 * Represents the file writer for the paths game in
 * order to save a story. Note that this version does not
 * support Actions yet.
 * Here the classes FileWriter, BufferedWriter, PrintWriter
 * are used (chaining) with try-resource.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 1.0
 */
public class FileWriter {
  
  /**
   * Writes a story object to a paths (txt) a
   * chosen file on a strict format.
   *
   * @param file File - the location of the .paths file to write on.
   * @param story Story - the story object to write in file.
   * @throws IOException if the file is not a .paths file, or
   * cannot find the given location.
   */
  public void writeStory(File file, Story story) throws IOException {
    if (!file.getName().endsWith(".paths")) {
      throw  new IOException("Unsupported file format. Only .paths-files are supported.");
    }
    
    // Chaining with try-resource to automatically close the file if it fails.
    try (java.io.FileWriter fileWriter = new java.io.FileWriter(file);
         BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
         PrintWriter printWriter = new PrintWriter(bufferedWriter)) {
      
      // StringBuilder is used to write to correct format.
      StringBuilder storyString = new StringBuilder();
      
      // Story title.
      storyString.append(story.getTitle()).append("\n\n");
      
      // Opening passage.
      storyString.append("::").append(story.getOpeningPassage().getTitle()).append("\n");
      storyString.append(story.getOpeningPassage().getContent()).append("\n");
      story.getOpeningPassage().getLinks().forEach(link -> {
        storyString.append("[").append(link.getText()).append("]").
                append("(").append(link.getReference()).append(")").append("{");
        iterateThroughActions(storyString, link);
        storyString.append("}").append("\n");
      });
      storyString.append("\n");
      
      // Must remove opening passage from remaining passages in order to avoid duplicate.
      story.removePassage(new Link(story.getOpeningPassage().getTitle()
              , story.getOpeningPassage().getTitle()));
      
      // The remaining passages.
      story.getPassages().forEach(passage -> {
        storyString.append("::").append(passage.getTitle()).append("\n");
        storyString.append(passage.getContent()).append("\n");
        passage.getLinks().forEach(link -> {
          storyString.append("[").append(link.getText()).append("]").
                  append("(").append(link.getReference()).append(")").append("{");
          iterateThroughActions(storyString, link);
          storyString.append("}").append("\n");
        });
        storyString.append("\n");
      });
      
      printWriter.print(storyString);
      
    } catch (IOException e) {
      throw  new IOException("Cannot find the location of given file.");
    }
  }
  
  /**
   * Returns true if method successfully writes a story object to a newly created paths (txt)
   * file on a strict format. If the file already exist, then no
   * changes will be made to existing file and the method will return false. If you want to write over
   * an existing file, then use the writeStory-method.
   *
   * @param file File - the location of the .paths file to write on.
   * @param story Story - the story object to write in file.
   * @throws IOException if the file is not a .paths file, or
   * cannot find the given location.
   */
  public boolean writeStoryToNewFile(File file, Story story) throws IOException {
    if (!file.getName().endsWith(".paths")) {
      throw  new IOException("Unsupported file format. Only .paths-files are supported.");
    }
    boolean writeSucceeded = false;
    if (file.createNewFile()) {
      writeStory(file, story);
      writeSucceeded = true;
    }
    return writeSucceeded;
  }

  /**
   * Iterates through Actions in link and adds them to
   * a StringBuilder.
   *
   * @param storyString StringBuilder - the StringBuilder to add new content.
   * @param link Link - the link's actions to iterate and add to storyBuilder.
   */
  private void iterateThroughActions(StringBuilder storyString, Link link) {
    if (link.getActions().size() > 0) {
      if (link.getActions().size() == 1) {
        storyString.append(link.getActions().get(0).toPathsFormatString());
      } else {
        for (int i = 0; i < link.getActions().size() - 1; i++) {
          storyString.append(link.getActions().get(i).toPathsFormatString()).append("; ");
        }
        storyString.append(link.getActions().get(link.getActions().size()-1).toPathsFormatString());
      }
    }
  }
}