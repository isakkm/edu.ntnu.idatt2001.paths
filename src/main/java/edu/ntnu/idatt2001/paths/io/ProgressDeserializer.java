package edu.ntnu.idatt2001.paths.io;

import java.io.*;
import java.util.List;

/**
 * Represent the deserializer for game progress objects.
 * This class is using the singleton design pattern in order to give global
 * access and create one instance of this class. This is done in order
 * to avoid two or more threads deserializing a file simultaneously.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 1.0
 */
public class ProgressDeserializer {
  private static ProgressDeserializer instance;
  
  /**
   * Singleton constructor with private access modifier.
   * Instance is created only when it is requested for the first time.
   */
  private ProgressDeserializer() {}
  
  /**
   * Returns an instance of the PathsDeserializer with lazy singleton.
   *
   * @return PathsDeserializer - the instance of the PathsDeserializer class.
   */
  public static synchronized ProgressDeserializer getInstance() {
    if (instance == null) {
      instance = new ProgressDeserializer();
    }
    return instance;
  }
  
  /**
   * Deserialize a file and returns the object from that file.
   *
   * @param file File - the file to deserialize.
   * @return Object - the deserialized object from the file.
   * @throws IOException if fails to locate file, or object is incomplete.
   */
  public Object deserializeGameProgress(File file) throws IOException, RuntimeException {
    FileInputStream fs = null;
    ObjectInputStream is = null;
    List<Object> progress = null;
    try {
      fs = new FileInputStream(file);
      is = new ObjectInputStream(fs);
      progress = (List<Object>) is.readObject();
    } catch (ClassNotFoundException e) {
      throw new RuntimeException(e);
    } finally {
      if (is != null) {
        is.close();
        fs.close();
      }
    }
    return progress;
  }
}
