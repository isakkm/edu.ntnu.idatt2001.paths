package edu.ntnu.idatt2001.paths.io;

import edu.ntnu.idatt2001.paths.exceptions.UnplayableStoryException;
import edu.ntnu.idatt2001.paths.exceptions.WrongFileExtensionException;
import edu.ntnu.idatt2001.paths.exceptions.WrongStoryFormatException;
import edu.ntnu.idatt2001.paths.model.Link;
import edu.ntnu.idatt2001.paths.model.Passage;
import edu.ntnu.idatt2001.paths.model.Story;

import edu.ntnu.idatt2001.paths.model.actions.*;
import edu.ntnu.idatt2001.paths.validation.FormatValidation;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Represents the file reader for the paths game.
 * Note that this version does not support actions yet.
 * The class Scanner is here used to parse the text.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 1.0
 */
public class FileReader {
  private boolean openingPassageFound = false;
  private final List<String> lineErrors = new ArrayList<>();
  private int currentLine = 0;
  
  /**
   * Reads a paths (txt) file on a strictly given format. Note that the paths-file
   * must have the story title in the first line. Each passage's title must begin with double
   * colon :: and the links must be on the format [Text](Reference). Each passage must
   * at least be seperated by an empty line. This reader also supports action. The format for
   * adding actions is to use curly brackets {@literal {}} and separate action type and action value
   * with colon. If it several actions needs to be added to same link, separate the actions inside the
   * curly brackets with semicolon. Here are examples:
   * <pre>[link text](passage title){ActionType:value}</pre>
   * <pre>or [link text](passage title){ActionType:value;ActionType:value;...}</pre>
   * <pre>or [link text](passage title) {ActionType:value ; ActionType:value ; ...}</pre>
   * <pre>or [link text] (passage title) {ActionType:value}...</pre>
   * <pre>or [link text](passage title){}</pre>
   * <pre>or [link text](passage title)</pre>
   *
   * @param file File - the .paths file to be read.
   * @return Story - the converted story object from the file.
   * @throws IOException if the file format not includes .paths-files, or if
   * the location of the file is not found.
   */
  public Story readStory(File file) throws IOException
          , WrongStoryFormatException, WrongFileExtensionException, UnplayableStoryException {
    
    if (!file.getName().endsWith(".paths")) {
      throw  new WrongFileExtensionException("Unsupported file format. Only .paths-files are supported.");
    }
    Story story;
    String title;
    Passage openingPassage = null;
    Map<Link, Passage> passages = new HashMap<>();
    
    try {
      Scanner scanner = new Scanner(file);
  
      // The first line must be the story's title.
      title = scanner.nextLine();
      currentLine++;
      if (title.isBlank()) {
        lineErrors.add("Error in line " + currentLine + ": "
                + title + " - The first line must be the story's title and cannot be empty.");
      }
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        currentLine++;
  
        // Each passage begins with "::".
        if (FormatValidation.isValidPassageStart(line)) {
          String passageTitle = line.substring(2);
          // The next line must be the passage's content.
          String passageContent = scanner.nextLine();
          currentLine++;
          try {
            // Check if the expected passage content is on link format.
            if (FormatValidation.isValidLinkAndActionLine(passageContent)
                    || FormatValidation.isValidPassageStart(passageContent)) {
              throw new WrongStoryFormatException("Error in line " + currentLine + ": "
                      + passageContent + " - The passage's content can not be on link format.");
            }
          } catch (WrongStoryFormatException e) {
            lineErrors.add(e.getMessage());
          }
          List<Link> links = new ArrayList<>();
          String newLine = scanner.nextLine();
          currentLine++;
          
          // Reads the links and belonging actions.
          while (!newLine.isBlank()) {
            if (FormatValidation.isValidLinkAndActionLine(newLine)) {
              try {
                links.add(readLink(newLine));
              } catch (WrongStoryFormatException | NumberFormatException e) {
                lineErrors.add(e.getMessage());
              }
            } else {
              try {
                throw new WrongStoryFormatException("Error in line " + currentLine + ": "
                        + newLine + " - Wrong link format.");
              } catch (WrongStoryFormatException e) {
                lineErrors.add(e.getMessage());
              }
            }
            if (scanner.hasNextLine()) {
              newLine = scanner.nextLine();
              currentLine++;
            } else newLine = ""; // In order to avoid NullPointerException.
          }
          
          Passage passage = new Passage(passageTitle, passageContent, links);
          passages.put(new Link(passage.getTitle(), passage.getTitle()), passage);
          
          // Since the first passage is the opening passage.
          if (!openingPassageFound) {
            openingPassage = new Passage(passageTitle, passageContent, links);
            openingPassageFound = true;
          }
          
          // In order to follow to format strictly, there must be at least one empty line between passages.
          try {
            FormatValidation.isValidLineSeparation(scanner, newLine, currentLine);
          } catch (WrongStoryFormatException e) {
            lineErrors.add(e.getMessage());
          }
        }
      }
      scanner.close();
      if (lineErrors.size() != 0) {
        throw new UnplayableStoryException("It appears to be " + lineErrors.size() + " format errors in the paths-file.");
      }
    } catch (IOException e) {
      throw  new IOException("Cannot find the location of given file.");
    }
    story = new Story(title, passages, openingPassage);
    return story;
  }
  
  /**
   * Reads a link from a given line.
   *
   * @param line String - the line to read the link from.
   * @return Link - the parsed link from the line.
   */
  private Link readLink(String line) {
    String linkText = line.substring(line.indexOf('['), line.indexOf(']'))
            .replaceAll("[\\[\\]]", ""); // Removes the square brackets.
    String linkReference = line.substring(line.indexOf('('), line.indexOf(')'))
            .replaceAll("[()]", ""); // Removes the parenthesis.
    List<Action> listOfActions = readActions(line);
    
    return new Link(linkText, linkReference, listOfActions);
  }
  
  /**
   * Reads the actions from a given line.
   *
   * @param line String - the line to read the actions from.
   * @return listOfAction List - a list of actions.
   * @throws WrongStoryFormatException if
   */
  private List<Action> readActions(String line) throws WrongStoryFormatException, NumberFormatException {
    List<Action> listOfAction = new ArrayList<>();
    if (line.contains("{") && line.contains("}")) {
      String linkActions = line.substring(line.indexOf('{'), line.lastIndexOf('}'))
              .replaceAll("[\\{\\}]", "").trim();
    
      String[] actions = linkActions.split("(\\s+)?;(\\s+)?"); // Splits with ";" or "{ ; }" with spaces.
      
      for (String action : actions) {
        String[] actionValues = action.split(":"); // Splits the action on a format to action type and value.
        switch (actionValues[0]) {
          case "GoldAction" -> {
            FormatValidation.isValidActionValues(actionValues[1], currentLine, actionValues[0]);
            listOfAction.add(new GoldAction(Integer.parseInt(actionValues[1])));
          }
          case "HealthAction" -> {
            FormatValidation.isValidActionValues(actionValues[1], currentLine, actionValues[0]);
            listOfAction.add(new HealthAction(Integer.parseInt(actionValues[1])));
          }
          case "InventoryAction" -> listOfAction.add(new InventoryAction(actionValues[1]));
  
          case "RemoveInventoryAction" -> listOfAction.add(new RemoveInventoryAction(actionValues[1]));
  
          case "ScoreAction" -> {
            FormatValidation.isValidActionValues(actionValues[1], currentLine, actionValues[0]);
            listOfAction.add(new ScoreAction(Integer.parseInt(actionValues[1])));
          }
          case "ResetAllAction" -> listOfAction.add(new ResetAllAction());
  
          case "" -> {}
  
          default -> throw new WrongStoryFormatException("Error in line " + actionValues[0]
                  + ": " + actionValues[1] + " - is not a valid action");
        }
      }
    }
    return listOfAction;
  }
  
  /**
   * Returns the list with all line errors.
   *
   * @return lineErrors List - a list of all line errors.
   */
  public List<String> getLineErrors() {
    return lineErrors;
  }
}