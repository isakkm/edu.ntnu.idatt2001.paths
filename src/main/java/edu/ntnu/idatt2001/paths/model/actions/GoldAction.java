package edu.ntnu.idatt2001.paths.model.actions;

import edu.ntnu.idatt2001.paths.model.player.Player;

/**
 * GoldAction class.
 * Adds/subtracts the player's amount of gold when executed.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 17.0.6
 */
public class GoldAction implements Action {
  private final int gold;
  
  /**
   * The constructor for the gold action.
   *
   * @param gold int - the number of gold to add or subtract
   */
  public GoldAction(int gold) {
    this.gold = gold;
  }
  
  /**
   * Adds gold to the player's gold bar with calling the
   * addGold method from Player.
   *
   * @param player Player - the player to execute an action
   */
  @Override
  public void execute(Player player) {
    player.addGold(gold);
  }

  /**
   * Returns a String representation of the Gold Action to
   * a specified format.
   *
   * @return String - String representation of the Gold Action
   */
  @Override
  public String toPathsFormatString() {
    return "GoldAction:" + gold;
  }
}