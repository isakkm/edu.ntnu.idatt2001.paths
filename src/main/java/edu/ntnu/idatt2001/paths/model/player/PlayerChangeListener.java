package edu.ntnu.idatt2001.paths.model.player;

import java.io.Serializable;

/**
 * Represents a player listener which is notified when the player changes.
 *
 * @author Jens Christian Aanestad
 * @version 1.0
 * @see Player
 */
public interface PlayerChangeListener extends Serializable {
  
  /**
   * Called when the player's attributes has been called.
   */
  void playerChanged(Player player);
}
