package edu.ntnu.idatt2001.paths.model.actions;

import edu.ntnu.idatt2001.paths.model.player.Player;

import java.io.Serializable;

/**
 * Action Interface.
 * An interface with a method to
 * execute an action at a given time.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 17.0.6
 */
public interface Action extends Serializable {
  /**
   * Executes an action to the player.
   * Modifies the player's state.
   *
   * @param player Player - the player to execute an action
   * @throws NullPointerException if the player parameter is null
   */
  void execute(Player player);

  /**
   * Returns a representation of the action on a specified format.
   * This method is used to give action support for the file writer.
   * Example: "GoldAction:20" Where the action is a gold action which adds
   * 20 gold to the player.
   *
   * @return String - a string representation to a specified forat
   */
  String toPathsFormatString();
}