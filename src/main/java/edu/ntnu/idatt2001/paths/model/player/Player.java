package edu.ntnu.idatt2001.paths.model.player;

import java.io.Serializable;
import java.util.*;

/**
 * Representing the player of the game.
 * The player has a name, a health bar,
 * an amount of gold and score points and an inventory.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 1.0
 */
public class Player implements Serializable {
  private final String name;
  private int health;
  private int score;
  private int gold;
  private Map<String, Integer> inventory;
  private List<PlayerChangeListener> playerChangeListeners;
  // In order to reset to initial values.
  public final int INITIAL_GOLD;
  public final int INITIAL_HEALTH;
  public final int INITIAL_SCORE;

  /**
   * Private Player constructor with the builder design patter.
   *
   * @param builder Builder - the class builder
   */
  private Player(Builder builder) {
    this.name = builder.name;
    this.health = builder.health;
    this.score = builder.score;
    this.gold = builder.gold;
    this.inventory = builder.inventory;
    this.playerChangeListeners = builder.playerChangeListeners;
    this.INITIAL_GOLD = gold;
    this.INITIAL_HEALTH = health;
    this.INITIAL_SCORE = score;
  }

  /**
   * The builder for the construction of an object. In this
   * Builder class the name is required for the Player.
   * Every other attribute is optional.
   */
  public static class Builder {
    // Required
    private final String name;
    // Optional
    private int health = 100;
    private int score = 0;
    private int gold = 0;
    private Map<String, Integer> inventory = new HashMap<>();
    private List<PlayerChangeListener> playerChangeListeners = new ArrayList<>();
    int INITIAL_GOLD = 0;
    int INITIAL_HEALTH = 0;
    int INITIAL_SCORE = 0;

    /**
     * The Builder constructor.
     *
     * @param name String - the required name for the player
     */
    public Builder(String name) {
      if (name == null || name.isBlank()) {
        throw new IllegalArgumentException("Name cannot be null or blank");
      }
      this.name = name;
    }
  
    /**
     * Builder method to set health.
     *
     * @param health int - the player's health.
     * @return Builder.
     */
    public Builder health(int health) {
      if (health <= 0) {
        throw new IllegalArgumentException("Health cannot be zero or lower than zero");
      }
      this.health = health;
      INITIAL_HEALTH = health;
      return this;
    }
  
    /**
     * Builder method to set score.
     *
     * @param score int - the player's score.
     * @return Builder.
     */
    public Builder score(int score) {
      if (score < 0) {
        throw new IllegalArgumentException("Score cannot be lower than zero");
      }
      this.score = score;
      INITIAL_SCORE = score;
      return this;
    }
  
    /**
     * Builder method to set gold.
     *
     * @param gold int - the player's gold.
     * @return Builder.
     */
    public Builder gold(int gold) {
      if (gold < 0) {
        throw new IllegalArgumentException("Gold cannot be lower than zero");
      }
      this.gold = gold;
      INITIAL_GOLD = gold;
      return this;
    }
  
    /**
     * Builder method to set inventory.
     *
     * @param inventory Map{@literal <}String, Integer> - the player's inventory.
     * @return Builder.
     */
    public Builder inventory(Map<String, Integer> inventory) {
      if (inventory.keySet().stream().anyMatch(Objects::isNull)) {
        throw new IllegalArgumentException("The items in the inventory cannot be null values");
      }
      this.inventory = inventory;
      return this;
    }
  
    /**
     * Builder method to set change listeners.
     *
     * @param playerChangeListeners List{@literal <}String> - the player's change listeners.
     * @return Builder.
     */
    public Builder playerChangeListeners(List<PlayerChangeListener> playerChangeListeners) {
      if (playerChangeListeners.stream().anyMatch(Objects::isNull)) {
        throw new IllegalArgumentException("The changeListeners cannot be null values");
      }
      this.playerChangeListeners = playerChangeListeners;
      return this;
    }
  
    /**
     * Builds player object.
     *
     * @return Player - new player.
     */
    public Player build() {
      return new Player(this);
    }
  }
  
  /**
   * Returns a boolean for whether player is alive or not.
   * Returns true if alive.
   *
   * @return boolean - true if player is alive.
   */
  public boolean isAlive() {
    return getHealth() > 0;
  }
  
  /**
   * Sets the player's health to a new value.
   *
   * @param health int - the new health value.
   */
  public void setHealth(int health) {
    this.health = health;
    notifyPlayerChangeListeners();
  }
  
  /**
   * Sets the player's score to a new value.
   *
   * @param score int - the new score value.
   */
  public void setScore(int score) {
    this.score = score;
    notifyPlayerChangeListeners();
  }
  
  /**
   * Sets the player's gold amount to a new value.
   * @param gold int - the new amount of gold value.
   */
  public void setGold(int gold) {
    this.gold = gold;
    notifyPlayerChangeListeners();
  }
  
  /**
   * Clears out the player's inventory. Removes all items.
   */
  public void clearInventory() {
    if (this.inventory.size() != 0) {
      this.inventory.clear();
      notifyPlayerChangeListeners();
    }
  }
  
  /**
   * Returns the player's name.
   *
   * @return String - the player's name
   */
  public String getName() {
    return name;
  }
  
  /**
   * Adds health to the player's health bar. If it gets below zero
   * then the health will be set to zero
   *
   * @param health int - number to be added/subtracted to player's health.
   */
  public void addHealth(int health) {
    this.health += health;
    if (this.health < 0) {
      this.health = 0;
    }
    notifyPlayerChangeListeners();
  }
  
  /**
   * Returns the player's health.
   *
   * @return int - number representing the players health
   */
  public int getHealth() {
    return health;
  }
  
  /**
   * Adds score points to the player's total score.
   * The score can not negative.
   *
   * @param score int - number to add with the player's score
   */
  public void addScore(int score) {
    this.score += score;
    if (this.score < 0) {
      this.score = 0;
    }
  }
  
  /**
   * Returns the player's score.
   *
   * @return int - number representing the player's score
   */
  public int getScore() {
    return score;
  }
  
  /**
   * Adds gold to the player's amount of gold.
   * The amount of gold can not be negative
   *
   * @param gold int - number to add with the player's amount of gold
   */
  public void addGold(int gold) {
    this.gold += gold;
    if (this.gold < 0) {
      this.gold = 0;
    }
    notifyPlayerChangeListeners();
  }
  
  /**
   * Returns the player's gold.
   *
   * @return int - number representing the player's score
   */
  public int getGold() {
    return gold;
  }
  
  /**
   * Adds an item to the player's inventory.
   *
   * @param item String - text representing an item.
   */
  public void addToInventory(String item) throws IllegalArgumentException {
    if (item == null || item.isBlank()) {
      throw new IllegalArgumentException("Item cannot be null or blank");
    }
    if (this.inventory.containsKey(item)) {
      this.inventory.put(item, inventory.get(item) + 1);
    } else {
      this.inventory.put(item, 1);
    }
    notifyPlayerChangeListeners();
  }
  
  /**
   * Removes an item from the player's inventory.
   *
   * @param item String - text representing the item to be removed.
   */
  public void removeFromInventory(String item) throws IllegalArgumentException {
    if (item == null || item.isBlank()) {
      throw new IllegalArgumentException("Item cannot be null or blank");
    }
    if (this.inventory.containsKey(item)) {
      if (this.inventory.get(item) > 1) {
        this.inventory.put(item, inventory.get(item) - 1);
      }
      else {
        this.inventory.remove(item);
      }
      notifyPlayerChangeListeners();
    }
  }
  
  /**
   * Returns the players inventory.
   *
   * @return Map - map of the player's items
   */
  public Map<String, Integer> getInventory() {
    return new HashMap<>(inventory);
  }
  
  /**
   * Sets the players inventory.
   *
   * @param inventory Map{@literal <}String, Integer> - the map of items to set the inventory with.
   */
  public void setInventory(Map<String, Integer> inventory) {
    this.inventory = inventory;
    notifyPlayerChangeListeners();
  }
  
  
  /**
   * Returns the list of change listeners
   *
   * @return List {@literal <}PlayerChangeListeners>
   */
  public List<PlayerChangeListener> getPlayerChangeListeners() {
    return this.playerChangeListeners;
  }
  
  /**
   * Adds a listener to the player change listener list.
   *
   * @param playerChangeListener PlayerChangeListener - the listener to be added.
   */
  public void addPlayerChangeListener(PlayerChangeListener playerChangeListener) {
    this.playerChangeListeners.add(playerChangeListener);
  }
  
  /**
   * Sets the player change listener list to a new list.
   *
   * @param playerChangeListeners PlayerChangeListener - new listener list.
   */
  public void setPlayerChangeListeners(List<PlayerChangeListener> playerChangeListeners) {
    this.playerChangeListeners = playerChangeListeners;
  }
  
  /**
   * Notifies all listeners in the player change listener list that the player has changed.
   */
  private void notifyPlayerChangeListeners() {
    for (PlayerChangeListener playerChangeListener : playerChangeListeners) {
      playerChangeListener.playerChanged(this);
    }
  }
}