package edu.ntnu.idatt2001.paths.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Represents a passage in the story where it
 * is possible to enter another passage.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 1.0
 * @see Serializable
 */
public class Passage implements Serializable {
  private final String title;
  private final String content;
  private final List<Link> links;
  
  /**
   * The main constructor.
   * Initializes a new Passage object.
   * The title operates as an identifier
   *
   * @param title   String - the title to the passage
   * @param content String - text representing a paragraph
   * @param links   List   - a list of links which leads to another passage
   * @throws IllegalArgumentException if title is null/whitespaces or if content is null/whitespaces
   * or if links is null
   */
  public Passage(String title, String content, List<Link> links) throws IllegalArgumentException {
    if (title == null || title.isBlank()) {
      throw new IllegalArgumentException("The title which operates as an identifier can not be null or whitespaces");
    }
    if (content == null || content.isBlank()) {
      throw new IllegalArgumentException("The content can not be null or whitespaces");
    }
    if (links == null || links.stream().anyMatch(Objects::isNull)) {
      throw new IllegalArgumentException("The list of links can not be null or contain null values");
    }
    this.title = title;
    this.content = content;
    this.links = new ArrayList<>();
    this.links.addAll(links);
  }
  
  /**
   * Initializes a new passage object.
   * The title operates as an identifier.
   *
   * @param title   String - the title to a passage
   * @param content String - text representing a paragraph
   * @throws IllegalArgumentException if title is null/whitespaces or if content is null/whitespaces
   * or if links is null
   */
  public Passage(String title, String content) throws IllegalArgumentException {
    this(title, content, new ArrayList<>());
  }
  
  /**
   * Returns the title of the passage.
   *
   * @return String - title of the passage
   */
  public String getTitle() {
    return title;
  }
  
  /**
   * Returns the content of the passage.
   *
   * @return String - content of the passage
   */
  public String getContent() {
    return content;
  }
  
  /**
   * Returns the links that are available from this passage.
   *
   * @return List - paths that are available
   */
  public List<Link> getLinks() {
    return links;
  }
  
  /**
   * Adds a link to the list of links for the passage.
   * If the link is already in the list, it will return false.
   * Returns true if the link was added.
   *
   * @param link List - paths that are available
   * @return boolean  - true if link was added, false otherwise
   */
  public boolean addLink(Link link) throws IllegalArgumentException {
    if (link == null) {
      throw new IllegalArgumentException("The link cannot be null");
    }
    if (getLinks().contains(link)) {
      return false;
    }
    return links.add(link);
  }
  
  /**
   * Checks if list of links contains links.
   *
   * @return boolean - true if there are links, false otherwise
   */
  public boolean hasLinks() {
    return !links.isEmpty();
  }
  
  /**
   * Returns a String representation of the passage.
   *
   * @return String - string representation of the passage
   */
  @Override
  public String toString() {
    return "Passage: "
            + "title: " + title
            + ", connected links: " + links.size();
  }
  
  /**
   * Checks if two passages are equal by comparing titles as identifier.
   *
   * @param o Object - the object to compare with
   * @return boolean - true if the titles are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Passage passage)) {
      return false;
    }
    return Objects.equals(getTitle(), passage.getTitle());
  }
  
  /**
   * Returns the hashcode to the title of the passage.
   *
   * @return int - hashcode of the passage
   */
  @Override
  public int hashCode() {
    return Objects.hash(getTitle());
  }
}
