package edu.ntnu.idatt2001.paths.model.actions;

import edu.ntnu.idatt2001.paths.model.player.Player;

/**
 * RemoveInventoryAction class.
 * Removes an item from the player's inventory when executed.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 17.0.6
 */
public class RemoveInventoryAction implements Action {
  private final String item;
  
  /**
   * The constructor for the Remove Inventory action.
   *
   * @param item String - a text representing an item
   * to be removed from the player's inventory.
   * @throws IllegalArgumentException if the item is null or blank/whitespace.
   */
  public RemoveInventoryAction(String item) throws IllegalArgumentException {
    if (item == null || item.isBlank()) {
      throw new IllegalArgumentException("Item cannot be null or whitespaces");
    }
    this.item = item;
  }
  
  /**
   * Removes an item from the player's inventory with calling the
   * removeFromInventory method from Player.
   *
   * @param player Player - the player to execute an action
   */
  @Override
  public void execute(Player player) {
    player.removeFromInventory(item);
  }
  
  /**
   * Returns a String representation of the Remove Inventory Action to
   * a specified format.
   *
   * @return String - String representation of the Inventory Action
   */
  @Override
  public String toPathsFormatString() {
    return "RemoveInventoryAction:" + item;
  }
}
