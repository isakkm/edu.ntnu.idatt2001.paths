package edu.ntnu.idatt2001.paths.model.game;

import edu.ntnu.idatt2001.paths.model.Link;
import edu.ntnu.idatt2001.paths.model.Passage;
import edu.ntnu.idatt2001.paths.model.player.Player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents the game log, an addition to the game where it holds
 * previous player states, passages, and previously entered links. In this
 * way the application can undo.
 *
 * @author Jens Christian Aanestad
 * @version 1.0
 * @see Game
 */
public class GameLog implements Serializable {
  private final List<Passage> passages = new ArrayList<>();
  private final List<Player> players = new ArrayList<>();
  private final List<Link> links = new ArrayList<>();
  
  /**
   * Returns previous passage before current passage.
   * Checks if the undo is valid, and returns previous passage.
   * Returns opening passage if undo is invalid. (Basically the only
   * passage where undo is invalid.)
   *
   * @param game Game - the game to undo.
   * @return Passage - the previous passage.
   */
  public Passage undo(Game game) {
    if (isValidUndo()) {
      // Resets the player's attributes to last passage's stats.
      undoPlayer(game.getPlayer());
      // Removes the current player (Last element in players).
      players.remove(players.size() - 1);
      
      if (links.size() >= 1) {
        links.remove(links.size() - 1);
      }
  
      // Presumed that the last element in passages is the current passage the player is in.
      passages.remove(passages.size() - 1);
      // Returns the previous passage. Since the current passage is removed above, 1 is subtracted from index.
      return game.go(new Link(passages.get(passages.size() - 1).getTitle(), passages.get(passages.size() - 1).getTitle()));
    }
    else {
      // Returns opening passage.
      return game.begin();
    }
  }
  
  // Note that this method do not take a link parameter. That is because
  // this method is mainly called when initializing a game pane.
  /**
   * Adds passage and player to the passages and player lists.
   *
   * @param passage Passage - the passage to add in passages list.
   * @param player Player - the player to add in player list.
   */
  public void addLog(Passage passage, Player player) {
    passages.add(passage);
    players.add(new Player.Builder(player
            .getName())
            .gold(player.getGold())
            .score(player.getScore())
            .health(player.getHealth())
            .inventory(player.getInventory())
            .playerChangeListeners(player.getPlayerChangeListeners())
            .build());
  }
  
  // Is separated from addLogg because this method on only gets called when going through a link.
  /**
   * Adds a link to the links list.
   *
   * @param link Link - the link to add in links list.
   */
  public void addLink(Link link) {
    links.add(link);
  }
  
  /**
   * Resets a player's attributes to the attributes in previous passage.
   *
   * @param player Player - the player to reset attributes.
   */
  public void undoPlayer(Player player) {
    // Since method is called before current player is removed,
    // 2 is subtracted from index.
    player.setGold(players.get(players.size() - 2).getGold());
    player.setHealth(players.get(players.size() - 2).getHealth());
    player.setScore(players.get(players.size() - 2).getScore());
    player.setInventory(players.get(players.size() - 2).getInventory());
    player.setPlayerChangeListeners(players.get(players.size() - 2).getPlayerChangeListeners());
    if (links.size() >= 2) {
      links.get(links.size() - 2).executeActions(player);
    }
  }
  
  /**
   * Returns a boolean for if undo is possible/valid.
   *
   * @return Returns true if the passages list is greater than 1.
   */
  public boolean isValidUndo() {
    return passages.size() >= 2;
  }
}
