package edu.ntnu.idatt2001.paths.model.goals;

import edu.ntnu.idatt2001.paths.model.player.Player;

/**
 * GoldGoal class.
 * Checks if a player has equal or more gold compared to
 * a certain amount of gold.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 17.0.6
 */
public class GoldGoal implements Goal {
  private final int minimumGold;
  
  /**
   * The constructor for the gold goal.
   * Creates a gold goal which compares the player's
   * amount of gold with a minimum amount of gold.
   * Since the player start with 0 gold, a goal can therefore
   * never be less than 1.
   *
   * @param minimumGold int - a number that
   *                      represent expected minimum amount of gold
   * @throws IllegalArgumentException if the minimum amount of gold is 1.
   */
  public GoldGoal(int minimumGold) throws IllegalArgumentException {
    if (minimumGold < 1) {
      throw new IllegalArgumentException("The goal for gold can not be less than 1");
    }
    this.minimumGold = minimumGold;
  }
  
  /**
   * Returns the minimalGold value.
   *
   * @return minimumGold int.
   */
  @Override
  public Object getGoalValue() {
    return minimumGold;
  }
  
  /**
   * Returns true if player's amount of gold is above or equal to minimumGold.
   * Returns false if player's amount of gold is below minimumGold.
   *
   * @param player Player   - the player to check if goal is fulfilled
   * @return boolean        - true if player's amount of gold
   *                          is above or equal to minimumGold, false if not
   */
  @Override
  public boolean isFulfilled(Player player) {
    return player.getGold() >= minimumGold;
  }
}