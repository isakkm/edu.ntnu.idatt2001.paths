package edu.ntnu.idatt2001.paths.model.goals;

import edu.ntnu.idatt2001.paths.model.player.Player;

import java.util.List;
import java.util.Objects;

/**
 * InventoryGoal class.
 * Checks if a player has all certain times in the inventory.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 17.0.6
 */
public class InventoryGoal implements Goal {
  private final List<String> mandatoryItems;
  
  /**
   * The constructor for the inventory goal.
   * Creates a new inventory goal which assures that the player
   * has all the items in the specified mandatory list.
   * The list of mandatory items can not be null, can not contain null values,
   * can not contain whitespaces, or be empty.
   *
   * @param mandatoryItems List - a list of items that
   *                      represent an expected inventory with items
   * @throws IllegalArgumentException if the list is null or if there are no items or whitespaces in the list.
   */
  public InventoryGoal(List<String> mandatoryItems) throws IllegalArgumentException {
    if (mandatoryItems == null) {
      throw new IllegalArgumentException("The list of mandatory items cannot be null");
    }
    if (mandatoryItems.stream().anyMatch(Objects::isNull)) {
      // Uses the stream-anymatch method to see if an item is null
      throw new IllegalArgumentException("The items in the mandatory items can not have null values");
    }
    if (mandatoryItems.isEmpty()) {
      throw new IllegalArgumentException("The list of mandatory items cannot be empty");
    }
    if (mandatoryItems.stream().anyMatch(String::isBlank)) {
      // Uses the stream-anymatch method to see if an item is blank
      throw new IllegalArgumentException("The items in the mandatory items can not be whitespaces");
    }
    this.mandatoryItems = mandatoryItems;
  }
  
  /**
   * Returns the mandatoryItems list.
   *
   * @return mandatoryItems List.
   */
  @Override
  public Object getGoalValue() {
    return String.valueOf(mandatoryItems).replaceAll("[\\[\\]]", "");
  }
  
  /**
   * Returns true if player's inventory contains mandatoryItems.
   * Returns false if player's inventory do not contain all mandatoryItems.
   *
   * @param player Player   - the player to check if goal is fulfilled
   * @return boolean        - true if player's inventory has mandatoryItems,
   *                          false if not
   */
  @Override
  public boolean isFulfilled(Player player) {
    return player.getInventory().keySet().containsAll(mandatoryItems);
  }
}