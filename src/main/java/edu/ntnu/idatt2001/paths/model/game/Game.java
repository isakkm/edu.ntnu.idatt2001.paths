package edu.ntnu.idatt2001.paths.model.game;

import edu.ntnu.idatt2001.paths.model.Link;
import edu.ntnu.idatt2001.paths.model.Passage;
import edu.ntnu.idatt2001.paths.model.player.Player;
import edu.ntnu.idatt2001.paths.model.Story;
import edu.ntnu.idatt2001.paths.model.goals.Goal;

import java.io.Serializable;
import java.util.List;

/**
 * Represents the facade for a Paths-game.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 17.0.6
 * @see Serializable
 */
public class Game implements Serializable {
  private final Player player;
  private final Story story;
  private final List<Goal> goals;
  /**
   * Constructor for the Game class.
   * The player can not be null, and the story can not be null.
   * The list of goals can not be null and can not be empty.
   *
   * @param player  Player  - the player of the game
   * @param story   Story   - the story of the game
   * @throws IllegalArgumentException if the player is null, story is null or if the
   * list of goals is null or empty
   */
  public Game(Player player, Story story, List<Goal> goals) throws IllegalArgumentException {
    if (player == null) {
      throw new IllegalArgumentException("Player cannot be null");
    }
    if (story == null) {
      throw new IllegalArgumentException("Story cannot be null");
    }
    if (goals == null || goals.isEmpty()) {
      throw new IllegalArgumentException("Goals cannot be null or empty");
    }
    this.player = player;
    this.story = story;
    this.goals = goals;
  }
  
  /**
   * Returns the player of game.
   *
   * @return Player - the player of game
   */
  public Player getPlayer() {
    return player;
  }
  
  /**
   * Returns the story of the game.
   *
   * @return Story - the story of the game
   */
  public Story getStory() {
    return story;
  }
  
  /**
   * Returns the list of goals.
   *
   * @return goals List {@literal <}Goal> - list of goals.
   */
  public List<Goal> getGoals() {
    return goals;
  }
  
  /**
   * Returns the opening passage to the story.
   *
   * @return Passage - opening passage
   */
  public Passage begin() {
    return story.getOpeningPassage();
  }
  
  /**
   * Returns the passage connected to a given link.
   * Returns null if the link does not exist in the passages map.
   *
   * @param link Link - the link belonging to a passage
   * @return Passage  - the passage referred by given link
   * @throws IllegalArgumentException thrown if the link is incorrect
   */
  public Passage go(Link link) {
    return story.getPassage(link);
  }
}
