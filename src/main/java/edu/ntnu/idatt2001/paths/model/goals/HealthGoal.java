package edu.ntnu.idatt2001.paths.model.goals;

import edu.ntnu.idatt2001.paths.model.player.Player;

/**
 * HealthGoal class.
 * Checks if a player has a certain amount of health
 * remaining on the health bar.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal*
 * @version 17.0.6
 */
public class HealthGoal implements Goal {
  private final int minimumHealth;
  
  /**
   * The constructor for the health goal.
   * Creates a new health goal which compares the player's
   * health bar to certain minimum amount of health points.
   * Since a player is dead when the health bar reaches to zero,
   * the minimum health can not be less than 1.
   *
   * @param minimumHealth int - a number that
   *                      represent expected minimum health value
   * @throws IllegalArgumentException if the minimum health is less than 1
   */
  public HealthGoal(int minimumHealth) throws IllegalArgumentException {
    if (minimumHealth < 1) {
      throw new IllegalArgumentException("The goal for health cannot be lass than 1");
    }
    this.minimumHealth = minimumHealth;
  }
  
  /**
   * Returns the minimumHealth value.
   *
   * @return minimumHealth int.
   */
  @Override
  public Object getGoalValue() {
    return minimumHealth;
  }
  
  /**
   * Returns true if player's health value is above or equal to minimumHealth.
   * Returns false if player's health value is below minimumHealth.
   *
   * @param player Player   - the player to check if goal is fulfilled
   * @return boolean        - true if player's health value
   *                          is above minimumHealth, false if not
   */
  @Override
  public boolean isFulfilled(Player player) {
    return player.getHealth() >= minimumHealth;
  }
}