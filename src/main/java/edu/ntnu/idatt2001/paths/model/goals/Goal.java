package edu.ntnu.idatt2001.paths.model.goals;

import edu.ntnu.idatt2001.paths.model.player.Player;

import java.io.Serializable;

/**
 * Goal Interface.
 * An interface with a method to check if a goal is fulfilled.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 17.0.6
 */
public interface Goal extends Serializable {
  
  /**
   * Returns true if a goal has been achieved or false
   * if the goal is not achieved.
   *
   * @param player Player - the player to check if goal is fulfilled
   * @throws NullPointerException if player is null
   */
  boolean isFulfilled(Player player);
  
  /**
   * Returns the goal value/object.
   *
   * @return Object - the goal object.
   */
  Object getGoalValue();
}