package edu.ntnu.idatt2001.paths.model.actions;

import edu.ntnu.idatt2001.paths.model.player.Player;

/**
 * ResetAllAction class.
 * Resets all the player's attributes to default values.
 *
 * @author Jens Christian Aanestad
 * @version 1.0
 */
public class ResetAllAction implements Action {
  
  /**
   * The default constructor for the ResetAllAction action.
   */
  public ResetAllAction() {}
  
  /**
   * Adds an item to the player's inventory with calling the
   * addToInventory method from Player.
   *
   * @param player Player - the player to execute an action
   */
  @Override
  public void execute(Player player) {
    player.setGold(player.INITIAL_GOLD);
    player.setHealth(player.INITIAL_HEALTH);
    player.setScore(player.INITIAL_SCORE);
    player.clearInventory();
  }
  
  /**
   * Returns a String representation of the Inventory Action to
   * a specified format.
   *
   * @return String - String representation of the Inventory Action
   */
  @Override
  public String toPathsFormatString() {
    return "ResetAllAction:";
  }
}
