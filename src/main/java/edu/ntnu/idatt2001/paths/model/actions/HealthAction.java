package edu.ntnu.idatt2001.paths.model.actions;

import edu.ntnu.idatt2001.paths.model.player.Player;

/**
 * HealthAction class.
 * Adds/subtracts health points to player's health bar when called.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 17.0.6
 */
public class HealthAction implements Action {
  private final int health;
  
  /**
   * The constructor for the health action.
   *
   * @param health int - the number of health points to add or subtract
   */
  public HealthAction(int health) {
    this.health = health;
  }
  
  /**
   * Adds health to the player's health bar with calling the
   * addHealth method from Player.
   *
   * @param player Player - the player to execute an action
   */
  @Override
  public void execute(Player player) {
    player.addHealth(health);
  }

  /**
   * Returns a String representation of the Health Action to
   * a specified format.
   *
   * @return String - String representation of the Health Action
   */
  @Override
  public String toPathsFormatString() {
    return "HealthAction:" + health;
  }
}