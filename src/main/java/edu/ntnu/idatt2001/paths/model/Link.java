package edu.ntnu.idatt2001.paths.model;

import edu.ntnu.idatt2001.paths.model.actions.Action;
import edu.ntnu.idatt2001.paths.model.player.Player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Represents a link between two passages.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 1.0
 * @see Serializable
 */
public class Link implements Serializable {
  private final String text;
  private final String reference;
  private final List<Action> actions;
  
  /**
   * The main constructor.
   * Initializes a new Link object.
   *
   * @param text      String - text description of the link
   * @param reference String - reference to a passage
   * @param actions   List   - a list of actions which belong to link
   * @throws IllegalArgumentException if text is null or reference is null or if actions is null
   */
  public Link(String text, String reference, List<Action> actions) throws IllegalArgumentException {
    if (text == null || text.isBlank()) {
      throw new IllegalArgumentException("Text can not be null or empty");
    }
    if (reference == null || reference.isBlank()) {
      throw new IllegalArgumentException("Reference can not be null or empty");
    }
    if (actions == null || actions.stream().anyMatch(Objects::isNull)) {
      throw new IllegalArgumentException("Actions can not be null, "
              + "and can not contain null values");
    }
    this.text = text;
    this.reference = reference;
    this.actions = new ArrayList<>();
    this.actions.addAll(actions);
  }
  
  /**
   * Initializes a new Link object.
   * Does not have an action parameter in case the link executes no actions.
   *
   * @param text      String - text description of the link
   * @param reference String - reference to a passage
   * @throws IllegalArgumentException if text is null/empty or if reference is null/empty
   */
  public Link(String text, String reference) throws IllegalArgumentException {
    this(text, reference, new ArrayList<>());
  }
  
  /**
   * Returns the text.
   *
   * @return String - text description of the link
   */
  public String getText() {
    return text;
  }
  
  /**
   * Returns the reference.
   *
   * @return String - reference to a passage
   */
  public String getReference() {
    return reference;
  }
  
  /**
   * Returns the list of actions.
   *
   * @return List - a list of actions to the link.
   */
  public List<Action> getActions() {
    return new ArrayList<>(actions);
  }
  
  /**
   * Adds an action to the action list.
   *
   * @param action Action - an action object
   * @throws IllegalArgumentException if the action is null
   */
  public void addAction(Action action) throws IllegalArgumentException {
    if (action == null) {
      throw new IllegalArgumentException("Action cannot be null");
    }
    actions.add(action);
  }
  
  /**
   * Executes a link's action on the game's player.
   *
   * @param player - the player to execute actions on.
   */
  public void executeActions(Player player) {
    this.getActions().forEach(action -> {
      action.execute(player);
    });
  }
  
  /**
   * Returns a string representation of the link.
   *
   * @return String - string representation of the link
   */
  @Override
  public String toString() {
    return "Link: "
            + "text = " + text
            + ", reference = " + reference;
  }
  
  /**
   * Returns true if the links have the same text and reference.
   *
   * @param o Object - An object to be checked against this link
   * @return boolean - true if the links are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Link link)) {
      return false;
    }
    return Objects.equals(getReference(), link.reference);
  }
  
  /**
   * Returns a hashcode for the Link based on its attributes.
   *
   * @return int - hashcode for the link
   */
  @Override
  public int hashCode() {
    return Objects.hash(getReference());
  }
}