package edu.ntnu.idatt2001.paths.model.actions;

import edu.ntnu.idatt2001.paths.model.player.Player;

/**
 * InventoryAction class.
 * Adds an item to the player's inventory when executed.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 17.0.6
 */
public class InventoryAction implements Action {
  private final String item;
  
  /**
   * The constructor for the Inventory action.
   *
   * @param item String - a text representing an item
   * to add to the player's inventory
   * @throws IllegalArgumentException if the item is null or blank/whitespace
   */
  public InventoryAction(String item) throws IllegalArgumentException {
    if (item == null || item.isBlank()) {
      throw new IllegalArgumentException("Item cannot be null or whitespaces");
    }
    this.item = item;
  }
  
  /**
   * Adds an item to the player's inventory with calling the
   * addToInventory method from Player.
   *
   * @param player Player - the player to execute an action
   */
  @Override
  public void execute(Player player) {
    player.addToInventory(item);
  }

  /**
   * Returns a String representation of the Inventory Action to
   * a specified format.
   *
   * @return String - String representation of the Inventory Action
   */
  @Override
  public String toPathsFormatString() {
    return "InventoryAction:" + item;
  }
}
