package edu.ntnu.idatt2001.paths.model.actions;

import edu.ntnu.idatt2001.paths.model.player.Player;

/**
 * ScoreAction class.
 * Adds/subtracts score points from the player's score when executed.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 17.0.6
 */
public class ScoreAction implements Action {
  private final int points;
  
  /**
   * The constructor for the score action.
   *
   * @param points int - the number of points to add or subtract
   */
  public ScoreAction(int points) {
    this.points = points;
  }
  
  /**
   * Adds points to the player's score bar with calling the
   * addScore method from Player.
   *
   * @param player Player - the player to execute an action
   */
  @Override
  public void execute(Player player) {
    player.addScore(points);
  }

  /**
   * Returns a String representation of the Score Action to
   * a specified format.
   *
   * @return String - String representation of the Score Action
   */
  @Override
  public String toPathsFormatString() {
    return "ScoreAction:" + points;
  }
}
