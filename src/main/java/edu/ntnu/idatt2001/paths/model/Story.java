package edu.ntnu.idatt2001.paths.model;

import edu.ntnu.idatt2001.paths.model.Link;
import edu.ntnu.idatt2001.paths.model.Passage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Represents the story which contains a
 * collection of passages.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 17.0.6
 */
public class Story implements Serializable {
  private final String title;
  private final Map<Link, Passage> passages;
  private final Passage openingPassage;
  
  /**
   * The main constructor.
   * Initializes a Story object.
   * The title can not be null or blank, and passages
   * can not be null. Opening passage can not be null.
   * Since it is possible to add a passage to the passages List,
   * the list can therefore be empty.
   *
   * @param title           String  - name of the story
   * @param passages        Map     - links connected to passages
   * @param openingPassage  Passage - the passage to begin the story
   * @throws IllegalArgumentException if title is null/blank or if passages is null/empty
   * or if opening passage is null
   */
  public Story(String title, Map<Link, Passage> passages, Passage openingPassage) throws IllegalArgumentException{
    if (title == null || title.isBlank()) {
      throw new IllegalArgumentException("Title cannot be null or empty");
    }
    if (passages == null) {
      throw new IllegalArgumentException("Passages cannot be null");
    }
    if (openingPassage == null) {
      throw new IllegalArgumentException("Opening passage cannot be null");
    }
    if (passages.keySet().stream().anyMatch(Objects::isNull) || passages.values().stream().anyMatch(Objects::isNull)) {
      throw new IllegalArgumentException("The map of passages cannot have null keys or values");
    }
    this.title = title;
    this.passages = new HashMap<>();
    this.passages.putAll(passages);
    this.openingPassage = openingPassage;
  }
  
  /**
   * Initializes a Story object.
   * Creates a new story with an empty passages map.
   *
   * @param title           String  - name of the story
   * @param openingPassage  Passage - the passage to begin the story
   * @throws IllegalArgumentException if title is null/blank or if passages is null/empty
   * or if opening passage is null
   */
  public Story(String title, Passage openingPassage) {
    this(title, new HashMap<>(), openingPassage);
  }
  
  /**
   * Returns the title to the story.
   *
   * @return String - title to the story
   */
  public String getTitle() {
    return title;
  }
  
  /**
   * Returns the openingPassage to the story.
   *
   * @return Passage - opening passage to the story.
   */
  public Passage getOpeningPassage() {
    return openingPassage;
  }
  
  /**
   * Adds a new passage to the story.
   * The link for the passage can be created based on
   * the passage's title.
   *
   * @param passage Passage - passage to add to the story
   * @throws IllegalArgumentException if the passage is null or if a passage already is in the map
   */
  public void addPassage(Passage passage) throws IllegalArgumentException {
    if (passage == null) {
      throw new IllegalArgumentException("Passage cannot be null");
    }
    if (getPassages().contains(passage)) {
      throw new IllegalArgumentException("The passage "
              + "is already in the list of passages");
    }
    Link link = new Link(passage.getTitle(), passage.getTitle());
    this.passages.put(link, passage);
  }
  
  /**
   * Retrieves a passage from a link.
   * If the link is not in the map, then null is returned.
   *
   * @param link Link - a link which refers to a passage
   * @return Passage  - the passage referenced by the link
   */
  public Passage getPassage(Link link) {
    return passages.get(link);
  }
  
  /**
   * Retrieves the passages of the story as a collection.
   *
   * @return Collection - the passages in the map passages
   */
  public Collection<Passage> getPassages() {
    return passages.values();
  }

  /**
   * Removes a passage from the passages HashMap with a link.
   * If there exist other passages that links to this passage, it
   * will not be possible to remove the passage.
   *
   * @param link Link - the link which is associated with the reference
   */
  public void removePassage(Link link) throws IllegalArgumentException{
    if (link == null) {
      throw new IllegalArgumentException("Link cannot be null");
    }
    if (getPassages().stream().noneMatch(passage -> passage.getLinks().stream().anyMatch(links -> links.equals(link)))) {
      passages.remove(link);
    } else if (getPassage(link).getLinks().contains(link)) {
      passages.remove(link);
    }
  }

  /**
   * Returns all the broken links that do not reference to any passages in
   * the passage HashMap.
   *
   * @return List - a list of links that has no function
   */
  public List<Link> getBrokenLinks() {
    List<Link> brokenLinks = new ArrayList<>();
    getPassages().stream().map(Passage::getLinks).forEach(links -> links.forEach(link -> {
      if (passages.get(link) == null) {
        brokenLinks.add(link);
      }
    }));
    return brokenLinks;
  }
}