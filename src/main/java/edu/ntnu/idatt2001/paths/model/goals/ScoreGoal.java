package edu.ntnu.idatt2001.paths.model.goals;

import edu.ntnu.idatt2001.paths.model.player.Player;

/**
 * ScoreGoal class.
 * Checks if the player's score is above or equal to a
 * certain amount of score points.
 *
 * @author Jens Christian Aanestad
 * @author Isak Kallestad Mandal
 * @version 17.0.6
 */
public class ScoreGoal implements Goal {
  private final int minimumPoints;
  
  /**
   * The constructor for the score goal.
   * Creates a score gold which compares the player's score
   * to a certain amount of score points.
   * Since the player's score never be less than 0,
   * the score goal can therefore never be less than 1.
   *
   * @param minimumPoints int - a number that
   *                      represent expected minimum score
   */
  public ScoreGoal(int minimumPoints) throws IllegalArgumentException {
    if (minimumPoints < 1) {
      throw new IllegalArgumentException("The score goal can not be less than 1");
    }
    this.minimumPoints = minimumPoints;
  }
  
  /**
   * Returns minimumPoints value.
   *
   * @return minimumPoints int.
   */
  @Override
  public Object getGoalValue() {
    return minimumPoints;
  }
  
  /**
   * Returns true if player's score is above or equal to minimumPoints.
   * Returns false if player's score is below minimumPoints.
   *
   * @param player Player - the player to check if goal is fulfilled
   * @return boolean - true if player's score is above minimumPoints, false if not
   */
  @Override
  public boolean isFulfilled(Player player) {
    return player.getScore() >= minimumPoints;
  }
}
