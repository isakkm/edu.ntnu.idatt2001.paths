package edu.ntnu.idatt2001.paths;

import edu.ntnu.idatt2001.paths.view.App;

/**
 * Represents the entrypoint class in order to start the application.
 * @author jensc
 * @version 1.0
 */
public class Main {
  
  /**
   * The entrypoint of the application.
   *
   * @param args arguments
   */
  public static void main(String[] args) {
    App.main(args);
  }
}
