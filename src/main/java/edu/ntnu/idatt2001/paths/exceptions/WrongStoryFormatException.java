package edu.ntnu.idatt2001.paths.exceptions;

/**
 * Custom exception to be thrown if the story format is invalid;
 *
 * @author Jens Christian Aanestad
 * @version 1.0
 */
public class WrongStoryFormatException extends RuntimeException {
  
  /**
   * Constructor for wrong story format exception.
   * @param message String - the exception message.
   */
  public WrongStoryFormatException(String message) {
    super(message);
  }
}