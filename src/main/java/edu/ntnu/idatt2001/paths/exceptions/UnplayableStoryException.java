package edu.ntnu.idatt2001.paths.exceptions;

/**
 * Custom exception to be thrown if story is unplayable;
 *
 * @author Jens Christian Aanestad
 * @version 1.0
 */
public class UnplayableStoryException extends RuntimeException {
  
  /**
   * Constructor for unplayable story exception.
   *
   * @param message String - the exception message.
   */
  public UnplayableStoryException(String message) {
    super(message);
  }
}
