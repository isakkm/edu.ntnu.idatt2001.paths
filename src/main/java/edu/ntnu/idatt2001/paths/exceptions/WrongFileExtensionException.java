package edu.ntnu.idatt2001.paths.exceptions;

/**
 * Custom exception to be thrown if file extension is invalid;
 *
 * @author Jens Christian Aanestad
 * @version 1.0
 */
public class WrongFileExtensionException extends RuntimeException {
  
  /**
   * Constructor for wrong file extension exception.
   * @param message String - the exception message.
   */
  public WrongFileExtensionException(String message) {
    super(message);
  }
}
